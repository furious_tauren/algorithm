# kernel gtags
```sh
$ make gtags
```
这样可以跳过很多你没有编译的无关代码, 搜索结果更准确.

# 安装字体
```sh
$ sudo yum install google-droid-sans-fonts
```

# 查找同网段 PC
```sh
$ arp -a
$ cd /destination/dir/1
$ find . -name *.xml -exec diff {} /destination/dir/2/{} \;
```

# xrandr
```sh
$ echo "--auto: use the highest resolution"
$ xrandr --output VGA --same-as LVDS --auto

$ echo "disp1 has the same contents as disp0"
$ xrandr --output VGA --same-as LVDS --mode 1024x768

$ echo "open disp1 as right side extend screen"
$ xrandr --output VGA --right-of LVDS --auto

$ echo "Shutdown Display1"
$ randr --output VGA --off
$ randr --output VGA --auto --output LVDS --off
$ randr --output VGA --off --output LVDS --auto
```

# 1. Setup your system
## Install grub on PHD(Portable Hard Disk)
```sh
$ fdisk /dev/sdb ...
$ mkfs -t ext3 /dev/sdb1
$ e2label /dev/sdb1 boot
$ mount /dev/sdb1  /mnt
$ grub-install --root-directory=/mnt --no-floppy /dev/sdb
```

## Install fonts
```sh
$ cp *.ttf /usr/share/fonts/tauren
$ cd /usr/share/fonts/tauren
$ mkfontscale
$ mkfontdir
$ fc-cache -fv
```
mkfontdir: 搜索所用字体文件, 并写入到 font.dir 供 X-server 使用
fc-cache -fv: 刷新字体缓存
fc-list: 查看已安装的字体

## create alias 4 IPs
```sh
$ cat 192.168.110.73 gitserver >> /etc/hosts
host gitserver
	user git
	hostname 192.168.110.155
	port 22
	identityfile ~/.ssh/admin
```

## Release memory
```sh
$ sudo sync
$ sudo echo 3 > /proc/sys/vm/drop_caches
```
0-不释放, 1-释放页缓存, 2-释放dentries和inodes, 3-释放所有缓存


# 2. vim
## Gvim: Gtk-WARNING **: Invalid input string

中文区域设置为 zh_CN.utf8, 而 GVim 只能识别 zh_CN.UTF-8
```sh
$ echo "utf8 to utf-8"
$ cd /usr/share/vim/vim72/lang
$ sudo ln -s menu_zh_cn.utf-8.vim menu_zh_cn.utf8.vim
```

## Edit
> ^+e 向上滚屏(下移一行), ^+y 方向相反
  zt 当前行置顶, zz 置中, zb 置底.
  d4w: 不删除第 5 词词首. !!!c2w: 不删除词前空格
  `x->dl X->dh D->d$ C->C$ s->cl S->cc`
  . 命令重复除 ^+r 和 u 外的所有编辑命令.
  daw 删词及词后空格, diw: 删词(note: das dis cis cas)
  Visual: try o & O

## 加速:
```vim
:cmd: ^+l 向左一词, ^+r 向右, ^+b 行首, ^+e 行尾, ^+w 删词

d^ d$ dnw dnl
gU <-> gu 大小写转化
z-o --- z-c
^+a: 数字加1
insert 模式: ^+w 删除词

读取当前目录下的内容到第 10 行: :10read !ls
:write !wc 内容通过标准输入送入指定的命令中.

global:
:g/^/m 0
line(".")  当前光标所在行的行号
line("'<") 所选区域第一行的行号
string starting with "\=" is evaluated as an expression
:g/^/s/^/\=line("."). " "/
:s/^/\=1 + 2/

:let n=0 | g/abc(\zs\d\+/s//\=n/|let n+=1 ;)
let 为变量赋值 (:help let )
| 用来分隔不同的命令 (:help :bar )
g 在匹配后面模式的行中执行指定的ex命令 (:help :g )
\zs 指明匹配由此开始 (:help /\zs )
\d\+ 查找1个或多个数字 (:help /\d )
s 在选中的区域中进行替换 (:help :s )
\= 指明后面是一个表达式 (:help :s\= )

:s/abc\zs[a-z]\ze//
\zs 替换开始处
\ze 替换结束处
```

## vimdiff
> [c 上一个 diff 点
> ]c 下一个 diff 点
> d-p --- d-o diff-put 和 diff-obtain
> :diffupdate 更新差异
> 
> "xy --- "xp

## cscope
```shell
$ find `pwd` -name "*.h" -o -name "*.c" > tags.files
$ ctags -L tags.files
$ cscope -Rbkq -i tags.files
```
|  R  |  recuerse             |
| :-: | :-------------------- |
|  b  | not enter cscope      |
|  q  | speed up seaching     |
|  q  | exclude /usr/include  |


> :cs f g xx (definition)
> :cs f c xx (functions calling xx)
> :cs f d xx (functions called by xx)
> :cs f s xx (symbol)
> :cs f t xx (text string)
> :cs f f xx (file)
> :cs f i xx (file including xx)
> :cs f e xx (egrep)

## Regular Expression
> \+ 匹配 + 前的字符 1-n 次
> \* 0-n 次
> ? 0-1 次
> {i} i 次
> {i, j} i-j 次
> 
> . 任何字符
> ^ 行首  $ 行尾 \< 词首 \> 词尾
> [c1-c2] c1-c2 中的任何字符
> [^c1-c2] c1-c2 外的任何字符
> \(\) 标记 \(\) 间的内容, 之后可用 \1-\9 来引用它
> 
> a\|b: 多选一

examples:
```vim
:g/^[ \t]*$/d
:%s/[ \t]*$//
```

# 3. Common command
## Common usage
- shell 中发生错误时, 中断执行而退出
`$ set -e`

- 设置 bash 使用跟 vi 相同的快捷键
`$ set -o vi`

- minicom 支持彩色
`$ minicom -c on`

- 统计代码行数
`$ find . -name "*.c" | xargs wc -l`

- history
```shell
$ cd -
$ ls /home/git
$ cd !$
$ !cd
$ cd /home/git/hello.git
$ !!:s/hello.git/hoho
$ !-2
```
- mail
`$ mail -s "subject" user@ip < file`

- yes
`$ yes | rm -i`

- time
`$ time dd if=/dev/hda of=disk.mbr bs=512 count=1`
real    0m 30.10s
user    0m 0.01s
sys     0m 4.73s
user+sys 占用的 cpu 时间, real 总时间(cpu + dma)

- date 
```shell
$ date -s 1982-08-12
$ date -s 18:30:00
$ clock -w
```

- find 
```shell
$ find . -name *.C -type f -o -name *.H -ok rm {} \;
$ find . -name *.c -exec ls {} \;
$ find . -name *.c -print
$ find . -regex '.*b.*3'
$ find . -inum 211028 -exec mv {} newname.dir \;
$ find . -name *.cpp | xargs -i grep "xxx" {} --color -RnH
$ find . -name "*.c" -print0 | xargs -0 sed -i 's/x_cnt/x_counter/g'
```

- echo
`$ echo -en "\xaa\x10" > abc`
write non ascii chars using echo, -n: not print '\n'

- cp support RE
```shell
$ cp /root/{a,b} /root/dir/
$ cp fs2410_kernel_2614.[I,W]?? /tmp
```

- eval cmdLine
eval 会对 cmdLine 扫描两遍, 如果第一遍扫描后 cmdLine 是
个普通命令则执行此命令; 如果 cmdLine 中含有变量的间接引用,
则保证间接引用的语义

# 4. git
## basic

当你执行一次 commit, git 会把当前文件的内容做成快
照, 并保存一个指向此快照的引用. 对于没有发生变化的文
件, git 会保存一个其前版本的链接.

git 不以文件名的方式储存对象, 而是把对象储存到数
据库中, 我们可以通过对象内容的 hash 值去寻址对象.

`|working dir| ------> |staging area| ------> |repository|`

- Git directory 储存元数据和对象数据库, 它是执行依次
git clone 所得到的所有东西.

- 工作目录是工程的一个检出版本, 所有的文件都是从压缩
数据库中抽取的, 放于磁盘, 供你修改和使用.

- 暂存区是一个文件, 它保存下次将要提交的信息, 通常它
就是 index.

## gitignore
```
*.[ao]	# no .a files
!lib.a	# but do track lib.a
/TODO	# only ignore the root TODO file, not subdir/TODO
build/	# ignore all files in the build/ directory
```

## others
- reflog
reflog record your commit history. afer a
`$ git reset --hard abcde`,
the sha-1 "abcde" are still in reflog, you can run
`$ git reset --hard abcde`
to reset it. the
`$ git gc --prune=0`
does not clear refs still in reflog, so, firstly, you should run
`$ git reflog expire --expire-unreachable=0 --all`

- rebase
rebase to delete some commit
`$ git rebase -i sha1-id`

- cherry-pick
merge a commit
`$ git cherry-pick [-n] <commit name>`


