# 数字电路设计中的几个基本概念

## 建立时间和保持时间
建立时间（setup time）是指在触发器的时钟信号上升沿到来以前，数据稳定不变的时间，
如果建立时间不够，数据将不能在这个时钟上升沿被打入触发器；

保持时间（hold time）是指在触发器的时钟信号上升沿到来以后，数据稳定不变的时间，
如果保持时间不够，数据同样不能被打入触发器。

## timescale
Verilog HDL 中的一种时间尺度预编译指令，它用来定义模块仿真时的时间单位和时间精度:
```verilog
`timescale 仿真时间单位/时间精度
```
时间精度就是模块仿真时间和延时的精确程序，比如：定义时间精度为10ns，那么时序中所
有的延时至多能精确到10ns，而8ns或者18ns是不可能做到的。 

```verilog
`timescale 100ns / 10ns 
module muti_delay(
    input  wire din,
    output reg  dout
);
 
always @(din)
    #3.14  dout = din;
endmodule
```
可看到，本意是要延时`100*3.14=314ns`后将din 的值赋给dout1，但是它在310ns 的时候就
赋值了，时间精度定义为10ns， 因此不能精确到4ns，经过四舍五入后，`“#3.14”变成了“#3.1”`，
当然就是在310ns的时候赋值了。

# 实例解析
## Exp1
```verilog
`timescale 1ns/1ps
module adder #(parameter N=4)(
    input  [N-1:0] a,
    input  [N-1:0] b,
    input          cin,        
    output         cout,
    output [N-1:0] sum
);
assign {cout,sum} = a + b + cin; // assign表示连续赋值 
endmodule
```
1. 模块是以module开始， endmodule结束，被认为是一个完整电路描述，其余都只能认为是电路片段。
一个模块就是一个完整电路，如果有n个模块，这n个模块将通过某种机制结合起来，组成一个大的电
路, 但这n个模块都是独立运行的，而且是并行执行的。
2. 对于任意的有意义的逻辑变量（wire/reg，输入输出端口等），必须指定其位宽，如果没有显式指定，
默认的位宽为1bit。
3. assign相当于连线，只能用来给 wire型变量赋值。
4. {}是一种对两个变量合并赋值的简化描述：
```verilog
/* assign {cout,sum}=temp[N:0]; */
assign cout = temp[N]; 
assign sum[N-1:0] = temp[N-1:0];
```
5. 在赋值语句下，算术操作结果的长度由操作符左端目标长度决定。表达式中的所有中间结果应取最大
操作数的长度，如果这个中间结果会赋值给左端的表达式，则最大操作数长度也包括左端目标长度。

## Exp2
```verilog
module mux #(parameter N = 2)(
    input  [N-1:0] a,
    input  [N-1:0] b,
    input  [N-1:0] c, 
    input  [N-1:0] d,
    input  [1:0]   sel, 
    output [N-1:0] mux_out
);
    
    reg [N-1:0] mux_temp;
    assign mux_out = mux_temp;   

    always @(a or b or c or d or sel)
    case (sel)     
        0 : mux_temp = a; 
        1 : mux_temp = b;
        2 : mux_temp = c;
        3 : mux_temp = d;
            
        default : $display("Error with sel signal"); 
    endcase  
endmodule
```
1. 利用case或if语句描述组合电路时，必须提供所有的选择结果情形，否则HDL综合器将根据描述，
自动生成锁存电路。而锁存电路（latch）路，相对组合电路，会带来额外时钟延迟，并引入异步时
序，不适合常规电路设计。
2. always语句是表明所描述的电路行为将被敏感变量表的事件驱动。

## Exp3
```verilog
always @(posedge clk)
begin
    b <= a;
    c <= b;
end
```
1. 在第一个时钟上升沿到来时，a把值赋给b，b把值赋给c，而此时b的值还没有被更新（即a的值还
没有被触发器打给b），只是将b0(b前一次的值)打给c。
2. reg型变量应该赋初值，否则处于随机状态，仿真前段会显示为红色。一般在rst信号到来时，对
变量进行赋初值操作。
3. initial 在仿真初始化时执行一次，其中只能对 reg 变量赋值(`<=` 或 `=`).


## Exp4
```verilog
always @(posedge clk)
begin
    a <= 1;
    a <= 2;
end
```
**a** would be 2 in simulation, the last defined value takes effect. 
Usage of this might be a FSM which sparsely defines its outputs:

```verilog
always @(posedge clk)
begin
    out_one <= 1'b0;
    out_two <= 1'b0;
    out_thr <= 1'b0;
    case (state)
        2'd1: out_one <= 1'b1;
        2'd2: out_two <= 1'b1;
        2'd3: out_thr <= 1'b1;
    endcase
end
```


## Exp5

If you must use any port as `inout`, Here are few things special:

1. You can't read and write inout port simultaneously, hence kept highZ for reading.
2. There should be a condition at which it should be written.
3. `inout` could only be ***wire***.

```verilog
module test (
    inout value,
    output reg var
);

assign value = (condition) ? <some value / expression> : 'bz;

always @(<event>)
    var = value;

endmodule
```

## Exp6

the only difference between `output reg` and `output wire` is that `reg` can
be assigned to in a procedural block. this applies also to external modules who
instantiate it.

```verilog
module test (
    input  wire clk,
    output reg  var
);

always @(posegde clk)
    var <= ~val;

endmodule
```

# 常犯错误

## 多重驱动 (`multipledriver`)

不利于 clock tree synthesis

```verilog
always @(posedge clk)
    sig1 <= data;

always @(negedge clk)
    sig2 <= data;
```

考虑 cond1, cond2 都为真的情况

```verilog
always @(posedge clk)
    if (cond1) sig1 <= data;

always @(posedge clk)
    if (cond2) sig1 <= data;
```

## 例化使用 `inout` 信号

`inout` 使用 `tristate buffer`: 
- risk of bus contestion
- `floating` state(neither `in` nor `out`) causes high power consumption.

## 在 `combination logic` 中复位

```verilog
always @(*)
    if(~rst_n)
        out <= 0;
    else
        out <= a & (b | c);
```


# 知识库

- 使用 `timing logic` 时可省略 `else`，因为 `flip-flop` 具有记忆功能，不会产生 `latch`.
- 使用 `combination logic` 时, `if-else` 产生 `mux`.
- 能用 `case` 时不用 `if`, 这是因为 `if` 嵌套 `if` 会产生多级 `mux`.
- case 中使用`独热码`，虽增加了 `ff`，但减少了 `cmp`，延时更少.
- `~|` 称归约或非，结果为1位; `~|a` 将 a 每位相或再取反. `有 a = 4’b0001; 则 ~|a = 1'b0`

# dcm mmcm pll
1. dcm: 可对输入时钟进行相位移动，补偿，产生倍频和分频时钟，但是 5 以及以后的产品不用了.
2. pll: 不能相移时钟，但 pll 产生时钟的频率比 dcm 更精准，时钟的 jitter 也更好.
3. mmcm: pll+dcm 相移功能的结合体.

# testbench

```verilog

reg addr;
reg data;
initial begin
    clk   = 0;
    rst_n = 0;
    #200
    rst_n = 1;
end

initial begin
    #500
    foo(255);
end

always #10 clk <= ~clk;

task foo(len);
    integer len, i;
    begin
        for (i = 0; i < len; i = i + 1) begin
            @(posedge clk);
            addr <= i[7:0];
            data <= i[7:0];
        end

        addr <= 0;
        data <= 0;
    end
endtask
```
- `initial` 相当于上电触发(只运行一次)
- `testbench` 中 `begin end` 里的语句是顺序执行的，故 addr 会在 clk 上升沿时触发.



