# arm 汇编实验环境: 
 1. linux c 语言调用汇编写的函数
 1. u-boot go 命令调试(bin)

# hello.c
```c
void myprintf(char *fmt, ...);
int main(void)
{
	myprintf = 0xabcdabcd;	/* lookup uboot.map for printf */
	myprintf("hello, this func calls printf in uboot\n");
	return 0;
}
```
```sh
$ arm-linux-gcc -c hello.c

$ echo "should run go 0x30000000 in uboot"
$ arm-linux-ld -Ttext=0x30000000 -Tdata=0x40000000 hello.o -o hello

$ echo "elf to bin"
$ arm-linux-objcopy -I elf32-littlearm -O binary hello hello.bin
$ arm-linux-objcopy -d hello
```

# apcs
>r0-r3: corrupted
r4-r9(v1-v6): not changed
ip(r12): corrupted, used as old sp and its old value not saved
lr(r14): changed if you call a function
fp(r11): pointer to stack frame, then sp could be changed
pc: pushed for debug and not be popped
因为 sp 在函数中会无节制变化(特别是调用了其他函数时), 所以我们用 fp 来
指向本函数 sp 的顶部. 之后可以 sub sp, fp, #12 来恢复 sp.

```asm
foo:    mov     ip, sp                  @ .---------------.
        stmfd   sp!, {fp, ip, lr, pc}   @ |    old data   | <- ip
                                        @ ^---------------^
        sub     fp, ip, #4              @ |    pc         | <- fp(frame pointer)
                                        @ ^---------------^
                                        @ |    lr         | -> old pc(fp - 4)
        sub     sp, sp, #500            @ ^---------------^
        bl      test                    @ |    ip         | -> old sp(fp - 8)
                                        @ ^---------------^
        sub     sp, fp, #12             @ |    fp         | -> old fp(fp - 12)
        ldmfd   sp, {fp, sp, pc}        @ ^---------------^
```



# 中断压栈:
>r0-r12 共用, 压栈保存
每种模式都有各自的 r13, r14. 故不需为 svc 的 r13-r14 压栈.
压的是 irq 模式的 r13-r14.
压栈 r13 是为恢复 irq 的 sp 寄存器.
压栈 r14 是为调用其他函数.
^: 代表 `mov cpsr, spsr` 与 ldm 操作同时进行, 否在会出问题

```asm
irq:
	sub	lr, lr, #4
	stmfd	sp!, {r0-r14}
	bl	do_irq
	ldmfd	sp, {r0-r13, pc}^
```

# arm 寄存器组:
上电, ARM 默认 SVC 模式, ARM 状态,  IRQ&FIQ 关闭. 但考虑
到代码可能是在任何情况下拷贝到内存然后跳转过来运行的, 在
代码中还是要再设置一次.

total: 31
```
r0 r1 r2 r3 r4 r5 r6 r7 r8 r9 r10 r11 r12
---------- 8 ---------- ---- 10(fiq) ---- 
```

r13 r14: 每种模式使用一组共 12 个(usr/sys 公用一组)

r15(cpsr):
```
.-----------.----------.--------------.---.
|31 30 29 28| 7  6 | 5 |4  3  2  1  0 |   |
|-----------+------+---+--------------+---|
|N  Z  C  V | I  F | T |M4 M3 M2 M1 M0|   |
|-----------+------+---+--------------+---|
|-  0  c  o | !irq |a/t|1  0  0  0  0 |usr|0
|      a  v | !fiq |   |1  0  0  0  1 |fiq|1
|      r  e |      |   |1  0  0  1  0 |irq|2
|      r  r |      |   |1  0  0  1  1 |svc|3
|      y  f |      |   |1  0  1  1  1 |abt|7
|         l |      |   |1  1  0  1  1 |und|11
|         o |      |   |1  1  1  1  1 |sys|15
|         w |      |   |              |   |
^-----------^------^---^--------------^---^
```

# PC 
PC 总指向正在取址的指令, 在多级流水线中, 该地址等于
**正执行指令地址加 8.**
bl 把 pc - 4 放入 lr, 即 lr 为 bl 指令的下条指令地址.
pc 读写值不对称: mov pc, pc ;跳转到下面第 2 条语句(right!)
**所谓正在执行某条指令时发生中断, 指令是原子的, 此指令会执行完.**
函数返回:
`mov pc, lr	@ lr = pc(orig） - 4`
中断返回:
`mov pc, lr - 4	@ lr = pc(orig)`


注意: str pc, addr 保存的值可能是当前指令下面的 8 或
12 字节偏移(取决与芯片设置), 所以一般别用 str 操作 pc

# cpsr 中 t 位:
在 armv4 前应设置为 0:
在 armv4 及以后 t 版本中 0->arm 1->thumb
在 armv5 非 t 系列 0->arm, 1->下条指令将产生 und 中断.
b[l]x 指令根据跳转地址的最低位可转换为 thumb 状态

# 异常:
1. 将 CPSR 复制到相应异常模式的 SPSR
2. 据异常类型设置 CPSR 的运行模式位(irq 时, I 位置 1)
3. 把下一条指令的地址放到连接寄存器 LR(异常模式)
4. PC 从异常向量地址取下一条指令, 跳转到异常处理程序

# bss 段:
存放程序中未初始化的全局变量的一块内存区域, u-boot 在
链接脚本中指定该段的起始位置, 并计算大小.

# label
```asm
_armboot_start:
     .word _start 
```
label 地址由链接脚本和 gcc 编译所带的 -Ttext 选项共同决定.
最终的地址为: 链接脚本中指定的值加上 gcc 中 -Ttext 的参数

# gas 注释
```asm
;  @  /**/
```

# .macro
```
.macro inc, a ;可省略 "," 
ldr r0, =\a
add r0, #1
.endm
inc r0
```
分号用于划分独立的行

```asm
#define END(name) \
  .size name, .-name
#define ENTRY(name) \
	.export name !\
	.align 4 !\
name:
#define ENDPROC(name) \
	.type name, @function !\
	END(name) 
```

# gas 位操作
```asm
tst r0, #0x80 ;按位与结果设置 cpsr
cmp r0, #0x80 ;按 r0-#0x80 结果设置 cpsr
lsr ;寄存器中字的高端空出的位补 0
asr ;保持符号位不变, 如为正数, 则空出的位补 0, 否则补 1
ror ;由字的低端移出的位填入字的高端空出的位
eor ;异或
bic ;清位 bic r0, #3 ;r0 低 2 位清 0
mov r0, #-3
mov r0, r0, asr #30
```

# 条件码
>as below:
cs/hs (carry set/unsigned higher or same c set)
cc/lo (carry clear/unsigned lower c clear)
mi (minus/negative n set)
pl (plus/positive or zero n clear)
vs (overflow v set)
vc (no overflow v clear)
hi (unsigned higher c set and z clear)
ls (unsigned lower or same c clear or z set)

# gas 符号/字节 操作

有符号操作(s)只适用与字节(b)和半字(h)
```asm
foo:
	ldr r0, =rodata
        ldrsb r0, [r0]
        mov pc, lr
rodata: .word -1
```

# .align
```asm
.align: 在当期位置插入 0-3 字节, 以使代码 4 字节对齐.
.align 5 (2^5 ---> 32 字节对齐, cacheline 32 bits, 提高了效率
.balignl 16, 0xdeadbeef ;16 字节对齐, 空余部分用 0xdeadbeef 补齐
.fill count, size, value
.fill 1024, 1, 0
.skip/.space size, fill ;用 fill(省略默认为 0) 填充 size 个字节
```

# .equ
用于定义常数(非字符串), 可以与 .globl 合用

```asm
.equ  a, 0x1
.set  a, 0x1
a = 0x1
```
如命令出现常数需加 #, 而用 .equ 定义的常数无需 #

# .ascii

```asm
.ascii "abc", "bcd" ;插入 abcbcd 在当前位置
.asciz "abc", "bcd" ;插入 abc\0bcd\0 在当前位置
.byte 64, 0x12, 'a' ;插入 0x40 0x12 0x41 在当前位置
buf: .skip 512 ; 插入 512 字节, 其值不可预料
.rept 3
.ascii 'a'
.endr ; 插入 3 个 0x61
```

# .^
the ^ qualifier specifies that the CPSR is restored from the SPSR


# .section .type
>.section: 声明一个段
.section .start, "awx" ;allocatable, writable, executable
先面的格式只用于 elf 目标
.section ".start", #alloc, #write, #execinstr @兼容 solar

>.type: 一般只用于 elf 目标
.type name, [#%]function/object

# .swi
mov r0, #0x17 ;0x17 angel_SWIreason_EnterSVC 半主机操作
swi 0x123456  ;0x123456 是 arm 指令集的半主机操作编号

# mov 指令中的立即数
`mov r0, 0x10300000	@ 非 0 位不超过 2 个就行`

# 寄存器寻址
b 是相对当前 PC 的跳转, 偏移量通过 bit[23:0] 算出, 32M

uboot 可在任何内存运行, 故 b reset 是不二之选
ldr 指令操作数范围是 4k, ldr 宏到数据缓存区的偏移范围 4k
`ldr pc, _irq ; 在 mmu 开启后, 不能用 b(超出 32M)`


```asm
adr r0, _start		;通过 PC 当前值计算 _start 的值, 注意 _start 取值
ldr r0, #123		;123 在指令范围内
ldr r0, =label1
ldr r0, =0xffabcdef	;此处 ldr 是宏, 用于超出范围的常数/标签

ldr r0, [r1, #4]	;r0 ← [r1 + 4]  
ldr r0, [r1, #4]!	;r0 ← [r1 + 4]	, r1 ← r1 + 4
ldr r0, [r1], #4	;r0 ← [r1]	, r1 ← r1 + 4

ldr r0, label @ after compile: ldr r0, [pc, #n] @ in literal pool
adr r0, label @ after compile: add r0, #offset
```

# 多寄存器寻址
>ldmia r0, {r2, r1} ;与寄存器顺序无关: r1 ← [r0], r2 ← [r0 + 4]

movs pc, lr 
ldmia sp, {r0-lr}^	
当目标寄存器包含 pc 时, 把spsr复制到 cpsr
对于 usr 和 sys 没有 spsr 的模式, 结果不可预料
{r0-lr}^: ^ 指明列表中的寄存器为 usr 模式下的寄存器

断压栈因不是同种模式的寄存器, 故不用简单的 stmfd 指令
stmfd入栈指令, 相当于stmdb
ldmfd出栈指令, 相当于ldmia

# arm 初始化: 
整个start.S是一额段.

a, 中断向量(以 irq 为例, 其他的异常中断有类似的形式):

IRQ_STACK_START 在 arch/arm/lib/interrupts.c 的 interrupt_init 
函数中会被重新赋值.

上电的时候指令Cache可关闭, 也可不关闭, 但数据Cache一定
要关闭, 否则可能导致刚开始的代码里面, 去取数据的时候,
从Cache里面取, 而这时候 RAM 中数据还没有Cache过来,
导致数据预取异常

```asm
/*
 * 1. *0 地址: nand启动时, soc 内置4k ram 映射为0地址.
 * 系统会通过硬件把 nand 的前4k代码搬运到此处; 
 * nor 启动时, nor 首地址被映射为0地址.
 * 我们在0地址安置中断向量表, 代码搬运到 ram 后的中断,
 * 只要没有开 mmu, 还是会到这里走一圈. 但是此时irq 函数
 * 在 ram 中, 所以不能用b 命令跳转.
 * 
 * 2. 进入 linux 后, linux 会建立自己独立的中断向量表.
 */
_start: b start_code 
	ldr pc, _irq 
	_irq:	.word irq
	.
	.
	.
	.macro	irq_save_user_regs
	sub	sp, sp, #S_FRAME_SIZE
	stmia	sp, {r0 - r12}		@ Calling r0-r12
	add	r7, sp, #S_PC
	stmdb	r7, {sp, lr}^		@ Calling SP, LR
	str	lr, [r7, #0]		@ Save calling PC
	mrs	r6, spsr
	str	r6, [r7, #4]		@ Save CPSR
	str	r0, [r7, #8]		@ Save OLD_R0
	mov	r0, sp
	.endm

	.macro	irq_restore_user_regs
	ldmia	sp, {r0 - lr}^		@ Calling r0 - lr
	mov	r0, r0
	ldr	lr, [sp, #S_PC]		@ Get PC
	add	sp, sp, #S_FRAME_SIZE
	/* return & move spsr_svc into cpsr */
	subs	pc, lr, #4
	.endm

	.align	5
irq:
	get_irq_stack 		@ 宏用于得到堆栈
	irq_save_user_regs 	@ 宏用于保存寄存器状态到堆栈
	bl	do_irq 		@ do_irq c文件中的空函数, 可以添加功能
```



6, ------ ddr 时序: 

read:
```
.---.   .-----.   .----.   .-----.   .---------.   .-----.   .---.
|ACT|-->|n NOP|-->|read|-->|n NOP|-->|prechange|-->|n NOP|-->|ACT|
^---^   ^-----^   ^----^   ^-----^   ^---------^   ^-----^   ^---^
  |-------tRCD-------|---CL---|
  |-----------------tRAS-----------------|---------tRP---------|
  |----------------------------tRC-----------------------------|
```

write:
```
.---.   .-----.   .---- .   .-----.   .---------.   .-----.   .---.
|ACT|-->|n NOP|-->|write|-->|n NOP|-->|prechange|-->|n NOP|-->|ACT|
^---^   ^-----^   ^---- ^   ^-----^   ^---------^   ^-----^   ^---^
  |-------tRCD------|          |----tWR--- |
  |------------------tRAS------------------|--------tRP---------|
  |---------------------------tRC-------------------------------|
```


# ldrex
ARMV6 用 SWP 原子交换寄存器和内存数据, 用于实现信号量操作
ldr 

# inline asm
```asm
__asm__ __volatile__(  
     代码列表  
     : 输出运算符列表  
     : 输入运算符列表  
     : 被更改资源列表  # 指示编译器压栈
);
```
`__asm__`: 同 asm, 是汇编代码指示符. 因 gcc 初期没有 asm 关键字
为避免 asm 与变量名冲突纳入了 __asm__.
`__volatile__`: 只是 gcc 编译时不要对代码进行优化! 即保持汇编
代码原样.
```asm
__asm__ __volatile__(
	"mrs r12, cpsr\n\t"
	"orr r12, r12, #0xc0\n\t"
	"msr cpsr_c, r12":::"r12", "cc", "memory"
);
```

  rn, 指示编译器压栈
  If our instruction can alter the condition code register,
we have to add "cc" to the list of clobbered registers.
  If our instruction modifies memory in an unpredictable fashion,
add "memory" to the list of clobbered registers. This will cause
GCC to not keep memory values cached in registers across the
assembler instruction. We also have to add the volatile keyword
if the memory affected is not listed in the inputs or outputs of
the asm.
```
.-----------.------------.      .----------.----------------------.
| constrain |    含义     |      | modifier |       说明            |
.-----------+------------.      .----------+----------------------.
|  r/l      | r0-r15     |      |  none    | read-only(4 input)   |
.-----------+------------.      .----------+----------------------.
|  m        | memory     |      |  =       | write-only(4 output) |
.-----------+------------.      .----------+----------------------.
|  I        | Immediate  |      |  +       | rw(4 both)           |
.-----------+------------.      .----------+----------------------.
|  X        | Any        |      |  &       | a register 4 output  |
^-----------^------------^      ^----------^----------------------^
```


# 内存屏障
指令执行分成 取指, 译码, 访存, 执行, 写回若干阶段. 流水线是并行的.
比如说 CPU 有一个加法器和一个除法器, 那么一条加法指令和一条除法指
令就可能同时处于 "执行" 阶段. 最终乱序执行完成. 这就是所谓的
"顺序流入, 乱序流出".

CPU 的乱序执行并不是任意的乱序, 而是以保证程序上下文因果关系为前
提, 比如: a++; b=f(a); c--;
由于 b=f（a）依赖于 a++ 的执行结果, 所以将在"执行"阶段之前被阻
塞, 直到 a++ 执行结果生成出来; 而 c-- 跟前面没有依赖, 它可能在
b=f(a) 之前就能执行完

以读屏障为例, 它用于保证读操作有序. 屏障之前的读操作一定会先于屏障
之后的读操作完成, 写操作不受影响. 同属于屏障的某一侧的读操作也不受
影响.
dmb 等待数据缓冲真正写入器件
dsb 等待流水线指令全部完成
isb 清空流水线并重新加载指令流水线

# 原子操作
ldrex 和 strex 是 AMRV6 引入的新的同步机制, 取代了过去的 SWP
指令. 与 address monitor 协同工作, 为内存的访问提供了一个状态机.

1. ldrex 会为执行处理器做一个标记(tag), 说当前对该物理地址已经有
一个 CPU 访问了, 但是还没有访问完毕.
2. strex 指令执行时, 会检查是否存在这个标记. 如果存在, 那么将完成
这次 store 的过程, 并且返回 0, 然后清除该标记. 如果没有这个标记,
不会完成store, 返回 1

```c
static inline void atomic_add(int i, atomic_t *v)
{
	unsigned long tmp;
	int result;

	__asm____volatile__("@ atomic_add\n"
	"1:	ldrex	%0, [%3]\n"
	"	add	%0, %0,%4\n"
	"	strex	%1, %0,[%3]\n"
	"	teq	%1, #0\n"
	"	bne	1b"
	:"=&r" (result), "=&r" (tmp), "+Qo"(v->counter)
	:"r" (&v->counter), "Ir" (i)
	:"cc");
}
```

根据 Linux 内核里面 atomic_add 的实现, 分析一个 UP 上的并发情景.

1. 首先会执行指令 ldrex 添加一个标记(Tag)
2. 假设此时发生了中断, 在中断处理或切换上下文后需对该值进行原子加一.
3. 在新上下文中执行指令 ldrex, strex. 由于此时有标记, strex 成功
   执行, 而后更新内存, 清除标记.
4. 假设此时中断返回, 被中断的任务继续进行. 当执行strex时, 会发现标记
   已被清除, 此时 strex 就不会更新内存, 并且返回 1
5. 跳转到 1(bne	1b), 重新执行

# `__attribute__((weak, alias("abc")))`
weak 弱类型的函数可以被其它同名函数覆盖(即不会发生冲突), 如果没有其它
同名函数, 就使用该 weak 函数, 类似于是默认函数.

in order to use the alias attribute, you cannot alias a symbol
outside of the translation unit. So not use alias in head files.
use it like this:

foo.h:
```c
void pr_debug(void);
```

foo.c:
```c
static void print(void)
{
	printf("%s\n", __func__);
}
void pr_debug(void) __attribute__ ((weak, alias("print")));
```

utils.c:
```c
#include "foo.h"
int main(void)
{
	pr_debug();
	return 0;
}
```


# things about mmu
```
va ------> tlb ---cached---> access
            |(not cached)
            V
      check ap ---noperm---> exception
            | perm
            V
   check C bit ---C=0------> access from memory
            | C=1
            V
   access from cache
   
.--------.   2    .--------.      .--------.      .--------.
|   AC   | domain |        |  1   |        |  1   |        |
|hardware| <----- |  TLB   |<---> |  TLB   |<---> |        |
^--------^    .-> |        |      |        |      |        |
    |         |   |        |      ^--------^  1   |  main  |
    |         |   |        | -------------------> | memory |
   2|abort    |   ^--------^   |                  |        |
    |        1|     2|C,B      |                  |        |
    V         |   .--v-----.   |  .----------. 4  |        |
.--------.    |  4| cache &|   `->|   cache  |<-->|        |
|  ARM   | -----> |   WB   |<---> |line fetch|    |        |
^--------^   VA   ^--------^      ^----------^    ^--------^

 31                    20 19 18 17 16 15     12   10  9  8     5  4  3  2  1 0
------------------------.--.--.--.--.--.------.----.--.--------.--.--.--.----.
| section base address  |NS| 0|nG| S| X| TEX  | AP | I| domain |XN| C| B| 1 0|
------------------------^--^--^--^--^--^------^----^--^--------^--^--^--^----^
 x x x x x x x x x x x x  0  0  0  0  0  1 0 1  1 1  x  0 0 0 0  0  1  1  1 0
```
>XN Bit The execute-never bit. Determines whether the region is executable
I Bit The meaning of this bit is IMPLEMENTATION DEFINED.
X Bit APX(AP[2])
Bit [18], when bits [1:0] == 0b10 
 0:	Descriptor is for a Section
 1:	Descriptor is for a Supersection.
TEX(101): L2 cache write-back, write-allocate


### enable mmu:
```
setup pagetable --> invalidate ICache and tlb --> enable mmu
   if va != pa, we need remap the code near(after) the
   command enbling mmu with va = pa.
          0x100 --> enable mmu cmd 
   pc --> 0x104 --> next cmd 
   
ENTRY(__turn_mmu_on)
	mov	r0, r0
	instr_sync
	mcr	p15, 0, r0, c1, c0, 0		@ write control reg
	mrc	p15, 0, r3, c0, c0, 0		@ read id reg
	instr_sync
	mov	r3, r3
	mov	r3, r13
	mov	pc, r3
__turn_mmu_on_end:
```

   "mrc	p15, 0, r3, c0, c0, 0" could be replace by "mov	r0, r0",
   It's actually the recommended sequence for most ARM CPUs,
   except Xscale in this respect.
     By ARM compliant implementation, this instrument guarantees
   that the write to the CP register has taken effect.


### disable mmu:
disable DCache --> invalidate TLB --> disable mmu

### CP15 domain register contains 16 domains, each occupies 2 bits.
   00: no access
   @01: need check AP bits(reference to arm datasheet)
   10: reserved
   11: access without checking AP bits

# things about cache
清除(invalidate)操作清零相应 cache 行的有效位, 从而强制将内存
数据加载到cache之中 
对于采用写回策略的 Dcache, 就需要使用清理(clean)操作. 把脏的
cache 行强制写到主存, 并把 cache 行中的脏位清零. 清理 cache
可以重建 cache 与主存的一致性, 它只用在使用写回策略的 Dcache上.
To clean, clean the L1 cache first then L2
To invalidate, invalidate the L2 cache first then L1.

  When you clear the C bit in the CP15 SC Register for
a given processor, data caching is disabled and no new cache
lines are allocated to the L1 data cache and L2 cache because
of requests from that processor.
  When you disable the cache, all Write-Back Cacheable requests
still look up the L1 cache.

```
"Now I think that it is a bootloader requirement.
"
"And in the arm linux kernel's booting requirement,
"Data cache must be off and must not contain any stale data.
"
"> You must invalidate the instruction cache, the data cache, and BTAC
"> before using them.
"> You are not required to invalidate the main TLB, even though it is
"> recommended for
"> safety reasons. This ensures compatibility with future revisions of
"> the processor.
">
"> But the kernel does.. ( __v7_setup in proc-v7.S )
"> clean & invalidate before turning on the MMU, not just 'invalidate'.
">
"> If dcache tag&data ram has garbage data when power up,
"> I think some dirty data lines can be flushed into to L2 or L3 memory
"> because kernel does clean.
"> And It can pollute the memory.
">
"> Is it possible in cortex-A9?
```

Cache 写回内存有两种模式:
1. Write Back: 
  Cache Line 中的数据被 CPU 核修改时并不立刻写回内存,
  Cache Line和内存中的数据会暂时不一致, 在 Cache Line
  中有一个 Dirty 位标记这一情况. 当一条 Cache Line 要被
  其它 VA 的数据替换时, 如果不是 Dirty 的就直接替换掉,
  如果是 Dirty 的就先写回内存再替换.

   Write Through:
  修改 Cache Line 中的数据时就立刻写回内存, Cache Line
  和内存中的数据总是一致的.

2. flush: 把 Cache 内容写回 Memory, 当 Cache 为 Write through, 不需要 Flush
invalidate: 把 Cache 内容直接丢掉不要

3. set associative cache
   8way set associative, 32bytes cache line ---> numset=4K/32(128)
```
   .--------------------------------------.--------------.----------.
   |        20bits pfn(4k-aligned)        | set idx(128) |          |
   ^--------------------------------------^--------------^----------^
  31                                 12 11          05 04      00
```

   cache 眼中, 内存被分为许多 4KB 大小的页, 每 32(line-size)B 称一个缓存线.

   SAC 即内存中一条给定的"缓存线"只能被保存在一个特定的 set 中.
   页的第 n 条缓存线(offset: 32*n -- 32*(n+1))须存储到 set n.

4. cache 的使用场合
   a. 收到 packet 后, 要对 packet buffer 做 cache invalidate
   b. 发送 packet 时, 要对 packet buffer 做 cache flush
  descripter 一般结构比较小, 如使用 cache 的话, 做 invalidate/flush
  可能得不偿失.  

5. In systems with multiple layers of caches, some care is needed when performing
   cache maintenance activity where the implementation does not provide atomic
   cache maintenance operations. This means systems where cache maintenance must
   be performed separately on L1 and L2 caches. Examples of such systems include
   Cortex-A9 coupled with L2C-310, or Cortex-A5 coupled with L2C-310. A particular
   area of concern is where there are multiple CPUs sharing L2 cache and there is
   a possibility that one CPU may access memory while another CPU is performing
   cache maintenance operations. ARM processors provide three kinds of cache
   maintenance operation, with provision for such operations to be broadcast between
   processors in an MPCore system. Such operations may be required when an external
   DMA is available in the system and is not participating in automatic cache
   coherency management.


# stack
  局部变量可能直接使用寄存器储存, 局部数组使用 stack.
  uCOS 中每个任务都有自己的堆栈, 异常也有自己的 stack. 如果支持中断嵌套,
  堆栈可能需要比较大. 注意堆栈溢出, 会导致各种奇怪的现象.
