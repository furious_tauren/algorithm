```
1. 读入所有的 Makefile.
2. 读入被 include 的其它 Makefile.
3. 初始化文件中的变量(展开: 除规则命令行中的变量和函数以外)
4. 推导隐晦规则, 并分析所有规则.
5. 为所有的目标文件创建依赖关系链.
6. 根据依赖关系, 决定哪些目标要重新生成.
7. 执行生成命令.

1-5 步为第一个阶段, 6-7 为第二个阶段. 第一个阶段中, 如果定义的变量被使用了, 那
么, make 会把其展开在使用的位置. 但 make 并不会完全马上展开, make 使用的是拖
延战术, 如果变量出现在依赖关系的规则中, 那么仅当这条依赖被决定要使用了, 变
量才会在其内部展开

objects = *.o
上面这个例子, 表示了, 通符同样可以用在变量中. 并不是说 [*.o] 会展开, 不 !objects
的值就是 "*.o". Makefile 中的变量其实就是 C/C++ 中的宏. 如果你要让通配符在变量中
展开, 也就是让 objects 的值是所有 [.o] 的文件名的集合, 那么, 你可以这样:
objects := $(wildcard *.o)
这种用法由关键字 "wildcard" 指出, 关于 Makefile 的关键字, 我们将在后面讨论.

.phony: 目标默认是要被生成或更新的文件, 可以用 phony 来声明一个伪目标, 如此
即使当前目录存在同名的文件也不会影响规则的执行.
如果当前目录有个文件叫 clean, 我们运行 make clean 时, 会遇到
"make: "clean" 是最新的. " 的错误, 用 phony 来声明 clean 就解决了这个问题.

指定源文件路径:
VPATH=../include:src
vpath %c src:bld
vpath % bst
vpath # 取消前面的所有 vpath 指定

make -C abc 等价于 cd abc && make, 当 abc 下 make 执行完之后再次回到本目录.
-s"("--slient")或是"--no-print-directory" 取消 Entering directory `/home/hchen/gnu/make'.

nullstring :=
space := $(nullstring) # end of the line
nullstring 是一个 Empty 变量,其中什么也没有,而我们的 space 的值是一个空格. 因为
在操作符的右边是很难描述一个空格的,这里采用的技术很管用,先用一个 Empty 变
量来标明变量的值开始了,而后面采用 "#" 注释符来表示变量定义的终止,这样,我
们可以定义出其值是一个空格的变量.
请注意这里关于 "#" 的使用, 注释符 "#" 的这种
特性值得我们注意,如果我们这样定义一个变量:
dir := /foo/bar # directory to put the frobs in
dir 这个变量的值是 "/foo/bar", 后面还跟了 4 个空格,如果我们这样使用这样变量来
指定别的目录------"$(dir)/file" 那么就完蛋了.

变量:
在 Makefile 中执行的时候其会自动原模原样地展开在所使用的地方
规则后面直接跟命令, 在所有的命令结束后, 可以定义变量. 但定义前面不要有 tab
键.
:= 前面的变量不能使用后面的变量, 只能使用前面已定义好了的变量

我们可以替换变量中的共有的部分,其格式是 "$(var:a=b)" 或是 "${var:a=b}", 其意
思是, 把变量 "var" 中所有以 "a" 字串 "结尾" 的 "a" 替换成 "b" 字串. 这里的 "结尾
"意思是 "空格" 或是 "结束符".
还是看一个示例吧:
foo := a.o b.o c.o
bar := $(foo:.o=.c)


$(subst <from>,<to>,<text>)
$(patsubst <pattern>,<replacement>,<text>)
$(strip <string>)
$(findstring <find>,<in>)
$(filter-out <pattern...>,<text>)
$(sort <list>)
$(word <n>,<text>)
$(wordlist <s>,<e>,<text>)
$(firstword <text>)

$(dir <names...>)
$(notdir <names...>)
$(suffix <names...>)
$(basename <names...>)
$(addsuffix <suffix>,<names...>)
$(addprefix <prefix>,<names...>)
$(join <list1>,<list2>)

$(foreach <var>,<list>,<text>)
$(origin <variable>)
$(if $(ARCH),ARCH=$(ARCH),) $(if CONDITION,then value1,else value)
call 函数是唯一一个可以用来创建新的参数化的函数. 你可以写一
个非常复杂的表达式这个表达式中, 你可以定义许多参数, 然后你可
以用 call 函数来向这个表达式传递参数:
  $(call <expression>;,<parm1>;,<parm2>;,<parm3>;...)
执行时, 参数中的变量, 如 $(1), $(2), 会被参数 <parm1>, <parm2>
依次取代. 而 <expression>; 的返回值就是 call 函数的返回值:
  reverse =  $(1) $(2)
  foo = $(call reverse,a,b)
那么, foo 的值就是 "a b". 当然, 参数的次序是可以自定义的, 如:
  reverse =  $(2) $(1)
  foo = $(call reverse,a,b)
此时 foo 的值就是 "b a"
```
