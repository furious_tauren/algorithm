```c
/* XXX ------ udelay
 * loops = n * HZ * loops_per_jiffy / 1000000
 * refer to arch/arm/lib/delay.S(2199023U>>11 = 2^30/1000000)
 * loops_per_jiffy is cacluted in start_kernel, easy but skillful
 */


/* XXX: ------ Assert in kernel */
/* BUG_ON and panic causes an oops and halt your system */
BUG_ON(bad_thing);

if (terrible_thing)
	panic("....");

/* to see value of regs and the callback route, use dump_stack() */
if (debug)
	dump_stack();


/* XXX ------ list_head */
/* init: */ LIST_HEAD(hello); /* or */
struct list_head head;
LIST_HEAD_INIT(head);

list_add(c, a); /* insert */
/*
 * a ---n---> b ---n---> a    a ---n---> c ---n---> b ---n---> a
 * a ---p---> b ---p---> a    a ---p---> b ---p---> c ---p---> a
 */
list_add_tail(c, a); /* insert */
/*
 * b ---n---> c ---n---> a ---n---> b
 * b ---p---> a ---p---> c ---p---> b
 */


list_del(a); /* delete*/

list_entry(ptr, type, member); /* container_of(ptr, type, member) */

list_for_each_entry(pos, head, member);
/* for loop: pos used to be return parent struct of every node */
/* example */
static struct s *s;
static LIST_HEAD(list);
list_add_tail(&s->list, &list);
list_for_each_entry(s, head, member) {
	s->a = 2;
}

list_for_each_entry_safe(pos, n, head, member) /* support delete */
list_for_each(list, head)

/* XXX ------ hash table */
hlist_del(struct hlist_node *n); /* delete */
hlist_add_head(struct hlist_node *n, struct hlist_head *h); /* n after head */
hlist_add_before(struct hlist node *n,struct hlist_node *next); /* next -> n */
hlist_add_after(struct hlist node *n,struct hlist_node *next); /* n -> next */
hlist_for_each_entry(tpos, pos, head, member); /* tpos -> parent struct */


/* XXX ------ critical section
 * only allow one thread entering at any time.
 * globa varibal: as hardware, visit it only when
 * it is free(need atomic op)
 */
if (atomic_dec_and_test(&a)) {
	atomic_inc(&a);
	return -EBUSY;
} else {
	... critical section ...
}


/* XXX ------ gcc */
switch-case: case 2 ... 7: statement;

int widths[] = {[1 ... 9] = 1, [10 ... 19] = 2, [20] = 3};

struct store {
     int a;
     int data[0];
};


/* XXX ------ likely
 * if (likely(a)), case a would be compile after fore
 * code, so it could be cached with the fore code
 */



/*
 * XXX: ------ Error process
 * ERR_PTR and PTR_ERR is simple, it's all about a type convertion.
 *
 * In kernel, some func return a pointer. If error, we all
 * hope the returned pointer could reflect it. A right returned
 * pointer points to the start of a page(4k). It means: ptr & 0xfff == 0
 * So if ptr is between (0xfffff000, 0xffffffff), it's a invalid ptr.
 * And cast int(-1000-0) to unsigned long, it would be between
 * 0xfffff000-0xffffffff.
 */
static inline long IS_ERR(const void *ptr)
{
	return (unsigned long)ptr > (unsigned long)-1000L;
}

if (a) return ERR_PTR(-EBUSY);

/*
 * XXX: ------ Basic
 * 在编写驱动时, 程序员应当特别注意这个基础的概念:
 * 编写内核代码来存取硬件, 但是不能强加特别的策略给
 * 用户, 因为不同的用户有不同的需求. 驱动应当做到使
 * 硬件可用, 将所有关于如何使用硬件的事情留给应用程序.
 * 一个驱动, 这样, 就是灵活的, 如果它提供了对硬件能
 * 力的存取, 没有增加约束. 然而, 有时必须作出一些策
 * 略的决定. 例如, 一个数字 I/O 驱动也许只提供对硬件
 * 的字符存取, 以便避免额外的代码处理单个位.
 *
 *   对策略透明的驱动有一些典型的特征. 包括支持同步
 * 和异步操作, 可以多次打开的能力, 利用硬件全部能力,
 * 没有软件层来"简化事情"或者提供策略相关的操作. 这
 * 样的驱动不但对他们的最终用户好用, 而且证明也是易
 * 写易维护的.
 *
 *   __devinit 和 __devinitdata 只在内核没有配置支持
 * hotplug 设备时转换成 __init 和 _initdata
 *
 *   在注册内核设施时, 注册可能失败. 即便最简单的动作
 * 常常需要内存分配, 分配的内存可能不可用. 因此模块代
 * 码必须一直检查返回值, 并确认要求的操作实际上已成功.
 *
 *   内核完全可能会在注册完成之后马上使用任何你注册的
 * 设施. 换句话说, 在你的初始化函数仍然在运行时, 内核
 * 将调用进你的模块. 不要注册任何设施, 直到所有的需要
 * 支持那个设施的你的内部初始化已经完成.
 *
 *   内核调试, printk 和 ioctl/proc 仍是 linus 推荐的
 * 方式. printk 在串口上带来的延迟可用 ioctl 方案代替.
 * strace/kgdb 不是 linus 推荐的.
 *
 */

/*
 * XXX: ------ bit_ops
 * use local variable to guarantee "if" and "test_and_set_bit"
 * to be atomic.
 */
static inline int
____atomic_test_and_set_bit(unsigned int bit, volatile unsigned long *p)
{
	unsigned long flags;
	unsigned int res;
	unsigned long mask = 1UL << (bit & 31);

	p += bit >> 5;

	raw_local_irq_save(flags);
	res = *p;
	*p = res | mask;
	raw_local_irq_restore(flags);

	return (res & mask) != 0;
}

/*
 * 圆方括号箭头一句号.
 * 自增自减非反负, 针强地址长度,
 * 乘除, 加减, 再移位,
 * 小等大等, 等等不等,
 * 八位与, 七位异, 六位或, 五与, 四或, 三疑, 二赋, 一真逗.
 * printf("%.*f", 4, 12.1234567);
 */

/*
 * XXX: ------ ALIGN
 * #define ALIGN(x,a) (((x)+(a)-1)&~(a-1)) 
 * 就是以a为上界对齐的意思. 举个 4k 页面边界的例子, 即 a = 4096: 
 *  如 x = 3888, 那么以上界对齐, 执行结果就是4096.
 *  如 x = 4222, 则结果为8192.
 */
 
/*
 * XXX: ------ mmu
 * 转换表基址寄存器(CP15 的 C2)保存着第一级转换表基址的
 * 物理地址(C2 bits[31:14] 有效, bits[13:0] SBZ.
 * 故一级表须 16KB 对齐).
 *
 * 转换表基址寄存器的 bits[31:14](18bits) 与虚拟地址的
 * bits[31:20](12bits) 和两个 0 位连接形成 32 为物理地址.
 * 这个地址选择了一个四字节转换表入口, 它是第一级描述符
 * 或是指向第二级页表的指针. 一级页表使用 4096 个描述符来
 * 表示 4GB 空间, 每个描述符对应 1MB 的虚拟地址.
 *
 * 一级页表项: C2[31:14] + VA[31:20] + 00
 * VA 低 12(微页为 10) bits 等同于物理地址, 剩余的高 20(细表
 * 为 22) bits 用于一级一级的索引.
 * PA 的高 20/16/20/22 bits 保存在段/大页/小页/微页描述符中.
 * 页映射时, L1 盛放 L2 base 的高 22/20(粗/细页) bits.
 * VA[19:x] 用于索引 L2 项.
 * C: cacheable B: bufferable
 *
 * 段映射:
 * 31             (18bits)            14 13                         0
 * .------------------------------------.----------------------------.
 * |            TTB Base                |     SBZ(16K aligned)       | C2
 * ^------------------------------------+----------------------------^
 *       +
 * 31    |                20 19            12 1110 9  8      5 4  3  2  1  0
 * .------------------------.----------------.----.--.--------.--.--.--.--.--.
 * |     section base       |      SBZ       | AP |  | domain |1 |C |B |1 |0 |
 * ^------------------------+----------------+----+--+--------+--+--+--+--+--^
 *       |
 *   PA Start
 *
 * 粗表小页:
 * 31             (18bits)            14 13                         0
 * .------------------------------------.----------------------------.
 * |            TTB Base                |     SBZ(16K aligned)       | C2
 * ^------------------------------------+----------------------------^
 *       +
 * 31    |                                    10 9  8      5 4  3 2  1  0
 * .--------------------------------------------.--.--------.--.----.--.--.
 * | coarse pase base(包含 L2 Base 的 PA 高 22 位)|  | domain |1 |    |0 |1 |
 * ^--------------------------------------------+--+--------+--+----+--+--^
 *       +
 * 31    |                                     1110 9 8  7 6  5 4  3  2  1  0
 * .------------------------------------------.----.----.----.----.--.--.--.--.
 * |            small pase base               |ap3 |ap2 |ap1 |ap0 |C |B |1 |0 |
 * ^------------------------------------------+----+----+----+----+--+--+--+--^
 *
 * 一级页表描述符
 * 31                     20 19            12 1110 9  8     5  4  3  2  1  0
 * .------------------------.----------------.----.--.--------.--.--.--.--.--.
 * |            coarse pase base                  |  | domain |1 |     |0 |1 |
 * ^------------------------+----------------+----+--+--------+--+--+--+--+--^
 * |   section pase base    |                | AP |  | domain |1 |C |B |1 |0 |
 * ^------------------------+----------------+----+--+--------+--+--+--+--+--^
 * |            fine pase base               |    |  | domain |1 |     |1 |1 |
 * ^------------------------+----------------+----+--+--------+--+--+--+--+--^
 *
 * 二级页表描述符
 * 31                             16 15    12 1110 9 8  7 6  5 4  3  2  1  0
 * .--------------------------------.--------.----.----.----.----.--.--.--.--.
 * |        large pase base         |        |ap3 |ap2 |ap1 |ap0 |C |B |0 |1 |
 * ^--------------------------------+--------+----+----+----+----+--+--+--+--^
 * |            small pase base              |ap3 |ap2 |ap1 |ap0 |C |B |1 |0 |
 * ^-----------------------------------------+----+----+----+----+--+--+--+--^
 * |               tiny pase base                 |         |ap  |C |B |1 |1 |
 * ^-----------------------------------------+----+--+-+----+----+--+--+--+--^
 *
 * 一个 L2 粗页表有 256(1MB/4KB)个页表项, 占用 1KB 空间,
 * 每个页表项将一个 4KB 的虚拟存储块转换成一个 4KB 的物
 * 理存储块. 如果转换的是一个 64KB 页, 同一个页表必须在
 * 页表中重复 16 次.
 */
 
/* XXX: ------ 内存的访问权限检查
 * 由 C3, Domain, C1 的 R/S 位或描述符的 AP 位共同决定.
 * "域"决定是否对某块内存进行权限检查, "AP" 决定如何进行权限检查
 * arm 有16个域, C3 每两位对应一个域(共32位), 决定是否进行权限检查
 *  00: 无访问权限(任何访问都将导致 "Domain fault" 异常)
 *  01: 客户模式(使用段描述符/页描述符 AP 进行权限检查)
 *  10: 保留(相当于"无访问权限")
 *  11: 管理模式(不进行权限检查, 允许任何访问)
 * Domain 占用 4 位, 用来表示内存属于 0-15, 哪一个域
 * 例:
 *  粗页表中的"Domain"为 0b1010, 表示 1MB 内存属于域 10, 如果C3[21:20]
 *  等于0b01, 则使用二级页表中的大页/小页描述符中的 "ap3-0" 进行权限检查.
 *
 * AP 结合 C1 的 R/S 位, 决定如何进行访问检查
 * .--.-.-.---.---.
 * |AP|S|R|svc|usr|
 * ^--+-+-+---+---^
 * |00|0|0|NO |NO |
 * ^--+-+-+---+---^
 * |00|1|0|R  |NO |
 * ^--+-+-+---+---^
 * |00|0|1|R  |R  |
 * ^--+-+-+---+---^
 * |01|x|x|R/W|NO |
 * ^--+-+-+---+---^
 * |10|x|x|R/W|R  | 1<<10
 * ^--+-+-+---+---^
 * |11|x|x|R/W|R/W| 1<<11
 * ^--+-+-+---+---^
 * |xx|1|1|reserve|
 * ^--+-+-+---+---^
 */

/* XXX: ------ C1
 * .--.--.--.--.--.--.--.--.--.--.--.--.--.--.--.--.
 * |15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 * ^--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--^
 * |. |. |V |I |. |. |R |S |B |. |. |. |. |C |A |M |
 * ^--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--^
 *
 * V: 向量表所在的位置, 0:0x00000000; 1:0xFFFF0000
 * I: 0:关闭 ICaches; 1:开启ICaches
 * B: 0:little endian; 1:big endian
 * C: 0:关闭 DCaches; 1:开启 DCaches
 * A: 0:aliagn; 1:no aliagn
 * M: 0:关闭MMU; 1:开启MMU
 */
 
/*
 * The bootmem allocator is used, for example, to allocate the
 * mem_map - the array of page structs used by the VM subsystem
 * to keep track of the disposition of physical pages.

 * The bootmem allocator lives only until the kernel has set up
 * the data structures necessary to support the zone allocator.
 */

/*
 * XXX: ------ slab
 * XXX: ------ atag&cmdline
 */
 
/*
 * XXX: ------ __attribute__
 * GNU C的一大特色就是 __attribute__ 机制. __attribute__ 可设置函数属
 * 性(Function Attribute), 变量属性(Variable Attribute)和类型属性(Type Attribute)
 * __attribute__ 语法格式为:
 * __attribute__ ((attribute-list))
 *
 * 其位置约束为: 放于声明的尾部 ";" 之前.
 */
struct foo
{
     char c;
     int i;
} __attribute__ ((__packed__));

int x __attribute__((aligned(16))) = 0;
 
/*
 * # alias ("target")
 * The alias attribute causes the declaration to be emitted as
 * an alias for another symbol, which must be specified.
 *
 * defines ‘f’ to be a weak alias for ‘__f’. In C++, the mangled
 * name for the target must be used. It is an error if ‘__f’ is not
 * defined in the same translation unit.
 */
void __f () { /* Do something. */; }
void f () __attribute__ ((weak, alias ("__f")));

 
 /* # format
  * format 属性告诉编译器, 按 printf, scanf, strftime或strfmon的参数
  * 表格式规则对该函数的参数进行检查. archetype 指定是哪种风格;
  * string-index 指定传入函数的第几个参数是格式化字符串;
  * first-to-check 指定从函数的第几个参数开始按上述规则进行检查
  * 编译时只用指定了 -Wformat 选项, 才会出现警告信息(-Wall 更没问题)
  */
int my_printf(void *my_object, const char *my_format, ...)
			__attribute__ ((format (printf, 2, 3)));
			
/* # noreturn
 * This attribute tells the compiler that the function won't ever
 * return, and this can be used to suppress errors about code
 * paths not being reached.
 */
extern void exitnow();

int foo(int n)
{
        if ( n > 0 )
	{
                exitnow();
		/* control never reaches this point */
	}
        else
                return 0;
}

/*
 * $ cc -c -Wall test1.c
 * test1.c: In function `foo':
 * test1.c:9: warning: this function may return with or without a value
 *
 * But when we add __attribute__, the compiler suppresses the
 * spurious warning:
 */
extern void exitnow() __attribute__((noreturn));

/* # used
 * 指示编译器在对象文件中保留静态函数, 即使将该函数解除引用也是如此
 */

/*
 * XXX: ------ __atags_pointer
 * 1. u-boot 将配置参数地址通过 r2 传递给内核
 * 2. __mmap_switched 中把它储存于 __atags_pointer 中
 */
struct tag_header {
	__u32 size;
	__u32 tag;
};

struct tag {
	struct tag_header hdr;
	union {
		struct tag_core		core;
		struct tag_mem32	mem;
		struct tag_videotext	videotext;
		struct tag_ramdisk	ramdisk;
		struct tag_initrd	initrd;
		struct tag_serialnr	serialnr;
		struct tag_revision	revision;
		struct tag_videolfb	videolfb;
		struct tag_cmdline	cmdline;

		struct tag_acorn	acorn;

		struct tag_memclk	memclk;
	} u;
};

#define tag_next(t)	((struct tag *)((__u32 *)(t) + (t)->hdr.size))

#define for_each_tag(t,base)		\
	for (t = base; t->hdr.size; t = tag_next(t))

```
