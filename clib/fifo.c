/* ~.~ *-c-*
 *
 * Copyright (c) 2018, John Lee <furious_tauren@163.com>
 * Fri May 25 16:32:13 CST 2018
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include "fifo.h"

#undef min
#define min(a, b) ((a) < (b) ? (a) : (b))

static void __fifo_in(struct fifo *fifo, const void *src,
			unsigned int len, unsigned int off)
{
	unsigned int l;

	off &= fifo->mask;
	l = min(len, (fifo->mask + 1) - off);

	memcpy(fifo->data + off, src, l);
	memcpy(fifo->data, src + l, len - l);
}

static void __fifo_out(struct fifo *fifo, void *des,
			unsigned int len, unsigned int off)
{
	unsigned int l;

	off &= fifo->mask;
	l = min(len, (fifo->mask + 1) - off);

	memcpy(des, fifo->data + off, l);
	memcpy(des + l, fifo->data, len - l);
}

unsigned int fifo_in(struct fifo *fifo, const void *src, unsigned int len)
{
	unsigned int l;

	l = fifo_space(fifo);
	if (len > l)
		len = l;

	__fifo_in(fifo, src, len, fifo->in);
	fifo->in += len;
	return len;
}

unsigned int fifo_out(struct fifo *fifo, void *des, unsigned int len)
{
	unsigned int l;

	l = fifo->in - fifo->out;
	if (len > l)
		len = l;

	__fifo_out(fifo, des, len, fifo->out);
	fifo->out += len;
	return len;
}

struct fifo *fifo_alloc(unsigned int size)
{
	struct fifo *fifo;

	fifo = malloc(size + sizeof(struct fifo));
	if (!fifo)
		return NULL;
	fifo->data = (void *)fifo + sizeof(struct fifo);
	fifo->in = fifo->out = 0;
	fifo->mask = size - 1;

	return fifo;
}

void fifo_free(struct fifo *fifo)
{
	free(fifo);
}

void fifo_init(struct fifo *fifo, void *data, unsigned int size)
{
	fifo->data = data;
	fifo->mask = size - 1;
	fifo->in = fifo->out = 0;
}

#if 0
int main(int argc, char *argv[])
{
	unsigned int i;
	struct fifo *fifo;
	char buf[4096] = { 0, 1, 2, 3 };
	char a[4096];

	fifo = fifo_alloc(8192);

	for (i = 0; i < 0x100010; i++) {
		fifo_in(fifo, buf, 4096);
		fifo_out(fifo, a, 4096);
		printf("%08x(%08x,%08x): %d %d %d %d\n", i, fifo->in, fifo->out, a[0], a[1], a[2], a[3]);
	}

	for (i = 0; i < sizeof(a); i++) {
		printf("%02x ", a[i]);
		if ((i + 1) % 20 == 0)
			printf("\n");
	}
	fifo_free(fifo);
	return 0;
}
#endif

