/* ~.~ *-c-*
 *
 * Copyright (c) 2018, John Lee <furious_tauren@163.com>
 * Fri May 25 16:32:22 CST 2018
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __FIFO_H__
#define __FIFO_H__

struct fifo {
	unsigned int mask;
	unsigned int in;
	unsigned int out;

	void *data;
};

static inline unsigned int fifo_cnt(struct fifo *fifo)
{
	return fifo->in - fifo->out;
}

static inline unsigned int fifo_space(struct fifo *fifo)
{
	return (fifo->mask + 1) - (fifo->in - fifo->out);
}

static inline void fifo_reset(struct fifo *fifo)
{
	fifo->in = fifo->out = 0;
}

unsigned int fifo_in(struct fifo *fifo, const void *src, unsigned int cnt);
unsigned int fifo_out(struct fifo *fifo, void *des, unsigned int cnt);


struct fifo *fifo_alloc(unsigned int size);
void fifo_free(struct fifo *fifo);

void fifo_init(struct fifo *fifo, void *data, unsigned int size);

#endif

