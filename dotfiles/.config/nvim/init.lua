require('options')
require('plugins')
require('keymaps')
require('plugins.nvim-tree')
require('plugins.lualine')
--require('plugins.bufferline')
require('plugins.lsp')
require('plugins.treesitter')
require('plugins.telescope')
require('plugins.cmp')
require('plugins.autopairs')
require('plugins.toggleterm')
require('plugins.leap')
require('plugins.lspsaga')
require('plugins.symbols-outline')
require('plugins.wilder')

-- vim.cmd[[set background=light]]
-- vim.cmd[[colorscheme tokyonight-moon]]
vim.cmd[[set background=dark]]
vim.cmd[[colorscheme gruvbox]]

vim.o.exrc=true
