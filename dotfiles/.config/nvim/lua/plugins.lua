
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
  },
  { "ellisonleao/gruvbox.nvim", priority = 1000 , config = true, opts = {}},
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  },

  {'akinsho/toggleterm.nvim', version = "*", config = true},
  {'ggandor/leap.nvim'},
  {'akinsho/bufferline.nvim', version = "*", dependencies = 'nvim-tree/nvim-web-devicons'},
  {'nvim-lualine/lualine.nvim', dependencies = 'nvim-tree/nvim-web-devicons' },
  {
    'nvim-tree/nvim-tree.lua',
    lazy = true,
    dependencies = {
      'nvim-tree/nvim-web-devicons',
    },
  },

  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function () 
      local configs = require("nvim-treesitter.configs")

      configs.setup({
          ensure_installed = { "c", "cpp", "lua", "vim", "vimdoc", "query", "elixir", "heex", "javascript", "html" },
          sync_install = false,
          highlight = { enable = true },
          indent = { enable = true },
        })
    end
  },

  {
    'nvim-telescope/telescope.nvim', branch = '0.1.x',
      dependencies = { 'nvim-lua/plenary.nvim' }
  },
  {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",
    "nvimdev/lspsaga.nvim"
  },


  {
    "neovim/nvim-lspconfig",
    dependencies = {
        {
            "SmiteshP/nvim-navbuddy",
            dependencies = {
                "SmiteshP/nvim-navic",
                "MunifTanjim/nui.nvim"
            },
            opts = { lsp = { auto_attach = true } }
        }
    },
    -- your lsp config or other stuff
  },



  {
    "ray-x/lsp_signature.nvim",
    --"simrat39/symbols-outline.nvim",
    "hrsh7th/cmp-nvim-lsp", -- { name = nvim_lsp }
    "hrsh7th/cmp-buffer",   -- { name = "buffer" }
    "hrsh7th/cmp-path",     -- { name = "path" }
    "hrsh7th/cmp-cmdline",  -- { name = "cmdline" }
    "hrsh7th/nvim-cmp",
  },

  {
    "hrsh7th/cmp-vsnip",    -- { name = 'vsnip' }
    "hrsh7th/vim-vsnip",
    "rafamadriz/friendly-snippets",
  },

  {
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    config = true
  },
  {'gelguy/wilder.nvim'},

  {
    "folke/trouble.nvim",
    opts = {}, -- for default options, refer to the configuration section for custom setup.
    cmd = "Trouble",
    --keys = {
    --  {
    --    "<leader>xx",
    --    "<cmd>Trouble diagnostics toggle<cr>",
    --    desc = "Diagnostics (Trouble)",
    --  },
    --  {
    --    "<leader>xX",
    --    "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
    --    desc = "Buffer Diagnostics (Trouble)",
    --  },
    --  {
    --    "<leader>cs",
    --    "<cmd>Trouble symbols toggle focus=false<cr>",
    --    desc = "Symbols (Trouble)",
    --  },
    --  {
    --    "<leader>cl",
    --    "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
    --    desc = "LSP Definitions / references / ... (Trouble)",
    --  },
    --  {
    --    "<leader>xL",
    --    "<cmd>Trouble loclist toggle<cr>",
    --    desc = "Location List (Trouble)",
    --  },
    --  {
    --    "<leader>xQ",
    --    "<cmd>Trouble qflist toggle<cr>",
    --    desc = "Quickfix List (Trouble)",
    --  },
    --},
  },

  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function()
      require("lazy").load({ plugins = { "markdown-preview.nvim" } })
      vim.fn["mkdp#util#install"]()
    end,
  },

  {
    'mikesmithgh/kitty-scrollback.nvim',
    enabled = true,
    lazy = true,
    cmd = { 'KittyScrollbackGenerateKittens', 'KittyScrollbackCheckHealth' },
    event = { 'User KittyScrollbackLaunch' },
    -- version = '*', -- latest stable version, may have breaking changes if major version changed
    -- version = '^5.0.0', -- pin major version, include fixes and features that do not have breaking changes
    config = function()
      require('kitty-scrollback').setup()
    end,
  },


})
