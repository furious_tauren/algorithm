local opt = vim.opt

-- line number
opt.relativenumber = true
opt.number = true

-- indent
opt.tabstop     = 8
opt.softtabstop = 8
opt.shiftwidth  = 8
opt.expandtab   = false
opt.autoindent  = true
opt.textwidth   = 80

-- display line wrapped
opt.wrap = false

-- search
opt.ignorecase = true
opt.smartcase  = true

-- gui
-- insert/normal/visual
opt.showmode      = false
-- Highlight current line
opt.cursorline = true
opt.number        = true
opt.termguicolors = true
-- filename or titlestring
opt.title         = true
opt.foldenable    = false
-- If on, Vim will wrap long lines at a character in 'breakat'
-- rather than at the last character that fits on the screen.
opt.linebreak     = true
opt.pumblend      = 10
opt.pumheight     = 15
opt.signcolumn    = 'number'
opt.list          = true
opt.listchars     = { trail = '.', tab = '»-»' }
-- gui frontend
opt.guifont       = 'monospace:h16.4'

-- behavior
opt.complete      = nil
opt.completeopt   = 'menu,menuone,noselect'
opt.grepprg       = 'rg --vimgrep --follow --no-heading'
opt.hidden        = true
opt.inccommand    = 'nosplit'
opt.lazyredraw    = true
opt.mouse         = 'a'
opt.scrolloff     = 5
opt.shortmess     = 'atToOc'
opt.sidescrolloff = 10
opt.timeoutlen    = 400
opt.ttimeoutlen   = 50
-- fs
vim.opt.undofile      = true
vim.opt.undolevels    = 1000
vim.opt.undoreload    = 10000
vim.opt.writebackup   = true

if vim.g.neovide == true then
    -- vim.cmd 'set guifont=Hack\ NF:h10'
    -- vim.o.guifont='Consolas:h10'
    -- vim.o.guifont='FiraCode NF:h14'
    vim.o.guifont='Operator Mono,FiraCode NF:h14'
    vim.api.nvim_set_keymap('n', '<F11>', ":let g:neovide_fullscreen = !g:neovide_fullscreen<CR>", {})
end

