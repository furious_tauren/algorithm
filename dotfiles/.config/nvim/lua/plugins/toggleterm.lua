require("toggleterm").setup({
    open_mapping = [[<c-\>]],
    start_in_insert = true,
    -- direction = 'vertical',
    -- direction = 'horizontal',
    direction = 'float',
    shadow_terminals = true
})

