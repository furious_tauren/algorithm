/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2016年 02月 29日 星期一 10:06:20 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/mman.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/firmware.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/poll.h>

#include "fifo.h"
#include "io_cmd.h"

#define GPIO_nCFG	(32 + 16) 
#define GPIO_DONE	(32 + 22)
#define GPIO_nSTATUS	(32 * 4 + 4)
#define GPIO_DCLK	(32 + 19)
#define GPIO_DATA	(32 + 21)

#define TARO_BUFSIZE	0x300000
#define TARO_BLKSIZE	(1024 * 16)

#undef min
#define min(x, y)	((x) < (y) ? (x) : (y))

struct pr_info {
	volatile bool		is_pending;
	volatile int		status;
	volatile size_t		fire_number;
	volatile size_t		page_number;
	volatile size_t		page_printed;
	volatile size_t		blk_size;
	long long		remaining;
};

struct taro_data {
#	define BIT_OPEN		0
	unsigned long		bitmask;
	int			irq_xmit;
	int			irq_stat;

	struct device		*dev;
	struct fifo		*fifo;

	void __iomem		*ram;
	void __iomem		*ioaddr;
	wait_queue_head_t	waitq;
	struct pr_info		pinfo;
};
static struct taro_data *__data;

/* ---------------------------------------------------------- */
static void firmware_ioconfig(void)
{
	gpio_direction_output(GPIO_DCLK, 1); 
	gpio_direction_output(GPIO_DATA, 1); 

	gpio_direction_output(GPIO_nCFG, 1); 
	gpio_direction_input(GPIO_DONE);
	gpio_direction_input(GPIO_nSTATUS);
}

static int firmware_setup_weim(void)
{
	void __iomem *ioaddr;

	ioaddr = ioremap(0x021b8000, 0x4000);
	if (ioaddr == NULL)
		return -ENOMEM;

	/*
	 * Set external interrupt polarity to high
	 * and free-run continuous BCLK
	 */
	writel(0x00000028, ioaddr + 0x90);
	iounmap(ioaddr);
	return 0;
}

static void __write(u8 *buf, int len)
{
	int i, j;
	__u32 val;

	void *gpio_dr = ioremap(0x020a0000, 0x4000);
	if (!gpio_dr) {
		pr_err("%s :ioremap failed\n", __func__);
		return;
	}

	local_irq_disable();
	val = readl(gpio_dr) & ~(1 << 19);

	for (i = 0; i < len; i++) {
		for (j = 0; j < 8; j++) {
			/* set dclk to 0 and set data to j bit of buf[i] */
			val &= ~(1 << 21);
			val |= (buf[i] >> j & 0x1) << 21;
			writel(val, gpio_dr);

			/* generate the rise edge */
			writel(val | (1 << 19), gpio_dr);
		}
	}
	local_irq_enable();

	iounmap(gpio_dr);
}

static int __load_firmware(const u8 *firmware, int size)
{

	unsigned long timeout, read_time;
	u8 *p = (u8 *)firmware;

	/* pull down 1us to reconfigure fpga */
	gpio_set_value(GPIO_nCFG, 0); 
	udelay(1);

	/* start timing */
	while (gpio_get_value(GPIO_nSTATUS) != 0)
		usleep_range(1, 1);
	gpio_set_value(GPIO_nCFG, 1); 

	while (gpio_get_value(GPIO_nSTATUS) == 0)
		usleep_range(1, 1);
	udelay(2);	/* Tst2ck */

	/* write data */
	__write(p, size);
	timeout = jiffies + msecs_to_jiffies(1);	/* 1s */
	do {
		read_time = jiffies;

		/* success */
		if (gpio_get_value(GPIO_DONE)) {
			gpio_set_value(GPIO_DATA, 1);
			gpio_set_value(GPIO_DCLK, 1);
			gpio_set_value(GPIO_nCFG, 1);
			return 0;
		}

	} while (time_before(read_time, timeout));

	return -ETIMEDOUT;
}

static int load_firmware(void)
{
	int rval;
	const struct firmware *fw_entry;

	rval = firmware_setup_weim();
	if (rval) {
		pr_err("firmware_setup_weim failed\n");
		return rval;
	}

	firmware_ioconfig();

	rval = request_firmware(&fw_entry, "taro.fw", NULL);
	if (rval)
		return rval;

	rval = __load_firmware(fw_entry->data, fw_entry->size);
	if (rval)
		pr_err("firmware not available\n");

	release_firmware(fw_entry);
	return rval;
}

/* ---------------------------------------------------------- */
static void status_log(struct taro_data *data, int status)
{
	struct pr_info *pinfo = &data->pinfo;

	pinfo->is_pending = true;
	pinfo->status = status;
	wake_up_interruptible(&data->waitq);
}

static void start_printing(struct taro_data *data)
{
	writel(0x1, data->ioaddr + 0x1a0);	/* Enable PD */
}

static inline void pause_printing(struct taro_data *data)
{
	writel(0x3, data->ioaddr + 0x1a0);
}

static inline void stop_printing(struct taro_data *data)
{
	writel(0x0, data->ioaddr + 0x1a0);	/* Disable PD */
}

static void restart_printer(struct taro_data *data)
{
	struct pr_info *pinfo = &data->pinfo;

	writel(0x1, data->ioaddr + 0x010);	/* Reset */

	fifo_reset(data->fifo);
	memset(pinfo, 0, sizeof(*pinfo));
}


static irqreturn_t taro_xmit_interrupt(int irq, void *dev_id)
{
	struct taro_data *data = __data;
	struct pr_info *pinfo = &data->pinfo;
	size_t size = min(pinfo->blk_size, pinfo->remaining);

	size = min_t(size_t, size, fifo_cnt_d(data->fifo));
	if (size) {
		memcpy(data->ram, fifo_oaddr(data->fifo), size);
		fifo_out(data->fifo, NULL, size);
		pinfo->remaining -= size;
	}

	return IRQ_HANDLED;
}

static irqreturn_t taro_stat_interrupt(int irq, void *dev_id)
{
	struct taro_data *data = __data;
	struct pr_info *pinfo = &data->pinfo;

	int status = readl(data->ioaddr + 0x0220);
	if (status & SL_PAGE_END) {

		pinfo->page_printed++;

		if (pinfo->page_printed >= pinfo->page_number) {
			pr_info("%s :GOOD JOB(remaining:%llx)\n",
					__func__, pinfo->remaining);
			status |= SL_JOB_END;
			stop_printing(data);
		} else if (pinfo->page_printed <= pinfo->page_number - 10
				&& pinfo->page_number > 10) {
			writel(pinfo->fire_number, data->ioaddr + 0x3000);
		}

	}

	if (status & SL_DATA_LACK) {
		pr_err("%s :DATA LACK(remaining:%llx)\n",
				__func__, pinfo->remaining);
		stop_printing(data);
	}

	/*
	 * status:  (start)   (end) (param lack) (data lack)
	 * bits:        0        1        2        3
	 */
	status_log(data, status);

	/* reset status register */
	writel(0xf, data->ioaddr + 0x01f0);
	writel(0x0, data->ioaddr + 0x01f0);

	return IRQ_HANDLED;
}

static int taro_open(struct inode *inode, struct file *filp)
{
	struct taro_data *data = __data;

	filp->private_data = data;

	if (test_and_set_bit(BIT_OPEN, &data->bitmask))
		return -EBUSY;

	return 0;
}

static int taro_release(struct inode * inode, struct file * filp)
{
	struct taro_data *data = (struct taro_data *)filp->private_data;
	clear_bit(BIT_OPEN, &data->bitmask);
	return 0;
}

static ssize_t taro_read(struct file *filp,
		char __user *buf, size_t count, loff_t *ppos)
{
	struct taro_data *data = (struct taro_data *)filp->private_data;
	struct pr_info *pinfo = &data->pinfo;
	int tmp[2] = { 0 };

	wait_event_interruptible(data->waitq, pinfo->is_pending);
	tmp[0] = pinfo->status;
	if (tmp[0] & SL_PAGE_END)
		tmp[1] = pinfo->page_printed;
	else if (tmp[0] & SL_PAGE_START)
		tmp[1] = pinfo->page_printed + 1;

	pinfo->is_pending = false;

	if (copy_to_user(buf, tmp, sizeof(tmp)))
		return -EFAULT;

	return sizeof(tmp);
}

static ssize_t taro_write(struct file *filp,
		const char __user *buf, size_t count, loff_t *ppos)
{
	struct taro_data *data = (struct taro_data *)filp->private_data;
	struct pr_info *pinfo = &data->pinfo;
	int status = SL_JOB_START;

	if (copy_from_user(&pinfo->remaining, buf, sizeof(long long)))
		return -EFAULT;

	memcpy(data->ram, fifo_oaddr(data->fifo), pinfo->blk_size);
	fifo_out(data->fifo, NULL, pinfo->blk_size);

	pr_info("%s :Start: Fires(0x%x), Pages(0x%x)\n",
		       	__func__, pinfo->fire_number, pinfo->page_number);
	pr_info("%s :remaining %llx\n", __func__, pinfo->remaining);
	pinfo->remaining -= pinfo->blk_size;

	status_log(data, status);
	start_printing(data);

	return 0;
}

static int taro_mmap(struct file* filp, struct vm_area_struct *vma)
{
	struct taro_data *data = (struct taro_data *)filp->private_data;

	/* mapped as io memory */
	vma->vm_flags |= VM_IO | VM_LOCKED | VM_DONTEXPAND | VM_DONTDUMP;
	if (remap_pfn_range(vma, vma->vm_start,
				virt_to_phys(data->fifo)>>PAGE_SHIFT,
				TARO_BUFSIZE + sizeof(struct fifo),
				vma->vm_page_prot
			   ))
		return -EAGAIN;

	return 0;
}

static unsigned taro_poll(struct file *filp, poll_table *wait)
{
	unsigned int mask = 0;
	struct taro_data *data = (struct taro_data *)filp->private_data;
	struct pr_info *pinfo = &data->pinfo;

	poll_wait(filp, &data->waitq, wait);

	if (pinfo->is_pending)
		mask = POLLIN | POLLRDNORM;

	return mask;
}

static long taro_ioctl(struct file *filp, unsigned cmd, unsigned long arg)
{
	struct taro_data *data = (struct taro_data *)filp->private_data;
	struct pr_info *pinfo = &data->pinfo;

	switch (cmd) {
	case IOCMD_SET_FIRENUM:
		pinfo->fire_number = arg;
		break;
	case IOCMD_SET_PAGECNT:
		pinfo->page_number = arg;
		break;

	case IOCMD_SET_HBCNT:
		pinfo->blk_size = 4096 * (arg + 1);
		break;

	case IOCMD_PR_CTRL:
		switch (arg) {
		case PRCTRL_START:
			restart_printer(data);
			break;
		case PRCTRL_PAUSE:
			pause_printing(data);
			break;
		case PRCTRL_STOP:
			stop_printing(data);
			status_log(data, SL_JOB_CANCELED);
			break;
		case PRCTRL_RESUME:
			start_printing(data);
			break;

		default:
			return -EINVAL;
		}
		break;

	default:
		return -EINVAL;
	}

	return 0;
}

static struct file_operations taro_fops = {
	.owner		= THIS_MODULE,
	.open		= taro_open,
	.release	= taro_release,
	.write		= taro_write,
	.read		= taro_read,
	.mmap		= taro_mmap,
	.poll		= taro_poll,
	.unlocked_ioctl	= taro_ioctl,
};

static struct miscdevice misc_dev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "taro",
	.fops = &taro_fops,
};

/* ---------------------------------------------------------- */
static int taro_init(struct taro_data *data)
{
	int ret;

	data->fifo = fifo_alloc(TARO_BUFSIZE);
	if (!data->fifo) {
		pr_err("cannot allocate memory\n");
		return -ENOMEM;
	}

	init_waitqueue_head(&data->waitq);

	ret = request_irq(data->irq_xmit, taro_xmit_interrupt,
			IRQF_TRIGGER_LOW, NULL, NULL);
	if (ret) {
		pr_err("unable to request irq %d\n", data->irq_xmit);
		goto err_xmit_interrupt;
	}

	ret = request_irq(data->irq_stat, taro_stat_interrupt,
			IRQF_TRIGGER_RISING, NULL, NULL);
	if (ret) {
		pr_err("unable to request irq %d\n", data->irq_stat);
		goto err_sr_interrupt;
	}

	data->ioaddr = ioremap(0x08000000, 0xc000);
	if (data->ioaddr == NULL) {
		pr_err("failed to ioremap\n");
		ret = -ENOMEM;
		goto err_ioremap;
	}

	data->ram = data->ioaddr + 0x4000;
	return 0;

err_ioremap:
	free_irq(data->irq_stat, NULL);
err_sr_interrupt:
	free_irq(data->irq_xmit, NULL);
err_xmit_interrupt:
	fifo_free(data->fifo);
	return ret;
}

static void taro_exit(struct taro_data *data)
{
	free_irq(data->irq_stat, NULL);
	free_irq(data->irq_xmit, NULL);
	fifo_free(data->fifo);
	iounmap(data->ioaddr);
}


static int __init taro_init_module(void)
{
	struct taro_data *data;
	int ret;

	ret = load_firmware();
	if (ret)
		return ret;

	data = kzalloc(sizeof(struct taro_data), GFP_KERNEL);
	if (!data) {
		pr_err("taro: kzalloc failed\n");
		return -ENOMEM;
	}

	data->irq_xmit = gpio_to_irq(49);
	data->irq_stat = gpio_to_irq(15);
	ret = taro_init(data);
	if (ret) {
		kfree(data);
		return ret;
	}

	ret = misc_register(&misc_dev);
	if (ret) {
		pr_err("taro: misc_register failed\n");
		taro_exit(data);
		kfree(data);
		return ret;
	}

	data->dev = misc_dev.this_device;
	__data = data;
	dev_info(data->dev, "Loaded\n");
	return 0;
}

static void taro_cleanup_module(void)
{
	struct taro_data *data = __data;

	dev_info(data->dev, "Unloaded\n");
	misc_deregister(&misc_dev);
	taro_exit(data);
	kfree(data);
}


module_init(taro_init_module);
module_exit(taro_cleanup_module);

MODULE_AUTHOR("John Lee");
MODULE_DESCRIPTION("Taro driver");
MODULE_LICENSE("GPL");

