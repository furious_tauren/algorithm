/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * Mon May 18 09:41:33 CST 2015
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifdef __KERNEL__
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/sched.h>
#define FREE kfree
#else
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#define FREE free
#endif

#include "fifo.h"

#ifdef __KERNEL__
static void __fifo_in(struct fifo *fifo, const void *src,
			size_t len, size_t off)
{
	size_t l = min(len, fifo->size - off);

	memcpy(fifo->kdata + off, src, l);
	memcpy(fifo->kdata, src + l, len - l);
}

static void __fifo_out(struct fifo *fifo, void *des,
			size_t len, size_t off)
{
	size_t l = min(len, fifo->size - off);

	memcpy(des, fifo->kdata + off, l);
	memcpy(des + l, fifo->kdata, len - l);
}
#else
static void __fifo_in(struct fifo *fifo, const void *src,
			size_t len, size_t off)
{
	size_t l = min(len, fifo->size - off);

	memcpy(fifo->udata + off, src, l);
	memcpy(fifo->udata, src + l, len - l);
}

static void __fifo_out(struct fifo *fifo, void *des,
			size_t len, size_t off)
{
	size_t l = min(len, fifo->size - off);

	memcpy(des, fifo->udata + off, l);
	memcpy(des + l, fifo->udata, len - l);
}
#endif

size_t fifo_in(struct fifo *fifo, const void *src, size_t cnt)
{
	size_t space = fifo_space(fifo);
	if (cnt > space)
		cnt = space;

	if (src != NULL)
		__fifo_in(fifo, src, cnt, fifo->in);

	fifo->in = (fifo->in + cnt) % fifo->size;
	fifo->space -= cnt;

	return cnt;
}

/*
 * a spinlock could be a waste of the system,
 * but it makes this routine could be called in irq.
 */
size_t fifo_out(struct fifo *fifo, void *des, size_t cnt)
{
	size_t tmp = fifo_cnt(fifo);
	if (cnt > tmp)
		cnt = tmp;

	if (des != NULL)
		__fifo_out(fifo, des, cnt, fifo->out);

	/* take a warning, (fifo->out + cnt) could overflow */
	fifo->out = (fifo->out + cnt) % fifo->size;
	fifo->space += cnt;

	return cnt;
}

struct fifo *fifo_alloc(size_t size)
{
	struct fifo *fifo;

#ifdef __KERNEL__
	fifo = kzalloc(size + sizeof(struct fifo), GFP_DMA);
#else
	fifo = malloc(size + sizeof(struct fifo));
#endif
	if (!fifo)
		return NULL;
#ifdef __KERNEL__
	fifo->kdata = (void *)fifo + sizeof(struct fifo);
#else
	fifo->udata = (void *)fifo + sizeof(struct fifo);
#endif
	fifo->in = fifo->out = 0;
	fifo->size = fifo->space = size;

	return fifo;
}

void fifo_free(struct fifo *fifo)
{
	FREE(fifo);
}

#ifndef __KERNEL__
void fifo_init(struct fifo *fifo, void *data, size_t size)
{
	fifo->udata = data;
	fifo->size = fifo->space = size;
	fifo->in = fifo->out = 0;
}
#endif

