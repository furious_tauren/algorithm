/* ~.~ *-c-*
 *
 * Copyright (c) 2013, John Lee <furious_tauren@163.com>
 * Tue Mar 25 17:32:54 CST 2014
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __FIFO_H__
#define __FIFO_H__

#ifdef __KERNEL__
# include <linux/kernel.h>
#else
# include <sys/types.h>
#endif

#undef min
#define min(a, b) ((a) < (b) ? (a) : (b))

struct fifo {
	size_t		size;
	volatile size_t	space;
	volatile size_t	in;
	volatile size_t	out;

	void		*udata;
	void		*kdata;
};

static inline size_t fifo_space(struct fifo *fifo)
{
       return fifo->space;
}
static inline size_t fifo_cnt(struct fifo *fifo)
{
       return fifo->size - fifo->space;
}

static inline size_t fifo_space_d(struct fifo *fifo)
{
	return min(fifo_space(fifo), fifo->size - fifo->in);
}

static inline size_t fifo_cnt_d(struct fifo *fifo)
{
	return min(fifo_cnt(fifo), fifo->size - fifo->out);
}

#ifdef __KERNEL__
static inline void *fifo_iaddr(struct fifo *fifo)
{
	return fifo->kdata + fifo->in;
}
static inline void *fifo_oaddr(struct fifo *fifo)
{
	return fifo->kdata + fifo->out;
}
#else
static inline void *fifo_iaddr(struct fifo *fifo)
{
	return fifo->udata + fifo->in;
}
static inline void *fifo_oaddr(struct fifo *fifo)
{
	return fifo->udata + fifo->out;
}
#endif

static inline void fifo_reset(struct fifo *fifo)
{
	fifo->space = fifo->size;
	fifo->out = fifo->in = 0;
}


size_t fifo_in(struct fifo *fifo, const void *src, size_t cnt);
size_t fifo_out(struct fifo *fifo, void *des, size_t cnt);

void *fifo_iaddr(struct fifo *fifo);
void *fifo_oaddr(struct fifo *fifo);

struct fifo *fifo_alloc(size_t size);
void fifo_free(struct fifo *fifo);

#ifndef __KERNLE__
void fifo_init(struct fifo *fifo, void *data, size_t size);
#endif

#endif

