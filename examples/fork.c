#if 0
--------------------------------------------------
fork 奇妙之处是它仅仅被调用一次, 却能够返回两次,
它可能有三种不同的返回值:
　　1) 在父进程中, fork 返回新创建子进程的进程 ID
　　2) 在子进程中, fork 返回 0
　　3) 如果出现错误, fork 返回一个负值

fork 把进程当前的情况拷贝一份(变量, 代码, 堆栈, 寄存器)
子进程的 PC 指针等同于父进程, 两个进程都是在同一个位置
开始运行.

fork 子进程与父进程相同的文件描述符指向相同的文件表,
引用计数增加, 共享文件文件偏移指针.

--------------------------------------------------
Unix 用一个独特的方法, 它将进程创建与
加载一个新进程映象分离
int execl(const char *path, const char *arg, ...);

execl 系列函数会把当前进程替换掉
	execl("/bin/ls", "ls", "-l", (char *)0);
	printf("here should never reached\n");

--------------------------------------------------
当进程退出时, 它向父进程发送一个 SIGCHLD 信号,
默认情况下父进程总是忽略 SIGCHLD 信号, 此时进程
状态一直保留在内存中, 直到父进程使用 wait 函数收集
状态信息, 才会清空这些信息.

用wait来等待一个子进程终止运行称为回收进程.

当父进程忘了用 wait() 函数等待已终止的子进程时, 子进程就
会进入一种无父进程的状态,此时子进程就是僵尸进程.
--------------------------------------------------
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/wait.h>

int g = 12;

int system(const char * cmdstring)
{
	pid_t pid;
	int status;

	if (cmdstring == NULL)
		return (1);

	if ((pid = fork()) < 0) {
		status = -1;
	} else if (pid == 0) {
		execl("/bin/sh", "sh", "-c", cmdstring, (char *)0);
		_exit(127); /* 子进程正常执行则不会执行此语句 */
	} else {
		while (waitpid(pid, &status, 0) < 0) {

			if (errno != EINTR) {
				status = -1;
				break;
			}
		}
	}

	return status;
}

int main(void)
{
	pid_t fpid;
	int a, b;

	b = 15;
	fpid = fork();

	if (fpid < 0) {
		printf("error in fork!");
	} else if (fpid == 0) {
		a = 3;
	} else {
		a = 5;
	}

	printf("a=%d, b=%d, g=%d\n", a, b, g);
	return 0;
}
