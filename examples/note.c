#if 0
POSIX 是标准化组织推行的, 适合系统之间移植, 更简单一些.
而 System V 历史较为悠久, 接口比较复杂. 实现更全.
POSIX 是大势所趋.

在IPC, 进程间的消息传递和同步上, POSIX用得较普遍.
而在共享内存方面, POSIX实现尚未完善, system V 仍为主流.
--------------------------------------------------


在 POSIX.1 应用程序中, 幻数 0, 1, 2 应被代换成符号常数
STDIN_FILENO, STDOUT_FILENO 和 STDERR_FILENO. <unistd.h>

文件描述符的范围是 0 ~ OPEN_MAX, 现在很多系统将其增加至 63

lseek: 返回值是偏移值, 有可能时 0, 所以判断失败与否应判断
其返回值是否等于 -1
lseek 仅将当前的文件位移量记录在内核内, 它并不引起任何
I/O 操作.

read 成功则返回读到的字节数.
如已到达文件的尾端, 则返回 0(EOF)

      proc1               file table
      .-----------.     .----------.
 fd1: |     |     |     |  flags   |
      .-----+-----.     .----------.
 fd2: |a.bin|     | <-> |  offset  |
      .-----+-----.     .----------.
 fd3: |     |     |     |v-pointer | ------------.
      ^-----^-----^     ^----------^             |
                                                 |
      proc2               file table             v v-node tbl
      .-----------.     .----------.        .----------------.
 fd1: |     |     |     |  flags   |        |  v-node info   |
      .-----+-----.     .----------.        .----------------.
 fd2: |a.bin|     | <-> |  offset  |        |  i-node info   |
      .-----+-----.     .----------.        .----------------.
 fd3: |     |     |     |v-pointer | -----> |  file length   |
      ^-----^-----^     ^----------^        ^----------------^

the flags and offset in file table are different between
fds to the same inode, in different process.

O_APPEND 就使内核每次对这种文件进行写之前, 都将文件的
当前位移量设置到该文件的尾端处, 于是在每次写之前就不再
需要调用 lseek, 从而实现原子 write 操作.
--------------------------------------------------

TCP 为什么需要 "三次握手":
为了防止已失效的连接请求报文段突然又传送到了服务端.
--------------------------------------------------
流:
用于实际 I/O 的文件描述符,
指向流缓存的指针, 缓存的长度, 当前在缓存中的字符数,
出错标志等.

#include <stdio.h>
FILE *fopen(const char *pathname, const char *type);
FILE *freopen(const char *pathname, const char *type, FILE *fp);
FILE *fdopen(int filedes, const char *type);

freopen 在一个特定的流 fp 上打开一个指定的文件,
若该流已打开, 则先关闭该流.
此函数一般用于将一个指定的文件打开为一个预定义的流:
stdin stdout stderr

fdopen 取一个现存的文件描述符, 与一个 I/O 流相结合.

全缓存: 当填满标准 I / O缓存后才进行实际I / O操作
行缓存: 当在输入和输出中遇到新行符时, 执行I / O操作
无缓存: 标准 I / O库不对字符进行缓存
SVR4 4.3+B S D的系统默认使用下列类型的缓存:
• 标准出错是不带缓存的
• 如涉及终端设备的其他流, 则它们是行缓存的;
如果我们不喜欢系统默认, 则可调用下列函数更改缓存类型
void setbuf(FILE *fp, char *buf);
buf 须指向一个长度为 BUFSIZ 的缓存(在 <stdio.h> 中)
int setvbuf(FILE *fp, char* buf, int mode, size_t size);
_IOFBF:全缓存; _IOLBF:行缓存; _IONBF:不带缓存

函数 getchar 等同于 getc(stdin)
int getc(FILE *fp);
int fgetc(FILE *fp);
int putc(int c, FILE *fp);
int fputc(intc, FILE *fp);

char *fgets(char *buf, int n, FILE *fp);
int fputs(const char *str, FILE *fp);
int fprintf(FILE *fp, const char *format, ...);
int fscanf(FILE *fp, const char *format, ...);

int fileno(FILE *fp);

long ftell(FILE *fp);
int fseek(FILE *fp, long offset, int whence);
void rewind(FILE *fp);
--------------------------------------------------
#endif


int main(void)
{
	return 0;
}





