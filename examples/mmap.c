
#if 0
---------------------------------------------------
int open(const char *pathname, int flags, int perms)

flags(文件打开方式):
O_RDONLY O_WRONLY O_RDWR

O_CREAT: 如果改文件不存在, 就创建一个新的文件,
并用第三个参数为其设置权限

O_EXCL: 如果使用 O_CREAT 时文件存在, 则返回错误消息.
此时 open 是原子操作, 防止多个进程同时创建同一个文件


O_NOCTTY: 若文件为终端, 那么该终端不会成为调用
open() 的那个进程的控制终端

O_TRUNC: 若文件已经存在, 那么会删除文件中的全部
原有数据, 并且设置文件大小为 0

O_APPEND: 以添加方式打开文件, 在打开文件的同时,
文件指针指向文件的末尾, 即将写入的数据添加到文件
的末尾

O_NONBLOCK: 此文件的本次打开操作和后续的 I/O 操作
设置非阻塞方式.

O_SYNC: 使每次 write 都等到物理 I/O 操作完成才返回(写
入磁盘).

O_RSYNC: read 等待所有写入同一区域的写操作完成后再进行.

---------------------------------------------------
IPC 之 mmap: 使用信号量 sem_wait/sem_post 来保护临界区
int sem_init(sem_t *sem, int pshared, unsigned int value)
If pshared has the value 0, then the semaphore is  shared  between  the
threads  of  a  process,  and should be located at some address that is
visible to all threads (e.g., a global variable, or  a  variable  allo‐
cated dynamically on the heap).

If  pshared is nonzero, then the semaphore is shared between processes,
and should be located in a region of shared  memory

void* mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset)
	proc        file
	.-----.-h   .-----. ----
	|stack|     |     | offs
	.-----.     .-----. ----
	|mmap | <-> |mmap | len
	.-----.     .-----. ----
	|heap |     |     |
	.-----.     ^-----^
	|.bss |
	.-----.
	|.data|
	.-----.
	|.text|
	^-----^-l

	prot 参数指定共享内存的访问权限. 可取如下几个值的或:
	PROT_READ, PROT_WRITE, PROT_EXEC, PROT_NONE.

	flags: MAP_SHARED, MAP_PRIVATE, MAP_FIXED.
	offset 参数一般设为 0, 表示从文件头开始映射.

	addr 指定文件应被映射到进程空间的起始地址,
	一般被指定一个空指针, 此时选择起始地址的任务留给内核
	来完成.
mmap 后, 暂时脱离了磁盘上文件的影响. 只有在调用了 munmap 或
msync时, 才把内存中的相应内容写回磁盘文件.
---------------------------------------------------
#include <unistd.h>
int dup(int filedes);
int dup2(int filedes, int filedes2);
dup 返回当前可用文件描述符中的最小数值. dup2 返回 filedes2
如 filedes2 已打开, 则先将其关闭
返回的新文件描述符与参数 filedes 共享同一个文件表项
---------------------------------------------------

#endif


#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>

int main(int argc, char **argv)
{
	int fd;
	int i;
	struct stat sb;
	char *mapped;

	/* 打开文件 */
	if ((fd = open("/tmp/mmap", O_RDWR | O_CREAT, 00777)) < 0) {
		perror("open");
	}

	/* 获取文件的属性 */
	if ((fstat(fd, &sb)) == -1) {
		perror("fstat");
	}

	/* 将文件映射至进程的地址空间 */
	/* if ((mapped = (char *)mmap(NULL, sb.st_size, PROT_READ | */
	if ((mapped = (char *)mmap(NULL, 4096, PROT_READ |
			PROT_WRITE, MAP_SHARED, fd, 0)) == (void *)-1) {
		perror("mmap");
	}

	/* 映射完后, 关闭文件也可以操纵内存 */
	close(fd);

	/* 修改一个字符,同步到磁盘文件 */
	for (i = 0; i < 26; i++)
		mapped[i] = 'a' + i;
	mapped[26] = 0;

	puts(mapped);

	/* if ((msync((void *)mapped, sb.st_size, MS_SYNC)) == -1) { */
	if ((msync((void *)mapped, 4096, MS_SYNC)) == -1) {
		perror("msync");
	}

	/* 释放存储映射区 */
	/* if ((munmap((void *)mapped, sb.st_size)) == -1) { */
	if ((munmap((void *)mapped, 4096)) == -1) {
		perror("munmap");
	}

	return 0;
}
