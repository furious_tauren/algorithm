#if 0
--------------------------------------------------
int pthread_join(pthread_t tid, void **status);
主线程很快结束从而使整个进程结束, 创建的线程没机会执行
加入 pthread_join 后, 主线程会一直等待直到等待的线程结束

线程或者是可汇合的(joinable) 或者是脱离的(detached)
joinable: 终止时, 其线程 ID 和退出状态将保留, 直到另外
一个线程调用pthread_join

detached 终止时, 所有的资源都释放.
pthread_detach(pthread_self());
--------------------------------------------------
pthread_attr_t:
调度策略:
void pthread_attr_setschedpolicy(pthread_attr_t *attr, int policy);
SCHED_FIFO:	先进先出调度策略, 执行线程运行到结束
SCHED_RR:	轮询调度策略, 按时间片将每个线程分配到处理器上
SCHED_OTHER:	另外的调度策略(根据实现定义), 默认调度策略

设置优先级:
pthread_attr_setschedparam(pthread_attr_t *, struct sched_param *);
SCHED_FIFO 或 SCHED_RR 时有效


设置分离状态:
pthread_attr_setdetachstate(pthread_attr_t *attr, int detachstate)
PTHREAD_CREATE_DETACHED 或 PTHREAD _CREATE_JOINABLE

pthread_attr_destroy:
The thread attribute is copied in pthread_create, so it should be
destroyed after pthread_create call
--------------------------------------------------
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>

void *foo(void *arg)
{
	printf("hello, thread\n");
	pthread_exit(NULL);
}

int main(void)
{
	pthread_attr_t attr;
	pthread_t tid;
	struct sched_param param;
	int newprio = 20;

	/* 初始化属性i */
	pthread_attr_init(&attr);

 	pthread_attr_setschedpolicy(&attr, SCHED_RR);
	pthread_attr_getschedparam(&attr, &param);
	param.sched_priority = newprio;
	pthread_attr_setschedparam(&attr, &param);
	pthread_create(&tid, &attr, (void *)foo, NULL);
	pthread_attr_destroy(&attr);
	pthread_join(tid, NULL);

	return 0;
}
