/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * Thu Aug 31 11:03:20 CST 2017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>    
#include <string.h>
#include <termios.h>    

#include <sys/types.h>    
#include <sys/stat.h>    
#include <fcntl.h>    
#include <errno.h>      
#include <sys/time.h>    

// for rs485
#include <linux/serial.h>
#include <sys/ioctl.h>

#define RETRIES 5

static int
serial_options(int fd, speed_t baudrate, int dbits, int sbits, int parity) 
{ 
	struct termios newtio;   
	int rc;

	rc = tcgetattr(fd, &newtio);     
	if (rc) {
		fprintf(stderr, "tcgetattr");
		return rc;
	}

	newtio.c_cflag &= ~CSIZE;     
	newtio.c_cflag |= dbits; 

	switch (parity) {   
	case 'n': 
	case 'N':    
		newtio.c_cflag &= ~PARENB;   
		newtio.c_iflag &= ~INPCK;     
		break;  
	case 'o':   
	case 'O':     
		newtio.c_cflag |= (PARODD | PARENB);   
		newtio.c_iflag |= INPCK;             
		break;  
	case 'e':  
	case 'E':   
		newtio.c_cflag |= PARENB;         
		newtio.c_cflag &= ~PARODD;        
		newtio.c_iflag |= INPCK;       
		break; 
	case 'S': 
	case 's':     
		newtio.c_cflag &= ~PARENB; 
		newtio.c_cflag &= ~CSTOPB;
		break;  
	default:   
		newtio.c_cflag &= ~PARENB;   
		newtio.c_iflag &= ~INPCK;     
		break;    
	} 

	switch (sbits) {   
	case 1:    
		newtio.c_cflag &= ~CSTOPB;   
		break;  
	case 2:    
		newtio.c_cflag |= CSTOPB;   
		break; 
	default:  
		newtio.c_cflag &= ~CSTOPB;  
		break;  
	} 

	newtio.c_cc[VTIME]	= 0;    
	newtio.c_cc[VMIN]	= 0; 
	newtio.c_cflag	|= (CLOCAL | CREAD); 
	newtio.c_oflag	|= OPOST; 
	/*
	 * In canonical mode, input is processed when a new line
	 * character is received.
	 */
	newtio.c_lflag &= ~ICANON;

	/* If this bit is set, sent characters will be echoed back.
	 * Because we disabled canonical mode, I don’t think these
	 * bits actually do anything, but it doesn’t harm to
	 * disable them just in case!
	 */
	newtio.c_lflag &= ~(ECHO|ECHOE|ECHONL);

	/* Clearing IXOFF, IXON and IXANY disables software flow
	 * control, which we don’t want:
	 */
	newtio.c_iflag	&= ~(IXON | IXOFF | IXANY);                     
	/* Clearing all of the following bits disables any special
	 * handling of the bytes as they are received by the
	 * serial port, before they are passed to the application.
	 * We just want the raw data thanks!
	 */
	newtio.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);

	cfsetispeed(&newtio, baudrate);   
	cfsetospeed(&newtio, baudrate);   
	tcflush(fd, TCIFLUSH); 
	rc = tcsetattr(fd, TCSANOW, &newtio);
	if (rc == -1)
		fprintf(stderr, "tcsetattr");

	return rc; 
}


static int serial_init(void)
{
	int fd;
	char pathname[16];

	sprintf(pathname, "/dev/ttyS1");
	fd = open(pathname, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd == -1) {
		fprintf(stderr, "open faided\n");
		return -1;
	}

	if (serial_options(fd, B115200, CS8, 1, 'n')) {
		close(fd);
		return -1;
	}

	return fd;
}

int main(void)
{
	int n;
	int fd;
	fd_set rd;
	char buf[256];
	FILE *fp;

	if ((fd = serial_init()) < 0)
		return -1;

	fp = fopen("adis_log", "w");
	if (!fp) {
		close(fd);
		goto out;
	}
	struct serial_rs485 rs485conf = {0};
	rs485conf.flags |= SER_RS485_ENABLED;
	if (ioctl (fd, TIOCSRS485, &rs485conf) < 0) {
		printf("ero:\n");
		goto out;
	}
	const char *str= "hello";
	for (int i = 0; i < 3; i++) {
		write(fd, str, 5);
		getchar();

	}
	return 0;

	while (1) {
		int i;
		short gx, gy, gz, ax, ay, az;

		FD_ZERO(&rd);
		FD_SET(fd, &rd);

		while(FD_ISSET(fd, &rd)) {
			if(select(fd+1, &rd , NULL, NULL, NULL) < 0) {
				perror("select error!\n");
			} else {
				i = 0;
				while((n = read(fd, &buf[i], 1)) > 0) {
					if (buf[i] == '\n') {
						buf[i + 1] = 0;
						sscanf("%d%d%d%d%d%d", &gx, &gy, &gz, &ax, &ay, &az);
						printf("%d, %d, %d, %d, %d, %d\n",
								gx, gy, gz, ax, ay, az);
					}

				}
			}
		}
	}

out:
	close(fd);
	return 0;
}




