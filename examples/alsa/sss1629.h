/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2015年 12月 21日 星期一 16:45:29 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __SSS1629_H__
#define __SSS1629_H__

#include <linux/types.h>
#include <stdio.h>

#include <alsa/asoundlib.h>
#include "volume_mapping.h"

#undef alsa_error
/* rc should be defined as local variable to assure thread safety */
#define alsa_error(fmt, args...) do { \
	fprintf(stderr, fmt "(%s)\n",  ##args, snd_strerror(rc)); \
} while (0)

/* 混响初始化函数, 成功返回０ */
int mixer_init(void);

/* usbraw 接口, 函数底层 io 函数, 用于调试 */
int hidraw_write(__u16 addr, __u8 value);
int hidraw_read(__u16 addr);

int i2c_read8(__u8 addr, __u8 offset);
int i2c_write8(__u8 addr, __u8 offset, __u16 value);

/* eeprom io 函数, 咱们的应用场景未必用的到 */
int eeprom_write(__u8 offset, const __u16 *buf, size_t len);
int eeprom_read(__u8 offset, __u16 *buf, size_t len);

/* 录音初始化　*/
int capture_init(void);
void capture_exit(void);


/* 开始录音, frames为录音帧数 */
int capture_start(FILE *fp, size_t frames);
void capture_stop(FILE *fp);

/* 音量设置　0-100 */
int playback_set_volume(int channel, long volume);
int capture_set_volume(int channel, long volume);


/* 录音要保存成　wav 格式，则需要这两个函数 */
void wav_head(FILE *fp);
void wav_format(FILE *fp, unsigned long len);

/* 混响初始化　*/
int mixer_init(void);
void mixer_exit(void);

/* 混响设置
 *
 * i2c_write8(0x2e, 0x00, val);
 * val 取值范围为 0-128
 */

#endif


