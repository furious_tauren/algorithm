/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2015年 12月 25日 星期五 09:45:21 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <poll.h>

#include "sss1629.h"

static struct capture {
	snd_pcm_t		*handle;
	snd_pcm_uframes_t	frames;	/* how many frames per period */
	unsigned		period;	/* us */
	int			start;

	size_t			buffersize;
	short			*buffer;
} __cap;

int capture_init(void)
{
	int rc;
	snd_pcm_hw_params_t *params;
	snd_pcm_sw_params_t *swparams;
	snd_pcm_info_t *info;
	snd_pcm_uframes_t frames;
	unsigned sample_rate;

	__cap.buffersize = 4096;
	__cap.buffer = (short *)malloc(__cap.buffersize * sizeof(short) * 2);
	if (__cap.buffer == NULL) {
		fprintf(stderr, "cannot malloc space for read buffer\n");
		return -1;
	}

	snd_pcm_info_alloca(&info);

	rc = snd_pcm_open(&__cap.handle, "default", SND_PCM_STREAM_CAPTURE, 0);
	if (rc < 0) {
		alsa_error("cannot open audio device");
		free(__cap.buffer);
		return rc;
	}

	rc = snd_pcm_info(__cap.handle, info);
	if (rc < 0) {
		alsa_error("info error");
		goto err_out;
	}


	snd_pcm_hw_params_alloca(&params);
	snd_pcm_sw_params_alloca(&swparams);

	/* Fill it in with default values. */
	rc = snd_pcm_hw_params_any(__cap.handle, params);
	if (rc < 0) {
		alsa_error("cannot initialize hardware parameter");
		goto err_out;
	}

	/* Interleaved mode */
	rc = snd_pcm_hw_params_set_access(
			__cap.handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
	if (rc < 0) {
		alsa_error("cannot set access type");
		goto err_out;
	}

	rc = snd_pcm_hw_params_set_format(
			__cap.handle, params, SND_PCM_FORMAT_S16_LE);
	if (rc < 0) {
		alsa_error("cannot set sample format");
		goto err_out;
	}

	/* Set sample rate to 44100 */
	sample_rate = 44100;
	rc = snd_pcm_hw_params_set_rate_near(
			__cap.handle, params, &sample_rate, 0);
	if (rc < 0) {
		alsa_error("cannot set sample rate");
		goto err_out;
	}

	rc = snd_pcm_hw_params_set_channels(__cap.handle, params, 2);
	if (rc < 0) {
		alsa_error("cannot set channel count");
		goto err_out;
	}

	/* Set period size to 32 frames. */
	frames = 32;
	rc = snd_pcm_hw_params_set_period_size_near(
			__cap.handle, params, &frames, 0);
	if (rc < 0) {
		alsa_error("cannot set period size");
		goto err_out;
	}

	/* Write the parameters to the driver */
	rc = snd_pcm_hw_params(__cap.handle, params);
	if (rc < 0) {
		alsa_error("cannot set parameters");
		goto err_out;
	}

	/* Use a buffer large enough to hold one period */
	rc = snd_pcm_hw_params_get_period_size(params, &__cap.frames, 0);
	if (rc < 0) {
		alsa_error("cannot get period size");
		goto err_out;
	}

	rc = snd_pcm_hw_params_get_period_time(params, &__cap.period, 0);
	if (rc < 0) {
		alsa_error("cannot get period time");
		goto err_out;
	}

	/*
	 * tell ALSA to wake us up whenever __cap.buffersize or
	 * more frames of data can be delivered
	 * also, tell ALSA that we'll start the device ourselves
	 */
	rc = snd_pcm_sw_params_current(__cap.handle, swparams);
	if (rc < 0) {
		alsa_error("cannot initialize software parameters structure");
		goto err_out;
	}

	rc = snd_pcm_sw_params_set_avail_min(
			__cap.handle, swparams, __cap.buffersize);
	if (rc < 0) {
		alsa_error("cannot set minimum available count");
		goto err_out;
	}

	rc = snd_pcm_sw_params_set_start_threshold(__cap.handle, swparams, 0U);
	if (rc < 0) {
		alsa_error("cannot set start mode");
		goto err_out;
	}

	rc = snd_pcm_sw_params(__cap.handle, swparams);
	if (rc < 0) {
		alsa_error("cannot set software parameters");
		goto err_out;
	}

	rc = snd_pcm_prepare(__cap.handle);
	if (rc < 0) {
		alsa_error("can't recovery from suspend");
		goto err_out;
	}

	return 0;

err_out:
	/* close the specified pcm handle and frees all associated resources */
	snd_pcm_close(__cap.handle);
	free(__cap.buffer);
	return rc;
}

static int xrun(void)
{
	snd_pcm_status_t *status;
	int rc;
	
	snd_pcm_status_alloca(&status);
	rc = snd_pcm_status(__cap.handle, status);
	if (rc < 0) {
		alsa_error("status error");
		return rc;
	}

	if (snd_pcm_status_get_state(status) == SND_PCM_STATE_XRUN) {
		rc = snd_pcm_prepare(__cap.handle);
		if (rc < 0) {
			alsa_error("xrun: prepare error");
			return rc;
		}
		return 0;	/* ok, data should be accepted again */
	}

	if (snd_pcm_status_get_state(status) == SND_PCM_STATE_DRAINING) {
		fprintf(stderr, "format changed? attempting recover...\n");
		rc = snd_pcm_prepare(__cap.handle);
		if (rc < 0) {
			alsa_error("xrun(DRAINING): prepare error");
			return rc;
		}
		return 0;
	}

	return -1;
}

static int suspend(void)
{
	int rc;

	while ((rc = snd_pcm_resume(__cap.handle)) == -EAGAIN)
		sleep(1);	/* wait until suspend flag is released */

	if (rc < 0) {

		rc = snd_pcm_prepare(__cap.handle);
		if (rc < 0) {
			alsa_error("suspend: prepare error");
			return rc;
		}
	}

	return 0;
}

static ssize_t pcm_read(unsigned char *data, size_t count)
{
	ssize_t rc;
	size_t result = 0;

	if (count != __cap.chunk_size) {
		count = __cap.chunk_size;
	}

	while (count > 0 && !in_aborting) {
		rc = snd_pcm_readi(__cap.handle, data, count);
		if (rc == -EAGAIN || (rc >= 0 && (size_t)rc < count)) {
			snd_pcm_wait(handle, 100);

		} else if (rc == -EPIPE) {
			if ((rc = xrun()) < 0)
				break;

		} else if (rc == -ESTRPIPE) {
			if ((rc = suspend()) < 0)
				break;

		} else if (rc < 0) {
			alsa_error("read error");
			break;
		}

		/*
		 * if (vumeter)
		 *         compute_max_peak(data, rc * hwparams.channels);
		 */
		result += rc;
		count -= rc;
		data += rc * bits_per_frame / 8;
	}

	if (result > 0)
		return result;

	return rc;
}

int capture_start(FILE *fp, size_t frames)
{
	snd_pcm_sframes_t avail, alread = 0;
	ssize_t size;
	int rc;

	if (__cap.start) {
		fprintf(stderr, "capture alread started\n");
		return -1;
	}

	__cap.start = 1;
	while (frames > 0 && __cap.start) {

		/*
		 * wait till the interface is ready for data,
		 * or 100ms has elapsed.
		 */
		rc = snd_pcm_wait(__cap.handle, 100);
		if (rc < 0) {
			if (xrun_recovery(__cap.handle, rc) < 0)
				break;

			continue;
		}

		/* find out how much space is available */
		avail = snd_pcm_avail_update(__cap.handle);
		if (avail < 0) {
			rc = avail;
			if (xrun_recovery(__cap.handle, rc) < 0) {
				alsa_error("snd_pcm_avail_update failed");
				break;
			}
			continue;
		}
		printf("avail(%x)\n", avail);

		if (avail > __cap.buffersize)
			avail = __cap.buffersize;

		if (avail > frames)
			avail = frames;
		/* printf("avail(%x)\n", avail); */

		rc = snd_pcm_readi(__cap.handle, __cap.buffer, avail);
		if (rc < 0) {
			if (xrun_recovery(__cap.handle, rc) < 0)
				break;
			continue;
		}

		printf("rc(%x)\n", rc);
		size = snd_pcm_frames_to_bytes(__cap.handle, rc);
		printf("size(%x)\n", size);
		fwrite(__cap.buffer, 1, size, fp);
		alread += rc;
		frames -= rc;
		printf("alread(%x),frames(%x)\n", alread, frames);
	}

	__cap.start = 0;

	if (alread > 0)
		rc = alread;
	return rc;
}

void capture_stop(FILE *fp)
{
	if (__cap.start) {
		__cap.start = 0;
		fclose(fp);
	}
}

void capture_exit(void)
{
	snd_pcm_close(__cap.handle);
	free(__cap.buffer);
}

