/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2015年 12月 11日 星期五 08:57:16 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include <dirent.h> 

#include "sss1629.h"

#define VID 0x0c76
#define PID 0x161f
#define HIDRAW_PATH "/dev"

#define I2C_CTL_REG	0x0B
#define I2C_DAT_HI	0x0A
#define I2C_DAT_LO	0x09

#define AT24C08_SIZE	1024

static int hidraw_fd = -1;

static const char *bus_str(int bus)
{
	switch (bus) {
	case BUS_USB:
		return "USB";
		break;
	case BUS_HIL:
		return "HIL";
		break;
	case BUS_BLUETOOTH:
		return "Bluetooth";
		break;
	case BUS_VIRTUAL:
		return "Virtual";
		break;
	default:
		return "Other";
		break;
	}
}

static int hidraw_open(int vid, int pid) 
{ 
	struct dirent *ent; 
	DIR *dir; 

	dir = opendir(HIDRAW_PATH);

	while ((ent = readdir(dir)) != NULL) { 
		int res;
		struct hidraw_devinfo info;
		char name[64];

		if (strncmp(ent->d_name, "hidraw", 6) != 0)
			continue;

		sprintf(name, "%s/%s", HIDRAW_PATH, ent->d_name);
		hidraw_fd = open(name, O_RDWR);
		if (hidraw_fd < 0) {
			perror("Unable to open device");
			goto err_out;
		}

		res = ioctl(hidraw_fd, HIDIOCGRAWINFO, &info);
		if (res < 0) {
			perror("HIDIOCGRAWINFO");
		} else if (info.vendor == vid && info.product == pid) {
			closedir(dir);
			printf("found: %s\n", name);
			return hidraw_fd;
		}

		close(hidraw_fd);
	} 

err_out:
	closedir(dir);
	return -1;
}

static void hidraw_info(void)
{
	struct hidraw_report_descriptor rpt_desc;
	struct hidraw_devinfo info;
	char buf[256];
	int res;
	int desc_size = 0;

	memset(&rpt_desc, 0x0, sizeof(rpt_desc));
	memset(&info, 0x0, sizeof(info));
	memset(buf, 0x0, sizeof(buf));

	res = ioctl(hidraw_fd, HIDIOCGRDESCSIZE, &desc_size);
	if (res < 0)
		perror("HIDIOCGRDESCSIZE");
	else
		printf("Report Descriptor Size: %d\n", desc_size);


	rpt_desc.size = desc_size;
	res = ioctl(hidraw_fd, HIDIOCGRDESC, &rpt_desc);
	if (res < 0) {
		perror("HIDIOCGRDESC");
	} else {
		int i;

		printf("Report Descriptor:\n");
		for (i = 0; i < rpt_desc.size; i++)
			printf("%hhx ", rpt_desc.value[i]);
		puts("\n");
	}

	/* Get Raw Name */
	res = ioctl(hidraw_fd, HIDIOCGRAWNAME(256), buf);
	if (res < 0)
		perror("HIDIOCGRAWNAME");
	else
		printf("Raw Name: %s\n", buf);

	/* Get Physical Location */
	res = ioctl(hidraw_fd, HIDIOCGRAWPHYS(256), buf);
	if (res < 0)
		perror("HIDIOCGRAWPHYS");
	else
		printf("Raw Phys: %s\n", buf);

	/* Get Raw Info */
	res = ioctl(hidraw_fd, HIDIOCGRAWINFO, &info);
	if (res < 0) {
		perror("HIDIOCGRAWINFO");
	} else {
		printf("Raw Info:\n");
		printf("\tbustype: %d (%s)\n",
				info.bustype, bus_str(info.bustype));
		printf("\tvendor: 0x%04hx\n", info.vendor);
		printf("\tproduct: 0x%04hx\n", info.product);
	}
}

int hidraw_write(__u16 addr, __u8 value)
{
	__u8 buf[5];
	int res;

	buf[0] = 0x00;
	buf[1] = 0x60;
	buf[2] = addr & 0xff;
	buf[3] = addr >> 8;
	buf[4] = 0x10;
	res = write(hidraw_fd, buf, 5);
	if (res < 0)
		goto err_out;

	buf[2] = value;
	buf[3] = 0x01;	/* do not care */
	buf[4] = 0x20;
	res = write(hidraw_fd, buf, 5);
	if (res < 0)
		goto err_out;

	return 0;

err_out:
	perror("write");
	return res;
}

int hidraw_read(__u16 addr)
{
	__u8 buf[16];
	int res;

	/* Send a Report to the Device */
	buf[0] = 0x00;	/* Report Number must be zero */
	buf[1] = 0x60;	/* Internal register control by host */
	buf[2] = addr&0xff;
	buf[3] = addr>>8;
	buf[4] = 0x10;
	res = write(hidraw_fd, buf, 5);
	if (res < 0) {
		perror("write");
		return res;
	}

	/* Get a report from the device */
	res = read(hidraw_fd, buf, 16);
	if (res < 0) {
		perror("read");
		return res;
	} else if (res < 4) {
		printf("\nthe read data is wrong\n");
		return -1;
	}

	return buf[1];
}

static int i2c_init(void)
{
	int val = hidraw_read(0x100);
	if (val < 0) {
		return val;
	} else if (!(val&0x1))
		hidraw_write(0x100, val | 0x1);

	return 0;
}

int i2c_read8(__u8 addr, __u8 offset)
{
	__u8 ctl;
	int val;

	val = hidraw_read(0x110);
	if (val != addr)
		hidraw_write(0x110, addr);

	val = hidraw_read(0x08);
	val |= 1<<2;
	hidraw_write(0x08, val);

	/* Set start&read bits */
	ctl = (offset & 0x3f) | 1<<7;
	hidraw_write(I2C_CTL_REG, ctl);

	val = hidraw_read(I2C_CTL_REG);
	if (val < 0 || val & 1<<7)
		return -1;

	return hidraw_read(I2C_DAT_LO);
}

int i2c_write8(__u8 addr, __u8 offset, __u16 value)
{
	__u8 ctl;
	int val;

	val = hidraw_read(0x110);
	if (val != addr)
		hidraw_write(0x110, addr);

	val = hidraw_read(0x08);
	val |= 1<<2;
	hidraw_write(0x08, val);

	/* Set the byte to write */
	hidraw_write(I2C_DAT_LO, value & 0xff);

	/* Set start&write bits */
	ctl = (offset & 0x3f) | 3<<6;
	hidraw_write(I2C_CTL_REG, ctl);

	val = hidraw_read(I2C_CTL_REG);
	if (val < 0 || val & 1<<7)
		return -1;

	return 0;
}

int eeprom_write(__u8 offset, const __u16 *buf, size_t len)
{
	int idx = 0; 

	int val = hidraw_read(0x110);
	if (val != 0x50)
		hidraw_write(0x110, 0x50);

	val = hidraw_read(0x08);
	if (val | 1<<2)
		hidraw_write(0x08, val & ~(1<<2));

	while (len--) {
		__u8 ctl;

		hidraw_write(I2C_DAT_HI, buf[idx]>>8);
		hidraw_write(I2C_DAT_LO, buf[idx] & 0xff);

		ctl = (offset & 0x3f) | 3<<6;
		hidraw_write(I2C_CTL_REG, ctl);

		val = hidraw_read(I2C_CTL_REG);
		if (val & 1<<7)
			break;

		idx++;
		offset++;
	}

	return idx;
}

int eeprom_read(__u8 offset, __u16 *buf, size_t len)
{
	int val;
	int idx = 0;

	val = hidraw_read(0x110);
	if (val != 0x50)
		hidraw_write(0x110, 0x50);

	val = hidraw_read(0x08);
	if (val | 1<<2)
		hidraw_write(0x08, val & ~(1<<2));

	while (len--) {
		__u8 ctl;

		ctl = (offset & 0x3f) | 1<<7;
		hidraw_write(I2C_CTL_REG, ctl);

		val = hidraw_read(I2C_CTL_REG);
		if (val & 1<<7)
			break;

		buf[idx] = hidraw_read(I2C_DAT_LO) & 0xff;
		buf[idx] |= hidraw_read(I2C_DAT_HI) << 8;

		idx++;
		offset++;
	} 

	return idx;
}


/* 0-100 */
static int __set_volume(snd_mixer_selem_channel_id_t c, long volume, int flag)
{
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;
	snd_mixer_elem_t* elem;
	double value;
	int rc;

	const char *card = "default";
	const char *play_elem_name = "Speaker";
	const char *mic_elem_name = "Mic";

	rc = snd_mixer_open(&handle, 0);
	if (rc < 0) {
		alsa_error("failed to open mixer");
		return rc;
	}

	rc = snd_mixer_attach(handle, card);
	if (rc < 0) {
		alsa_error("failed to attach CTL to mixer");
		goto err_out;
	}

	rc = snd_mixer_selem_register(handle, NULL, NULL);
	if (rc < 0) {
		alsa_error("cannot register mixer element class");
		goto err_out;
	}

	rc = snd_mixer_load(handle);
	if (rc < 0) {
		alsa_error("cannot load mixer elements");
		goto err_out;
	}

	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	if (flag == 1)
		snd_mixer_selem_id_set_name(sid, play_elem_name);
	else
		snd_mixer_selem_id_set_name(sid, mic_elem_name);
	elem = snd_mixer_find_selem(handle, sid);
	if (elem == NULL) {
		fprintf(stderr, "cannot find the element\n");
		rc = -ENOENT;
		goto err_out;
	}

	value = volume / 100.0;
	if (flag == 1) {
		set_normalized_playback_volume(elem, c, value, 0);
	} else {
		set_normalized_capture_volume(elem, c, value, 1);
	}

err_out:
	/* close a mixer and release all related resources */
	snd_mixer_close(handle);
	return rc;
}

int playback_set_volume(int channel, long volume)
{
	snd_mixer_selem_channel_id_t c;
	c = (snd_mixer_selem_channel_id_t)channel;

	return __set_volume(c, volume, 1);
}

int capture_set_volume(int channel, long volume)
{
	snd_mixer_selem_channel_id_t c;
	c = (snd_mixer_selem_channel_id_t)channel;

	return __set_volume(c, volume, 0);
}

void wav_head(FILE *fp)
{
	const __u8 buf[4] = {0};

	fwrite("RIFF", 4, 1, fp);	/* Byte 0..3 RIFF */
	fwrite(buf, 4, 1, fp);		/* Byte 4..7 file size - 4*/
	fwrite("WAVE", 4, 1, fp);	/* Byte 8..11 WAVE */
	fwrite("fmt ", 4, 1, fp);	/* Byte 12..15 fmt */
	fputc(16, fp);			/* Byte 16..19 format chunk length (16 bytes) */
	fwrite(buf, 3, 1, fp);		/* Byte 4..7 file size - 4*/
	fputc(1, fp);			/* Byte 20..21 compression code (1=PCM) */
	fputc(0, fp);
	fputc(2, fp);			/* Byte 22..23 Number of channels (2) */
	fputc(0, fp);
	fwrite(buf, 4, 1, fp);		/* Byte 24..27 Sample Rate (actual value later from decoder) */
	fwrite(buf, 4, 1, fp);		/* Byte 28..31 Average Bytes/Second (actual value later from decder) */
	fputc(2, fp);			/* Byte 32..33 Block Align (4 -- 2 bytes/channel * 2 channels) */
	fputc(0, fp);
	fputc(16, fp);			/* Byte 34..35 Bits Per Sample (16) */
	fputc(0, fp);
	fwrite("data", 4, 1, fp);	/* Byte 36..39 data */
	fwrite(buf, 4, 1, fp);		/* Byte 40..43 data size - 4*/
}

void wav_format(FILE *fp, unsigned long len)
{
	/*XXX fixed to 44100 */
	unsigned long sample_rate = 44100;

	/* Need to write file size - 4 to this offset */
	fseek(fp, 4, SEEK_SET);
	len -= 4;
	fputc(len & 0xff, fp);
	fputc((len >> 8) & 0xff, fp);
	fputc((len >> 16) & 0xff, fp);
	fputc((len >> 24) & 0xff, fp);

	/* Need to write sample rate here */
	fseek(fp, 24, SEEK_SET);
	fputc(sample_rate & 0xff, fp);
	fputc((sample_rate>>8) & 0xff, fp);
	fputc((sample_rate>>16) & 0xff, fp);
	fputc((sample_rate>>24) & 0xff, fp);

	/* Need to write sampleRate * 4 here */
	sample_rate *= 4;
	fputc(sample_rate & 0xff, fp);
	fputc((sample_rate>>8) & 0xff, fp);
	fputc((sample_rate>>16) & 0xff, fp);
	fputc((sample_rate>>24) & 0xff, fp);

	/* Need to write data size (file size - 44) to this offset */
	fseek(fp, 40, SEEK_SET);
	len -= 40;
	fputc(len & 0xff, fp);
	fputc((len >> 8) & 0xff, fp);
	fputc((len >> 16) & 0xff, fp);
	fputc((len >> 24) & 0xff, fp);
}

int mixer_init(void)
{
	hidraw_fd = hidraw_open(VID, PID);
	if (hidraw_fd < 1) {
		fprintf(stderr, "can't open device\n");
		return -1;
	}
	hidraw_info();
	return i2c_init();
}

void mixer_exit(void)
{
	if (hidraw_fd != -1)
		close(hidraw_fd);
}
	

int main(int argc, char **argv)
{
	unsigned val;
#if 0
	char buf[1024 * 1024] = {0};
	int rc;
	FILE *fp;

	mixer_init();

	while (1) {
		__u8 tmp = i2c_read8(0x2e, 0x00);
		printf("mixer current value: 0x%x\n", tmp);
		printf("enter a new one: ");
		if (scanf("%u", &val) == EOF)
			break;
		i2c_write8(0x2e, 0x00, val);
	}
	fp = fopen("1.wav", "wb");
	if (!fp) {
		fprintf(stderr, "can't open file\n");
		return -1;
	}

	wav_head(fp);

#endif
#if 1
	mixer_init();

	while (1) {
		__u8 tmp = i2c_read8(0x2e, 0x00);
		printf("mixer current value: 0x%x\n", tmp);
		printf("enter a new one: ");
		if (scanf("%u", &val) == EOF)
			break;
		i2c_write8(0x2e, 0x00, val);
	}
#endif

#if 0
	rc = capture_init();
	if (rc < 0) {
		printf("wokaoa\n");
		fclose(fp);
		return -1;
	}

	rc = capture_start(buf, sizeof(buf));
	printf("rc=0x%x\n", rc);
	rc = fwrite(buf, 1, rc, fp);
	wav_format(fp, ftell(fp), 44100);
	/* mixer_exit(); */
	capture_exit();
	fclose(fp);
#endif

#if 0
	int rc = 0;
	FILE *fp;

	fp = fopen("1.wav", "wb");
	if (!fp) {
		fprintf(stderr, "can't open file\n");
		return -1;
	}

	wav_head(fp);

	/*
	 * while (scanf("%ld", &vol) != EOF) {
	 *         playback_set_volume(0, vol);
	 *         playback_set_volume(1, vol);
	 *         capture_set_volume(0, vol);
	 *         capture_set_volume(1, vol);
	 * }
	 */

	rc = capture_init();
	if (rc < 0) {
		printf("wokaoa\n");
	}

	rc = capture_start(fp, 1024 * 1024);
	wav_format(fp, ftell(fp));
	capture_stop(fp);
	capture_exit();
#endif





	/*
	 * __u16 wbuf[] = {
	 *         0x02c8, 0x17ac, 0xc815, 0x3ac8,
	 *         0x3e3a, 0x807f, 0x801a, 0xf7df,
	 * };
	 * int len;
	 * int i;
	 */
	/*
	 *         hidraw_fd = hidraw_open(VID, PID);
	 *         hidraw_info();
	 * 
	 *         i2c_init();
	 */
	/* i2c_write8(0x2e, 0x00, 0x2f); */
	/* eeprom_write16(0x00, 0x02c8); */
	/* printf("val=0x%x\n", i2c_read8(0x2e, 0x00)); */
	/*
	 * printf("val=0x%x\n", i2c_read8(0x50, 0x00));
	 * printf("val=0x%x\n", i2c_read8(0x50, 0x01));
	 * printf("val=0x%x\n", i2c_read8(0x50, 0x02));
	 * printf("val=0x%x\n", i2c_read8(0x50, 0x03));
	 */

	/* len = eeprom_write(0x00, wbuf, 8); */
#if 0
	len = eeprom_read(0x00, buf, 512);
	for (i = 0; i < len; i++) {
		printf("%04x  ", buf[i]);
		if ((i + 1) % 6 == 0)
			printf("\n");
	}
	printf("\n");
#endif

	/*
	 * val = hidraw_read(0x08);
	 * val |= 1<<2;
	 * hidraw_write(0x08, val);
	 * printf("value:0x%x\n", hidraw_read(0x00));
	 * printf("value:0x%x\n", hidraw_read(0x08));
	 * eeprom_write16(0, 0x00c8);
	 * printf("%x\n", eeprom_read16(0));
	 */


	/* close(hidraw_fd); */
	return 0;
}

