#include<unistd.h>  
#include<stdio.h>  
#include<string.h>  

#include<netinet/in.h>  
#include<sys/socket.h>  
#include <arpa/inet.h>

#include <sys/time.h>
#include <errno.h>

#include <sys/stat.h>
#include <fcntl.h>

//#define PORT 50081
#define PORT 22222


typedef struct  
{
	int                 channel;
	int         	    type;        //图像类型，获取时只该字段有效
	int                 enable;      //0-不使能，1-使能
	int                 zoom_pos;   //变倍位置
	int                 zoom_step;  //变倍步长
	int                 focus_pos;   //聚焦位置
	int                 focus_step;  //聚焦步长
	int		    reserved[64];
}F_MZOOMFOCUS, *PF_MZOOMFOCUS;
int main(int argc,char **argv)  
{  
	F_MZOOMFOCUS mz;
#if 1
	struct timeval starttime,endtime;
	double timeuse;
	int i, n, sock;  
	int fd;
	struct stat st;
	size_t size;
	struct sockaddr_in addr_ser;  
	/* fire number: 1000:312.5k */
	static unsigned char buf[1024 * 1024];

	sock = socket(AF_INET,SOCK_STREAM,0);  
	if(sock == -1) {  
		printf("socket error\n");  
		return -1;  
	}  

	bzero(&addr_ser, sizeof(addr_ser));  
	addr_ser.sin_family = AF_INET;  
	//addr_ser.sin_addr.s_addr = inet_addr("127.0.0.1");  
	addr_ser.sin_addr.s_addr = inet_addr("192.168.1.100");  
	addr_ser.sin_port = htons(PORT);  
	if (connect(sock, (struct sockaddr *)&addr_ser, sizeof(addr_ser)) < 0) {
		printf("connect failed\n");  
		goto err_out;
	}

	printf("connect with server...\n");  
	memset(&mz, 0, sizeof(mz));
	/* gettimeofday(&starttime, 0); */

#if 1
	while (1) {
		int i;
#if 0
		char str[128];
		fgets(str, sizeof(str), stdin);
		for (i = 0; str[i]; i++) {
			if (str[i] == '\n') {
				str[i] = 0;
				break;
			}
		}
#endif
		if (mz.type)
			mz.type = 0;
		else
			mz.type = 1;

		printf("write port(%d): \n", PORT);
		n = send(sock, &mz, sizeof(mz), 0);  
		if (n < 0) {
			printf("%s", strerror(errno));
		} else {
			int i;
			printf("read port(%d): \n", PORT);
			n = recv(sock, &mz, sizeof(mz), 0);  
			printf("------mz.type=%d\n", mz.type);
		}
		getchar();
	}
#else
	char pb[20] = {0};
	cmd_head_t *head = (cmd_head_t *)pb;
	head->len =  20 - 16;
	head->cmd = 1;
	n = send(sock, pb, sizeof(pb), 0);  
	getchar();
	printf("%d\n", i);




		
#endif
	/*
	 * gettimeofday(&endtime, 0);
	 * timeuse = 1000000 * (endtime.tv_sec - starttime.tv_sec) + endtime.tv_usec - starttime.tv_usec;
	 * printf("speed %lfm/s\n", buf[0] / 1024.0 / 1024 / timeuse * 1000000);
	 */

err_out:
	close(sock);
	return 0;  
#endif
}
