/* ~.~ *-c-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Thu Jun 30 17:23:23 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __IO_CMD_H__
#define __IO_CMD_H__

#define SL_PAGE_START		(1<<0)
#define SL_PAGE_END		(1<<1)
#define SL_DATA_LACK		(1<<3)

#define SL_JOB_CANCELED		(1<<5)
#define SL_JOB_START		(1<<6)
#define SL_JOB_END		(1<<7)




#define IOCMD_SET_PR_MODE	5
#define IOCMD_SET_FIRENUM	6
#define IOCMD_SET_PAGECNT	7
#define IOCMD_SET_HBCNT		8

#define IOCMD_PR_CTRL		10
#define PRCTRL_START		0
#define PRCTRL_PAUSE		1
#define PRCTRL_RESUME		2
#define PRCTRL_STOP		3

#define IOCMD_RESUME		11

#endif

