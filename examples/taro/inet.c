/* ~.~ *-c-*
 *
 * Copyright (c) 2013, John Lee <furious_tauren@163.com>
 * Wed May 27 07:24:33 CST 2015
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <io.h>
#include <debug.h>
#include <macro.h>
#include <ep.h>
#include <printer.h>
#include <sock.h>

#define RETRIES 5

struct inet {
	struct eplist	*list;
	int		fd;

	unsigned short	port;
	struct fifo	*rxfifo;

	int	(*on_init)(struct inet *inet);
	void	(*on_exit)(struct inet *inet);
	on_recv	on_recv;
};

static int is_skb_flushing;

static int nbread_loop(struct ep *ep)
{
	struct inet *inet = (struct inet *)ep->arg;
	struct fifo *fifo = inet->rxfifo;

	while (1) {
		ssize_t rc;
		ssize_t size = fifo_space_d(fifo);

		if (size == 0)
			break;

		rc = nbtcp_read(ep->fd, fifo_iaddr(fifo), size);
		if (rc < 0) {
			return rc;
		} else if (rc == 0) {
			break;
		}

		fifo_in(fifo, NULL, rc);
	}

	return 0;
}

static int data_recv(struct ep *ep)
{
	struct fifo *fifo = ((struct inet *)ep->arg)->rxfifo;
	struct printer_info *prinfo = to_prinfo();
	long long len = 0LL;

	if (is_skb_flushing)
		return nbread_loop(ep);

	if (nbread_loop(ep) < 0)
		return -1;

	if (prinfo->status == SR_START) {
		size_t size;

		len = prinfo->page_cnt;
		len *= prinfo->page_size;
		size = min(KFIFO_SIZE, len);

		if (fifo_cnt_d(fifo) < size)
			return 0;

		if (io_write(&len, sizeof(len)) < 0) {
			pr_info("%s: io_write failed\n", __func__);
			return 0;
		}

		prinfo->status = SR_PRINTING;
	}

	return 0;
}

/* -------------------------------------------------------- */
/*
 * some of the following functions would be used as ep
 * callbacks. the callbacks take one void* argument.
 * on success, the callbacks shoule return 0, otherwise, -1
 */
/* -------------------------------------------------------- */
/* -------------------------------------------------------- */
static int __acpt(struct ep *ep)
{
	int conn_fd;
	struct inet *inet = (struct inet *)ep->arg;

	conn_fd = nbtcp_accept(ep->fd);
	if (conn_fd <= 0)
		return 0;

	if (inet->rxfifo)
		fifo_reset(inet->rxfifo);

	if (epl_add_one(inet->list, conn_fd, inet->on_recv, inet)) {
		pr_info("%s :eplist_add_one failed\n", __func__);
		goto err_out;
	}

	inet->fd = conn_fd;
	return 0;

err_out:
	close(conn_fd);
	return 0;
}

/* -------------------------------------------------------- */
#define CFIFO_SIZE 0x100000
static int cport_init(struct inet *inet)
{
	int sfd = nbtcp_open(inet->port);
	if (sfd < 0)
		return sfd;

	inet->rxfifo = fifo_alloc(CFIFO_SIZE);
	if (inet->rxfifo == NULL) {
		perror("fifo_alloc");
		goto err_out;
	}

	if (epl_add_one(inet->list, sfd, __acpt, inet)) {
		pr_err("epl_add_one for listen ep");
		fifo_free(inet->rxfifo);
		close(sfd);
		return -1;
	}

	pr_info("%s: ep_fd(%d)\n", __func__, sfd);
	return sfd;

err_out:
	close(sfd);
	return sfd;
}

static void cport_exit(struct inet *inet)
{
	fifo_free(inet->rxfifo);
}

static int dport_init(struct inet *inet)
{
	int sfd = nbtcp_open(inet->port);
	if (sfd < 0)
		return sfd;

	inet->rxfifo = io_fifo();

	if (epl_add_one(inet->list, sfd, __acpt, inet)) {
		pr_err("epl_add_one for listen ep");
		close(sfd);
		return -1;
	}
	
	pr_info("%s: ep_fd(%d)\n", __func__, sfd);
	return sfd;
}


/* -------------------------------------------------------- */
static struct inet __inet[] = {

	[0] = {
		.port = 8001,
		.on_init = cport_init,
		.on_exit = cport_exit,
		.on_recv = nbread_loop,
	},

	[1] = {
		.port = 8002,
		.on_init = dport_init,
		.on_recv = data_recv,
	},
};


int inet_init(void)
{
	int i; 
	struct eplist *list;
	struct inet *inet = __inet;

	list = epl_alloc(20);
	if (!list)
		return -1;

	for (i = 0; i < ARRAY_SIZE(__inet); i++, inet++) {
		int sockfd;

		inet->list = list;
		sockfd = inet->on_init(inet);
		if (sockfd < 0)
			goto err_out;

	}

	if (epl_start(list)) {
		pr_err("epl_start");
		goto err_out;
	}

	return 0;

err_out:
	epl_free(list);
	inet = __inet;
	for (i = 0; i < ARRAY_SIZE(__inet); i++, inet++) {
		if (inet->on_exit)
			inet->on_exit(inet);
	}
	return -1;
}

int inet_read(int num, void *buf, size_t size)
{
	struct inet *inet = &__inet[num];
	struct fifo *fifo = inet->rxfifo;
	int retries = RETRIES;

	do {
		if (fifo_cnt(fifo) < size)
			usleep(10);
		else
			return fifo_out(fifo, buf, size);
	} while (--retries);

	return -1;
}

int inet_write(int num, const void *buf, size_t size)
{
	struct inet *inet = &__inet[num];
	ssize_t rc = 0;

	while (size > 0) {
		ssize_t cnt;

		cnt = write(inet->fd, buf, size);
		if (cnt == -1) {
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				usleep(100);
				continue;
			} else {
				if (rc == 0)
					rc = -1;
				break;
			}
		}

		size -= cnt;
		buf += cnt;
		rc += cnt;
	}

	return rc;
}

void clear_skb(void)
{
	struct inet *inet = &__inet[1];
	struct fifo *fifo = inet->rxfifo;
	int retries = 10000;

	is_skb_flushing = 1;
	do {
		size_t size = fifo_cnt(fifo);
		if (size)
			fifo_out(fifo, NULL, size);
		usleep(100);
	} while (--retries);
	is_skb_flushing = 0;

	fifo_reset(fifo);
}

