/* ~.~ *-h-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2016年 03月 23日 星期三 10:31:18 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __COMMAND_H__
#define __COMMAND_H__

#include <sys/types.h>

/* -------------------- |   ID     |     info      |sub class |   class  | */
/* -------------------- | [00:31]  |    [32:55]    | [56:59]  | [60:63]  | */
#define CMD_S_H_VOL	(0x00000018|0x000000ull<<32|0x1ull<<56|0x1ull<<60)
#define CMD_C_H_VCALI	(0x00000019|0x000000ull<<32|0x1ull<<56|0x1ull<<60)
#define CMD_S_H_VCALI	(0x0000001a|0x000000ull<<32|0x1ull<<56|0x1ull<<60)
#define CMD_S_H_WAVE	(0x0000001c|0x000000ull<<32|0x1ull<<56|0x1ull<<60)
#define CMD_S_H_TEMP	(0x0000001e|0x000000ull<<32|0x1ull<<56|0x1ull<<60)
#define CMD_S_H_SHAKE	(0x0000002c|0x000000ull<<32|0x1ull<<56|0x1ull<<60)
#define CMD_S_H_BVOL	(0x000000c1|0x000000ull<<32|0x1ull<<56|0x1ull<<60)

#define CMD_S_H_PWROFF	(0x0000002f|0x000000ull<<32|0x2ull<<56|0x1ull<<60)

#define CMD_G_H_BVOL	(0x000000c0|0x000000ull<<32|0x3ull<<56|0x1ull<<60)
#define CMD_G_H_WAVE	(0x000000c3|0x000000ull<<32|0x3ull<<56|0x1ull<<60)
#define CMD_G_H_TEMP	(0x0000001f|0x000000ull<<32|0x3ull<<56|0x1ull<<60)
#define CMD_G_H_FVER	(0x00000040|0x000000ull<<32|0x3ull<<56|0x1ull<<60)
#define CMD_G_H_AVER	(0x00000041|0x000000ull<<32|0x3ull<<56|0x1ull<<60)

struct ctbl;

struct command {
	unsigned	size;
	unsigned	type;
	unsigned	id;
	unsigned	seqnum;
	unsigned	data[0];
};

typedef ssize_t (*parser_t)(struct ctbl *, struct command *cmd);

struct ctbl {
	unsigned long	param0;
	unsigned long	param1;

	int		cmd;
	parser_t	pfunc;
};

int command_init(void);
void format_cmd(struct command *cmd, int id, void *buf, size_t size);

#endif

