/* ~.~ *-c-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Tue Jul  5 15:55:26 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>

#include <sys/socket.h>
#include <netinet/in.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include <netdb.h>
#include <ifaddrs.h>
#include <netpacket/packet.h>

#include <debug.h>
#include <macro.h>

int setnonblocking(int fd)
{
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
	if (flags == -1) {
		pr_err("fcntl");
		return -1;
	}

	flags |= O_NONBLOCK;
	flags = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
	if (flags == -1) {
		pr_err("fcntl");
		return -1;
	}

	return 0;
}

int get_hostname(char *host, const char *name, int family)
{
	struct ifaddrs *ifaddr, *ifa;

	if (getifaddrs(&ifaddr) == -1) {
		pr_err("getifaddrs");
		return -1;
	}

	/* Walk through linked list */
	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		size_t size;

		if (ifa->ifa_addr == NULL)
			continue;

		if (strcmp(name, ifa->ifa_name))
			continue;

		if (family != ifa->ifa_addr->sa_family)
			continue;

		if (ifa->ifa_addr->sa_family == AF_INET)
			size = sizeof(struct sockaddr_in);
		else if (ifa->ifa_addr->sa_family == AF_INET6)
			size = sizeof(struct sockaddr_in6);
		else
			size = sizeof(struct sockaddr_ll);

		if (getnameinfo(ifa->ifa_addr, size, host,
					NI_MAXHOST, NULL, 0, NI_NUMERICHOST)) {
			pr_err("getnameinfo failed");
			freeifaddrs(ifaddr);
			return -1;
		}

		return 0;
	}

	fprintf(stderr, "%s: comm not found\n", name);
	freeifaddrs(ifaddr);
	return -1;
}

int nbtcp_open(unsigned short port)
{
	int sfd;
	struct sockaddr_in sin;
	const int opt = 1;

	sfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sfd == -1)  {
		pr_err("socket error");
		return -1;
	}

	setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_port = htons(port);
	bzero(&sin.sin_zero, 8);

	if (setnonblocking(sfd) == -1)
		goto err_out;

	if (bind(sfd, (struct sockaddr *)&sin, sizeof(sin)) == -1) {
		pr_err("socket bind");
		goto err_out;
	}

	/* one conn_fd one ep */
	if (listen(sfd, 1) == -1) {
		pr_err("listen error");
		goto err_out;
	}

	return sfd;

err_out:
	close(sfd);
	return -1;
}

int nbtcp_accept(int sfd)
{
	int conn_fd;
	socklen_t len;
	struct sockaddr_in cliaddr;

	len = sizeof(cliaddr);
	memset(&cliaddr, 0, len);

	conn_fd = accept(sfd, (struct sockaddr *)&cliaddr, &len);
	if (conn_fd < 0) {
		/* we should ignore the following errors
		 * EAGAIN :no connection arrival
		 * ECONNABORTED/EPROTO :abort by client
		 * EINTR :some slow system call such as ccept would be
		 * 	  interrupted while handling a captured signal
		 */
		if (errno == EAGAIN || errno == EINTR
				|| errno == ECONNABORTED || errno == EPROTO) {
			return 0;
		} else {
			pr_err("accept failed");
			return -1;
		}
	}

	if (setnonblocking(conn_fd) < 0)
		goto err_out;

	return conn_fd;

err_out:
	close(conn_fd);
	return -1;

}

/* return: -1(should close socket), 0(should retry) */
int nbtcp_read(int sfd, void *buf, size_t size)
{
	ssize_t len = recv(sfd, buf, size, 0);
	if (len > 0) {
		return len;
	} else if (len < 0 && errno != EAGAIN && errno != EINTR) {
		pr_err("nbtcp_read");
		if (errno == EFAULT)
			return 0;

		return -1;
	} else if (len == 0) {
		pr_info("closed by client\n");
		return -1;
	}

	return 0;
}

/* return 0 ? */
int nbtcp_write(int sfd, const void *buf, size_t size)
{
	ssize_t len = send(sfd, buf, size, 0);
	if (len >= 0) {
		return len;
	} else if (len < 0 && errno != EAGAIN && errno != EINTR) {
		pr_err("nbtcp_write");
		return -1;
	}

	return 0;
}

int canbus_open(int num)
{
	int sfd;
	struct sockaddr_can addr;
	struct ifreq ifr;
	char name[8];
	can_err_mask_t err_mask;

	sfd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if(sfd < 0) {
		pr_err("socket error");
		return -1;
	}

	addr.can_family = PF_CAN;
	sprintf(name, "can%d", num);
	strcpy(ifr.ifr_name, name);

	if (ioctl(sfd, SIOCGIFINDEX, &ifr) < 0) {
		pr_err("ioctl");
		goto err_out;
	}

	err_mask = CAN_ERR_TX_TIMEOUT | CAN_ERR_LOSTARB
		| CAN_ERR_CRTL | CAN_ERR_ACK
		| CAN_ERR_BUSOFF;
	setsockopt(sfd, SOL_CAN_RAW,
			CAN_RAW_ERR_FILTER, &err_mask, sizeof(err_mask));

	addr.can_ifindex = ifr.ifr_ifindex;
	if (bind(sfd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		pr_err("bind");
		goto err_out;
	}

	return sfd;

err_out:
	close(sfd);
	return -1;
}

