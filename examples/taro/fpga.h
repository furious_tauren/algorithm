/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2016年 03月 23日 星期三 10:54:30 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __FPGA_H__
#define __FPGA_H__

int rice_init(void);

int rice_ioread32(unsigned offset);
void rice_iowrite32(unsigned data, unsigned offset);

void rice_setbits(unsigned bits, unsigned addr);
void rice_clrbits(unsigned bits, unsigned addr);
void rice_setclrbit(unsigned bit, unsigned addr);

void rice_reset(void);

#endif

