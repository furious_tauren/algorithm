/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * Sat Mar 19 11:52:15 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __PRINTER__
#define __PRINTER__

#include <linux/types.h>
#include <fifo.h>
#include <debug.h>
#include <macro.h>

#define KFIFO_SIZE 0x300000


/* ^@: bits[0 15]-wave, bit[16]-temp, bit[20]-volcali */
#define PARAM_MAGIC	0x5E400000
#define PARAM_MASK	0xFFFF0000

struct uv_led {
	__u32 num;
	__u32 speed[4];
	__u32 distance;
};

struct env_temp {
	__u32 num;

	struct {
		__u32 is_open;
		__u32 real_temp;
		__u32 setting_temp;
	} temp[4];
};

struct printer_param {
	__u32		magic;

	struct uv_led	uv_led;
	struct env_temp	env_temp;
};

#define param_off(x) offsetof(struct printer_param, x)

struct printer_param *to_param(void);
int save_param(off_t offset, int bit, void *buf, size_t size);
int read_param(off_t offset, void *buf, size_t size);
int load_param(void);
int read_eeprom(off_t offset, void *buf, size_t size);
int write_eeprom(off_t offset, void *buf, size_t size);

enum printer_status {
	SR_IDLE = 0,
	SR_STOPPING,
	SR_START,
	SR_PRINTING,
};

struct printer_info {
	enum printer_status	status;
	int			is_pwroff;
	float			cur_coord;
	float			idle_coord;
	__u32			version;
	__u32			printed;
	__u32			enc_xres;
	__u32			enc_speed;
	__u32			page_cnt;
	__u32			page_size;
	char			temp[2][0x68];
};

extern struct printer_info __prinfo;

static inline struct printer_info *to_prinfo(void)
{
	return &__prinfo;
}

#endif



