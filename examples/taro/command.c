/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2016年 03月 23日 星期三 10:30:15 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <pthread.h>

#include <command.h>
#include <io.h>
#include <inet.h>
#include <can.h>
#include <fpga.h>
#include <printer.h>
#include <LWCommunicatorCmdDefine.h>

struct printer_info __prinfo = {
	.version = 0x009,
	.enc_xres = 2400,
};

static ssize_t fpga_write(struct ctbl *ctbl, struct command *cmd)
{
	if (ctbl->param0)
		rice_iowrite32(cmd->data[0], ctbl->param0);
	else
		rice_iowrite32(cmd->data[1], cmd->data[0]);

	if (ctbl->cmd == CMD_SET_FIRE_COUNT) {
		int i, cnt;

		if (__prinfo.page_cnt < 10)
			cnt = __prinfo.page_cnt;
		else
			cnt = 10;

		for (i = 0; i < cnt - 1; i++) {
			rice_iowrite32(cmd->data[0], 0x3000);
		}

		if (io_control(IOCMD_SET_FIRENUM, cmd->data[0]) < 0)
			return -1;

		if (io_control(IOCMD_SET_PAGECNT, __prinfo.page_cnt) < 0)
			return -1;
	}

	return 0;
}

static ssize_t fpga_read(struct ctbl *ctbl, struct command *cmd)
{
	if (ctbl->param0)
		cmd->data[1] = rice_ioread32(ctbl->param0);
	else
		cmd->data[1] = rice_ioread32(cmd->data[0]);

	return 4;
}

static ssize_t set_hcnt(struct ctbl *ctbl, struct command *cmd)
{
	size_t tmp;

	/* 0x01e0: bits[0-1]:point type, bits[2-3]:head count */
	tmp = rice_ioread32(0x01e0);
	tmp &= 0x3;
	tmp |= cmd->data[0]<<2;
	rice_iowrite32(tmp, 0x01e0);
	if (io_control(IOCMD_SET_HBCNT, cmd->data[0]))
		return -1;
	return 0;
}

static ssize_t set_PD_period(struct ctbl *ctbl, struct command *cmd)
{
	rice_iowrite32(cmd->data[0], 0x0170);	/* high period of PD */
	rice_iowrite32(cmd->data[0], 0x0180);	/*  low period of PD */
	return 0;
}

static ssize_t prinfo_set(struct ctbl *ctbl, struct command *cmd)
{
	void *p = (void *)&__prinfo + ctbl->param0;
	memcpy(p, &cmd->data[0], cmd->size - sizeof(*cmd));
	return 0;
}

static ssize_t prinfo_get(struct ctbl *ctbl, struct command *cmd)
{
	cmd->data[1] = *(__u32 *)((void *)&__prinfo + ctbl->param0);
	return 4;
}

static ssize_t param_set(struct ctbl *ctbl, struct command *cmd)
{
	size_t size = cmd->size - sizeof(struct command);
	if (cmd->id == 0xc5) {
		struct env_temp *p = (struct env_temp *)&cmd->data[0];
		if (p->temp[0].setting_temp < 100)
			return 0;
	}
	return save_param(ctbl->param0, ctbl->param1, cmd->data, size);
}

static ssize_t param_get(struct ctbl *ctbl, struct command *cmd)
{
	size_t size = cmd->size - sizeof(struct command);
	return read_param(ctbl->param0, &cmd->data[1], size);
}
/* -------------------------------------------------------------------------- */
static ssize_t hboard_write(struct ctbl *ctbl, struct command *cmd)
{
	int hid = cmd->data[0] + 1;

	if (can0_write(hid, cmd, cmd->size) < 0)
		return -1;

	return 0;
}

static ssize_t hboard_writeall(struct ctbl *ctbl, struct command *cmd)
{
	if (can0_writeall(cmd, cmd->size) < 0)
		return -1;

	if (ctbl->cmd == 0x2f)
		to_prinfo()->is_pwroff = 1;

	return 0;
}

static ssize_t hboard_response(struct ctbl *ctbl, struct command *cmd)
{
	int hid = cmd->data[0] + 1;

	return can0_read_response(hid, cmd, cmd->size, &cmd->data[1]);
}

static ssize_t hboard_temp(struct ctbl *ctbl, struct command *cmd)
{
	struct printer_info *pinfo = to_prinfo();
	int num = cmd->data[0];
	int size = sizeof(pinfo->temp[0]);

	memcpy(&cmd->data[1], pinfo->temp[num], size);
	return size;
}

static ssize_t set_vdm_index(struct ctbl *ctbl, struct command *cmd)
{
	struct command tmp;

	format_cmd(&tmp, 0x800, NULL, 0);
	if (can0_writeall(&tmp, tmp.size) < 0)
		return -1;

	/* Genete a _|^|_ for upto_plat */
	rice_iowrite32(1 | 10<<6 | 1<<31, 0x01b0);
	usleep(100 * 1000);
	rice_iowrite32(1 | 10<<6, 0x01b0);
	usleep(100 * 1000);

	/* reset fpga of both cboard and hboard */
	rice_iowrite32(0x1, 0x010);
	format_cmd(&tmp, 0x31, NULL, 0);
	if (can0_writeall(&tmp, tmp.size) < 0)
		return -1;

	if (can0_writeall(cmd, cmd->size) < 0) {
		pr_info("%s: can0_writeall failed\n", __func__);
		return -1;
	}

	usleep(500 * 1000);

	/* up to platform */
	rice_setclrbit(1<<0, 0x200);
	return 0;
}

static ssize_t set_bitmode(struct ctbl *ctbl, struct command *cmd)
{
	size_t tmp;
	int ret;

	if (cmd->data[0] > 0x2)
		return -1;

	ret = can0_writeall(cmd, cmd->size);
	if (ret < 0) {
		pr_info("%s: can0_writeall failed\n", __func__);
		return -1;
	}

	tmp = rice_ioread32(0x01e0);
	tmp &= 0xc;

	/* same as hboard, bits[0-1]: 0: 1bits mode, 1: 2bits mode */
	tmp |= cmd->data[0] - 1;
	rice_iowrite32(tmp, 0x01e0);
	return 0;
}

static ssize_t start_flash(struct ctbl *ctbl, struct command *cmd)
{
	__u32 times =  cmd->data[0];
	__u32 period =  cmd->data[1];

	rice_iowrite32(times | period<<6 | 1<<31, 0x01b0);
	return 0;
}

static ssize_t end_flash(struct ctbl *ctbl, struct command *cmd)
{
	rice_iowrite32(0, 0x01b0);
	return 0;
}

static ssize_t print_control(struct ctbl *ctbl, struct command *cmd)
{
	struct printer_info *prinfo = to_prinfo();

	if (ctbl->param0 == PRCTRL_START) {
		prinfo->printed = 0;
		clear_skb();
		prinfo->status = SR_START;
	}

	if (io_control(IOCMD_PR_CTRL, ctbl->param0) < 0)
		return -1;

	if (ctbl->param0 == PRCTRL_START) {
		if (can0_writeall(cmd, cmd->size) < 0)
			return -1;
	}

	return 0;
}

static ssize_t set_pr_mode(struct ctbl *ctbl, struct command *cmd)
{

	/* |0           |1        |2       |3            |
	 * |venc enable |enc sel  |pd sel  |pnp or npn(1)| */
	if (cmd->data[0]) {
		rice_clrbits(ctbl->param0, 0x150);
	} else {
		rice_setbits(ctbl->param0, 0x150);
	}

	return 0;
}

static ssize_t set_speed(struct ctbl *ctbl, struct command *cmd)
{
	rice_clrbits(1, 0x150);
	rice_setclrbit(1<<0, 0x030);
	rice_iowrite32(cmd->data[0], 0x160);
	rice_setbits(1, 0x150);
	return 0;
}

static ssize_t keepalive(struct ctbl *ctbl, struct command *cmd)
{
	return 0;
}

static ssize_t sec_read(struct ctbl *ctbl, struct command *cmd)
{
	if (read_eeprom(ctbl->param0, &cmd->data[1], ctbl->param1) < 0)
		return -1;
	return ctbl->param1;
}

#define __OFFS(x) offsetof(struct printer_info, x)
static struct ctbl __ctbls[] = {
	/* offset -------------------- cmd ---------------------- parse */
	{ 0x1018, 17, 0x46, sec_read },
	{ 0x1000, 18, 0x45, sec_read }, /* mac */

	{ 0x0140, 0, CMD_SET_XISHU,		fpga_write },
	{ 0x3000, 0, CMD_SET_FIRE_COUNT,	fpga_write },
	{ 0x0190, 0, CMD_SET_PRINT_DIRECTION,	fpga_write },
	{ 0x0040, 0, CMD_SET_YOFFSET,		fpga_write },
	{ 0x0210, 0, 0x35,			fpga_write },	/* job height */
	{ 0x0160, 0, 0x39,			set_speed },	/* virtual speed */

	/* offset=0, the value to be written should be read from cmd->data[0] */
	{ 0x0000, 0, CMD_SET_REGISTER_VALUE,	fpga_write },

	{ __OFFS(enc_xres),	0, CMD_SET_PRINTER_ENCODER_RESOLUTION,	prinfo_set },
	{ __OFFS(page_cnt),	0, CMD_SET_PAGE_COUNT,			prinfo_set },
	{ __OFFS(page_size),	0, 0x2d,				prinfo_set },
	{ __OFFS(enc_speed),	0, CMD_GET_CURRENT_SPEED,		prinfo_get },
	{ __OFFS(version),	0, 0x3f,				prinfo_get },
	{ __OFFS(is_pwroff), 	0, 0x44,				prinfo_get },

	{ param_off(uv_led),	0, 0x3d,		param_set },
	{ param_off(env_temp), 	1, 0xc5,		param_set },
	{ param_off(env_temp), 	0, 0xc6,		param_get },


	{ 0x0000, 0, CMD_GET_REGISTER_VALUE,		fpga_read },
	{ 0x0800, 0, 0x3e,				fpga_read },     /* Get cfpag V */

	{ 0x0000, 0, CMD_SET_VDM_INDEX,			set_vdm_index },
	{ 0x0000, 0, CMD_SET_VOLTAGE,			hboard_write },
	{ 0x0000, 0, CMD_SET_VOLTAGE_CALIBRATION_INFO,	hboard_write },
	{ 0x0000, 0, CMD_SET_WAVEFROM,			hboard_write },
	{ 0x0000, 0, CMD_SET_HEAD_TARGET_TEMPERATURE,	hboard_write },
	{ 0x0000, 0, CMD_SET_SHAKE_MODE,		hboard_write }, 
	{ 0x0000, 0, CMD_CLEAN_VOLTAGE,			hboard_write },
	{ 0x0000, 0, 0xc1,				hboard_write },    /* Set plat_vol */
	{ 0x0000, 0, 0x2e,				hboard_writeall }, /* power on     */
	{ 0x0000, 0, 0x2f,				hboard_writeall }, /* power off    */
	{ 0x0000, 0, 0xc0,				hboard_response }, /* Get plat_vol */
	{ 0x0000, 0, 0xc3,				hboard_response }, /* Get param    */
	{ 0x0000, 0, 0x1f,				hboard_temp     }, /* Get temp     */
	{ 0x0000, 0, 0x40,				hboard_response }, /* Get fpga V   */
	{ 0x0000, 0, 0x41,				hboard_response }, /* Get arm  V   */

	{ 0x0000, 0, CMD_SET_BIT_MODE,			set_bitmode },
	{ 0x0000, 0, 0x2b,				start_flash },
	{ 0x0000, 0, 0x30,				end_flash },

	{ PRCTRL_START,	0, 0x31, print_control },
	{ PRCTRL_PAUSE,	0, 0x32, print_control },
	{ PRCTRL_RESUME,0, 0x33, print_control },
	{ PRCTRL_STOP, 	0, 0x34, print_control },

	{ 0x0000, 0, 0x36, set_PD_period },
	{ 0x0000, 0, 0x37, set_hcnt      },

	{ 1<<0x2, 0, CMD_SET_PRINT_MODE, set_pr_mode },
	{ 1<<1|1, 0, 0x38, set_pr_mode },
	{ 1<<0x3, 0, 0x3a, set_pr_mode },

	{ 0x0000, 0, 0x3c, keepalive },

	{ 0 },
};

#if 0
static __u64 __cmds[] = {
	CMD_S_H_VOL,	CMD_C_H_VCALI,	CMD_S_H_VCALI,
	CMD_S_H_WAVE,	CMD_S_H_TEMP,	CMD_S_H_SHAKE,
	CMD_S_H_BVOL,

	CMD_S_H_PWROFF,

	CMD_G_H_BVOL,	CMD_G_H_WAVE,	CMD_G_H_TEMP,
	CMD_G_H_FVER,	CMD_G_H_AVER,
};
#endif

static int ctbl_parse(void *buf)
{
	ssize_t len;
	struct ctbl *ctbl = __ctbls;
	struct command *cmd = (struct command *)buf;

	while (cmd->id != ctbl->cmd) {
		if (ctbl->cmd == 0) {
			pr_info("%s :no such cmd(%x)\n", __func__, cmd->id);
			len = 0;
			cmd->data[0] = 1;
			goto out;
		}
		ctbl++;
	}

	len = ctbl->pfunc(ctbl, cmd);
	if (len < 0) {
		len = 0;
		cmd->data[0] = 1;
		goto out;
	}

	cmd->data[0] = 0;
out:	cmd->size = 20 + len;
	cmd->type = 2;
	cmd->id |= 0x80000000;

	return inet_write(0, buf, cmd->size);
}


static unsigned char cmd_buffer[4096];

static void *cmd_thread(void *arg)
{
	struct command *p = (struct command *)cmd_buffer;
	const int cmd_len = sizeof(struct command);

	pthread_detach(pthread_self());

	while (1) {
		int rc;
		size_t size;

		rc = inet_read(0, p, cmd_len);
		if (rc <= 0) {
			usleep(100);
			continue;
		}

		size = min(p->size, sizeof(cmd_buffer));
		size -= cmd_len;
		if (size > 0) {
			rc = inet_read(0, cmd_buffer + cmd_len, size);
			if (rc <= 0) {
				pr_info("%s :wrong data length\n", __func__);
				continue;
			}
		}

		pr_debug("%s: size(%x), id(%x)\n", __func__, p->size, p->id);
		ctbl_parse(cmd_buffer);
	}

	pthread_exit(NULL);
}


int command_init(void)
{
	pthread_t pid;
	return pthread_create(&pid, NULL, cmd_thread, NULL);
}

void format_cmd(struct command *cmd, int id, void *buf, size_t size)
{
	cmd->size = sizeof(*cmd) + size;
	cmd->id = id;
	cmd->type = 1;
	cmd->seqnum = 0;

	if (buf && size)
		memcpy(cmd->data, buf, size);
}

