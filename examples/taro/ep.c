/* ~.~ *-c-*
 *
 * Copyright (c) 2013, John Lee <furious_tauren@163.com>
 * Mon May 18 10:47:01 CST 2015
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <malloc.h>
#include <sys/epoll.h>
#include <pthread.h>

#include <ep.h>
#include <debug.h>

struct eplist *epl_alloc(size_t cnt)
{
	struct eplist *list;
	size_t size = sizeof(struct ep) * (cnt - 1);

	list = calloc(1, sizeof(struct eplist) + size);
	if (!list)
		return NULL;

	list->epoll_fd = epoll_create1(0);
	if (list->epoll_fd < 0) {
		free(list);
		return NULL;
	}

	list->num = cnt;
	while (cnt-- > 0)
		list->eps[cnt].fd = -1;

	return list;
}

void epl_free(struct eplist *list)
{
	while (list->num-- > 0)
		if (list->eps[list->num].fd != -1)
			epl_del_one(&list->eps[list->num]);

	close(list->epoll_fd);
	free(list);
}

static struct ep *filedes_to_ep(int fd, struct eplist *list)
{
	int i;

	for (i = 0; i < list->num; i++) {
		if (list->eps[i].fd == fd)
			return &list->eps[i];
	}

	pr_info("%s :no ep(fd=%d) found\n", __func__, fd);
	return NULL;
}


int epl_add_one(struct eplist *list, int fd, on_recv recv, void *arg)
{
	struct epoll_event ev;

	struct ep *ep = filedes_to_ep(-1, list);
	if (!ep)
		return -1;

	ev.data.fd = fd;
	ev.events = EPOLLIN;
	if (epoll_ctl(list->epoll_fd, EPOLL_CTL_ADD, fd, &ev))
		return -1;

	ep->arg = arg;
	ep->on_recv = recv;
	ep->fd = fd;
	return 0;
}

static void *epthread(void *arg)
{
	struct epoll_event events[BACKLOG];
	struct eplist *list = (struct eplist *)arg;

	pthread_detach(pthread_self());
	while (1) {
		int i;

		/* timeout == -1 causes epoll_wait() to block indefinitely */
		int n = epoll_wait(list->epoll_fd, events, list->num, -1);
		for (i = 0; i < n; i++)  {
			int rc = 0;
			struct ep *ep = filedes_to_ep(events[i].data.fd, list);

			if (ep == NULL) {
				pr_info("%s :null ep(%d)\n",
						__func__, events[i].data.fd);
				close(events[i].data.fd);
				continue;
			}

			/*
			 * receiving a RST segment. This means that the
			 * client has closed the socket and the server
			 * has performed one write on the closed socket. 
			 */
			if (events[i].events & (EPOLLERR | EPOLLHUP)) {
				pr_info("%s :errors(%d)\n", __func__, ep->fd);
				epl_del_one(ep);
				continue;
			}

			if (events[i].events & EPOLLIN) {
				rc = ep->on_recv(ep);
				if (rc < 0) {
					pr_info("%s :recv failed(%d)\n",
							__func__, ep->fd);
					epl_del_one(ep);
				}
			}
		}
	}

	pthread_exit(NULL);
}

int epl_start(struct eplist *list)
{
	pthread_t pid;
	return pthread_create(&pid, NULL, epthread, list);
}

