/* ~.~ *-h-*
 *
 * Copyright (c) 2013, John Lee <furious_tauren@163.com>
 * Wed May 27 07:29:05 CST 2015
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdio.h>
#include <string.h>
#include <errno.h>

/* print level */
#ifndef __PR_LEVEL__
#  define __PR_LEVEL__	__PR_INFO__
#endif

#define __PR_ERR__      3
#define __PR_WARN__     2
#define __PR_INFO__     1
#define __PR_DEBUG__    0

#define halt() do {	\
} while (1)

#define panic(fmt, args...) do {	\
	fprintf(stderr, fmt,  ##args);	\
	halt();	\
} while (0)

#define __pr_err(fmt, args...) do { \
	fprintf(stderr, fmt " :%s\n",  ##args, strerror(errno)); \
} while (0)

#define __pr_debug(fmt, args...) do { \
	fprintf(stderr, fmt,  ##args); \
} while (0)

#if __PR_LEVEL__ <= __PR_ERR__
#  define pr_err __pr_err
#else
#  define pr_err(fmt, args...) do {} while (0)
#endif

#if __PR_LEVEL__ <= __PR_DEBUG__
#  define pr_debug	__pr_debug
#else
#  define pr_debug(fmt, args...) do {} while (0)
#endif

#if __PR_LEVEL__ <= __PR_INFO__
#  define pr_info	__pr_debug
#else
#  define pr_info(fmt, args...) do {} while (0)
#endif

#if __PR_LEVEL__ <= __PR_WARN__
#  define pr_warn	__pr_debug
#else
#  define pr_warn(fmt, args...) do {} while (0)
#endif

int status(unsigned level, int hid, unsigned eid, char *desc);

#endif

