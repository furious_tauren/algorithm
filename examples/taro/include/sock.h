/* ~.~ *-h-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Tue Jul  5 16:37:21 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __SOCK_H__
#define __SOCK_H__

int setnonblocking(int fd);
int get_hostname(char *host, const char *name, int family);
int nbtcp_open(unsigned short port);
int nbtcp_accept(int sfd);
int nbtcp_read(int sfd, void *buf, size_t size);
int nbtcp_write(int sfd, const void *buf, size_t size);
int canbus_open(int num);

#endif

