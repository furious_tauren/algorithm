/* ~.~ *-h-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Sat Apr 23 19:08:52 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CAN_H_
#define __CAN_H_

#include <unistd.h>

int can0_init(void);

int can0_write(int num, const void *buf, size_t size);
int can0_writeall(const void *buf, size_t size);
int can0_read_response(int id, const void *wrbuf, size_t size, void *resp);

#endif
