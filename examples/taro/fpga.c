/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * 2016年 03月 23日 星期三 10:51:20 CST
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/types.h>

#include <signal.h>
#include <sys/time.h>

#include <printer.h>
#include <can.h>
#include <command.h>
#include <pthread.h>
#include <ads1015.h>

static void *__mapbase;

inline int rice_ioread32(__u32 addr)
{
	return *(volatile int *)(__mapbase + addr);
}
inline void rice_iowrite32(__u32 data, __u32 addr)
{
	*(volatile __u32 *)(__mapbase + addr) = data;
}

void rice_setbits(__u32 bits, __u32 addr)
{
	__u32 value = rice_ioread32(addr);
	value |= bits;
	rice_iowrite32(value, addr);
}
void rice_clrbits(__u32 bits, __u32 addr)
{
	__u32 value = rice_ioread32(addr);
	value &= ~bits;
	rice_iowrite32(value, addr);
}

inline void rice_setclrbit(__u32 bit, __u32 addr)
{
	rice_iowrite32(bit, addr);
	rice_iowrite32(0, addr);
}

void rice_reset(void)
{
	struct printer_info *pinfo = to_prinfo();

	pr_info("%s\n", __func__);

	rice_iowrite32(0x1, 0x010);	/* Reset */
	usleep(500 * 1000);

	/* perform the "down to zero" action */
	rice_setclrbit(1<<1, 0x200);

	/* record the coord */
	pinfo->idle_coord = (float)rice_ioread32(0x200);
	pinfo->status = SR_STOPPING;
}

static inline float coord_to_cm(__u32 res, __u32 coord)
{
	return coord * 2.54 / res;
}

static void uv_ctrl(struct uv_led *uv_led)
{
	__u32 val;
	int i;
	struct printer_info *pinfo = to_prinfo();

	if (pinfo->status == SR_IDLE)
		return;

	if (pinfo->status == SR_STOPPING) {

		__u32 coord = pinfo->cur_coord - pinfo->idle_coord;
		float dist = coord_to_cm(pinfo->enc_xres, coord);
		if (dist >= uv_led->distance / 100.0) {
			rice_iowrite32(0, 0x0240);
			pinfo->status = SR_IDLE;
			return;
		}
	}

	for (i = 0, val = 0; i < uv_led->num; i++) {
		int map[] = { 1, 0, 2, 3 };

		if (uv_led->speed[i] < pinfo->enc_speed)
			val |= 1<<map[i];
	}

	rice_iowrite32(val, 0x0240);
}

static void sec_tick(int signo)
{
	float tmp, coord;
	struct printer_info *pinfo = to_prinfo();
	struct printer_param *param = to_param();

	/* read current coord */
	coord = (float)rice_ioread32(0x200);
	tmp = coord_to_cm(pinfo->enc_xres, coord - pinfo->cur_coord);
	tmp /= 100;	/* cm to m */
	tmp *= 60;	/* s to minute */
	pinfo->cur_coord = coord;

	/* average speed in 1 second */
	pinfo->enc_speed = tmp;

	uv_ctrl(&param->uv_led);
	heater_ctrl(&param->env_temp);

	pr_debug("belt move at %f m/min\n", tmp);
}

static int start_tick(void)
{
	struct itimerval tick;

	memset(&tick, 0, sizeof(tick));
	tick.it_value.tv_sec = 1;
	tick.it_value.tv_usec = 0;

	tick.it_interval.tv_sec = 1;
	tick.it_interval.tv_usec = 0;

	signal(SIGALRM, sec_tick);

	if (setitimer(ITIMER_REAL, &tick, NULL) < 0) {
		pr_err("setitimer failed\n");
		return -1;
	}

	return 0;
}

static int is_rice_ready(void)
{
	int retries = 100;

	/* set lvds clock divider */
	rice_iowrite32(8, 0x0020);

	while (retries--) {
		if (rice_ioread32(0x0020) & (1<<14))
			return 1;
		usleep(1000);
	}

	return 0;
}

static void *temp_thread(void *arg)
{
	static struct command cmd;
	static char buf[0x68];
	struct printer_info *pinfo = to_prinfo();

	format_cmd(&cmd, 0x1f, NULL, 0);

	pthread_detach(pthread_self());

	while (1) {
		if (can0_read_response(1, &cmd, cmd.size, buf) > 0)
			memcpy(pinfo->temp[0], buf, sizeof(buf));
		if (can0_read_response(2, &cmd, cmd.size, buf) > 0)
			memcpy(pinfo->temp[1], buf, sizeof(buf));
		sleep(10);
	}
	
	pthread_exit(NULL);
}

#define MAPSZ 0x10000
int rice_init(void)
{
	pthread_t pid;
	int fd;
	void *mapped;


	fd = open("/dev/mem", O_RDWR);
	if (fd < 0) {
		perror("Unable to open /dev/mem");
		return -1;
	}

	mapped = mmap(0, MAPSZ, PROT_READ|PROT_WRITE,
				MAP_SHARED, fd, 0x8000000);
	if (mapped == (void *)-1) {
		perror("mmap");
		close(fd);
		return -1;
	}

	__mapbase = mapped;
	close(fd);

	if (!is_rice_ready()) {
		munmap(__mapbase, MAPSZ);
		return -1;
	}

	/* set pd to npn */
	rice_setbits(1<<3, 0x150);
	start_tick();
	pthread_create(&pid, NULL, temp_thread, NULL);
	return 0;
}

