/* ~.~ *-c-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Thu Jun 30 16:27:15 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <stdio.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <poll.h>

#include <io.h>
#include <inet.h>
#include <fpga.h>
#include <fifo.h>
#include <printer.h>
#include <debug.h>
#include <io_cmd.h>
#include <macro.h>

#define EVENT_PAGE_START	0x40000002
#define EVENT_PAGE_PRINTED	0x40000003
#define EVENT_JOB_FINISH	0x40000004
#define EVENT_CACEL_DONE	0x40000005
#define EVENT_JOB_START		0x40000006
#define EVENT_DATA_LACK		0x40000007

struct status_info {
	int		bit;
	unsigned	event;
};

static struct status_info status_info[] = {
	{ 0, EVENT_PAGE_START },
	{ 1, EVENT_PAGE_PRINTED },
	{ 3, EVENT_DATA_LACK },
	{ 5, EVENT_CACEL_DONE },
	{ 6, EVENT_JOB_START },
	{ 7, EVENT_JOB_FINISH },
};

static struct io_data {
	int		devfd;
	struct fifo	*fifo;
} __io;

inline struct fifo *io_fifo(void)
{
	return __io.fifo;
}

int io_poll(void)
{
	struct pollfd fds;

	fds.fd = __io.devfd;
	fds.events = POLLIN;

	return poll(&fds, 1, -1);
}

int io_control(unsigned long request, long arg)
{
	return ioctl(__io.devfd, request, arg);
}

int io_read(void *buf, size_t nbyte)
{
	return read(__io.devfd, buf, nbyte);
}

int io_write(const void *buf, size_t nbyte)
{
	return write(__io.devfd, buf, nbyte);
}

static void status_issue(int *status)
{
	int i;
	__u32 buffer[5] = {
		sizeof(buffer),
	};

	pr_debug("status: %d, %d\n", status[0], status[1]);

	/* page had been printed */
	for (i = 0; i < ARRAY_SIZE(status_info); i++) {
		if ((1<<status_info[i].bit) & status[0]) {
			buffer[2] = status_info[i].event;
			buffer[4] = status[1];
			inet_write(0, buffer, sizeof(buffer));
		}
	}

	/* for the following 2 status, PD has been disabled
	 * in kernel. just reset fpga and perform a "down to zero"
	 */
	if ((SL_JOB_END | SL_JOB_CANCELED) & status[0])
		rice_reset();
}

static void *io_thread(void *arg)
{
	int status[2];
	struct pollfd fds;

	fds.fd = __io.devfd;
	fds.events = POLLIN;

	pthread_detach(pthread_self());

	while (1) {
		if (poll(&fds, 1, -1) < 0)
			continue;

		if (fds.revents & POLLERR) {
			pr_err("%s", __func__);  
			continue;
		} 
		if (io_read(status, sizeof(status)) < 0)
			continue;

		status_issue(status);
	}

	pthread_exit(NULL);
}

int io_init(void)
{
	void *mapped;
	pthread_t pid;
	struct fifo *fifo;

	int fd = open("/dev/taro", O_RDWR);
	if (fd < 0) {
		perror("Unable to open /dev/taro");
		return -1;
	}

	mapped = mmap(0, KFIFO_SIZE + sizeof(struct fifo),
			PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (mapped == (void *)-1) {
		perror("mmap");
		goto err_out;
	}


	fifo = (struct fifo *)mapped;
	fifo_init(fifo, mapped + sizeof(struct fifo), KFIFO_SIZE);
	__io.devfd = fd;
	__io.fifo  = fifo;

	if (pthread_create(&pid, NULL, io_thread, NULL)) {
		perror("pthread_create");
		munmap(__io.fifo, KFIFO_SIZE + sizeof(struct fifo));
		goto err_out;
	}

	return 0;

err_out:
	close(fd);
	return -1;
}

void io_exit(void)
{
	close(__io.devfd);
	munmap(__io.fifo, KFIFO_SIZE + sizeof(struct fifo));
}

