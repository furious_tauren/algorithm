/* ~.~ *-c-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Sat Apr 23 18:56:55 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <sys/fcntl.h>
#include <pthread.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>

#include <sock.h>
#include <debug.h>
#include <macro.h>
#include <can.h>
#include <inet.h>

#include <printer.h>
#include <fifo.h>
#include <debug.h>
#include <command.h>

#define CANID_ALL	0x100

#define CAN0_NUM	4
#define FIFO_SIZE	0x1000
#define RETRIES		10


#define PACK_MAGIC	0x5AA555AA

#define PACK_ACK	0xA5
#define PACK_ERR	0x45
#define PACK_INF	0x49

struct can_pack {
	unsigned 	magic;
	unsigned short	info;
	unsigned char	type;
	unsigned char	id;
};

struct can_info {
	size_t		size;
	int		acked;
	struct fifo	fifo;
	char		buffer[FIFO_SIZE];
};

static struct canbus {
	int		can_fd;
	__u32		mask;
	pthread_mutex_t lock;
	struct can_info	can[CAN0_NUM];
} __cbus0;

static inline
struct can_info *to_caninfo(struct canbus *bus, int id)
{
	return &bus->can[id - 1];
}

/* -------------------------------------------------------------- */
/* things related to can0 thread  */
static inline struct canbus *to_cbus0(void)
{
	return &__cbus0;
}

static int is_cpack(struct can_frame *frame)
{
	struct can_pack *pack;
	__u8 type;

	if (frame->can_dlc != 8)
		return 0;

	pack = (struct can_pack *)frame->data;
	if (pack->magic != PACK_MAGIC)
		return 0;

	type = pack->type;
	if (type != PACK_ACK
			&& type != PACK_ERR && type != PACK_INF)
		return 0;

	return type;
}

static int proc_cpack(struct can_info *can, struct can_frame *frame)
{
	struct can_pack *pack = (struct can_pack *)frame->data;

	switch (pack->type) {

	case PACK_ACK:
		can->acked = 1;
		break;

	case PACK_ERR:
		return status(0, frame->can_id, pack->info, NULL);

	case PACK_INF:
		can->size = pack->info;
		break;

	default:
		return -1;
	}

	return 0;
}

static void can_err_handle(struct can_frame *frame)
{
	if (frame->can_id & CAN_ERR_BUSOFF)
		pr_info("(bus off) ");

	if (frame->can_id & CAN_ERR_TX_TIMEOUT)
		pr_info("(tx timeout) ");

	if (frame->can_id & CAN_ERR_ACK)
		pr_info("(ack) ");

	if (frame->can_id & CAN_ERR_LOSTARB) {
		pr_info("(lost arb)");
		if (frame->data[0])
			pr_info("[%d]", frame->data[0]);
		pr_info(" ");
	}

	if (frame->can_id & CAN_ERR_CRTL) {
		pr_info("(crtl)");
		if (frame->data[1] & CAN_ERR_CRTL_RX_OVERFLOW)
			pr_info("[RX buffer overflow]");
		if (frame->data[1] & CAN_ERR_CRTL_TX_OVERFLOW)
			pr_info("[TX buffer overflow]");
		if (frame->data[1] & CAN_ERR_CRTL_RX_WARNING)
			pr_info("[RX warning]");
		if (frame->data[1] & CAN_ERR_CRTL_TX_WARNING)
			pr_info("[TX warning]");
		pr_info(" ");
	}

	pr_info("\n");
}

static void *can0_thread(void *arg)
{
	struct can_frame frame;
	struct canbus *bus = (struct canbus *)arg;

	pthread_detach(pthread_self());

	while (1) {
		struct fifo *fifo;
		struct can_info *can;

		if (read(bus->can_fd, &frame, sizeof(frame)) < 0) {
			pr_err("%s", __func__);
			continue;
		} else if (frame.can_id & CAN_ERR_FLAG) {
			can_err_handle(&frame);
			continue;
		}

		if (frame.can_id > ARRAY_SIZE(bus->can)) {
			pr_info("%s :invalid can_id\n", __func__);
			continue;
		}

		can = to_caninfo(bus, frame.can_id);

		if (is_cpack(&frame)) {
			proc_cpack(can, &frame);
		} else {
			/* this comes after a write */
			fifo = &can->fifo;
			if (fifo_space(fifo) < frame.can_dlc) {
				pr_info("%s: fifo full\n", __func__);
				continue;
			}

			fifo_in(fifo, frame.data, frame.can_dlc);
		}
	}

	pthread_exit(NULL);
}

/* -------------------------------------------------------------- */
static int __write(struct canbus *bus, int id, const void *buf, size_t size)
{
	struct can_frame frame;
	ssize_t ret = 0;

	frame.can_id = id;

	while (size > 0) {
		ssize_t len = min(size, 8);
		ssize_t tmp = len;

		frame.can_dlc = len;
		memcpy(frame.data, buf, len);

		len = write(bus->can_fd, &frame, sizeof(frame));
		if (len < 0) {
			pr_err("%s", __func__);
			return -1;
		}

		buf += tmp;
		ret += tmp;
		size -= tmp;
	}

	return ret;
}


static void can_reset(struct canbus *bus, __u32 mask)
{
	int id;

	for (id = 0; mask; id++, mask >>= 1) {
		struct can_info *can;

		if ((mask & 0x1) == 0)
			continue;

		can = to_caninfo(bus, id);

		can->acked = 0;
		can->size  = 0;
		fifo_reset(&can->fifo);
	}

	return;
}

static int can_is_acked(struct canbus *bus, __u32 mask, int ms)
{
	struct can_info *can;
	int id;

	for (id = 0; mask; id++, mask >>= 1) {
		int retries;

		if ((mask & 0x1) == 0)
			continue;

		can = to_caninfo(bus, id);
		retries = ms;
		while (can->acked == 0 && retries--)
			usleep(1000);

		if (can->acked == 0) {
			pr_info("%s :can(%d) no ack\n", __func__, id);
			return 0;
		}
	}

	return 1;
}

static int
can_write(struct canbus *bus, int id, __u32 mask, const void *buf, size_t size)
{
	int ret;

	pthread_mutex_lock(&bus->lock);
	can_reset(bus, mask);
	ret = __write(bus, id, buf, size);
	if (ret < 0) {
		pr_err("%s", __func__);
		goto out;
	}

	if (!can_is_acked(bus, mask, 2000))
		ret = -1;

out:
	pthread_mutex_unlock(&bus->lock);
	return ret;
}

static void can_broadcast(struct canbus *bus)
{
	struct command cmd;
	int i;
	int retries;

	format_cmd(&cmd, 0x110, NULL, 0);

	__write(bus, CANID_ALL, &cmd, sizeof(cmd));
	for (i = 0; i < ARRAY_SIZE(bus->can); i++) {
		struct can_info *can = &bus->can[i];
		retries = 100;

		while (can->acked == 0 && retries--)
			usleep(1000);
		if (can->acked)
			bus->mask |= 1<<(i + 1);
	}
	pr_info("%s: Hid mask(0x%0x)\n", __func__, bus->mask);
}

typedef void *(*rthread_t)(void *);
static int __can_init(struct canbus *bus, int num, rthread_t thread)
{
	int i;
	pthread_t pid;

	for (i = 0; i < ARRAY_SIZE(bus->can); i++) {
		struct can_info *can = &bus->can[i];
		fifo_init(&can->fifo, can->buffer, sizeof(can->buffer));
	}

	bus->can_fd = canbus_open(num);
	if (bus->can_fd < 0)
		return -1;

	if (pthread_create(&pid, NULL, thread, bus)) {
		close(bus->can_fd);
		return -1;
	}

	pthread_mutex_init(&bus->lock, NULL);
	can_broadcast(bus);
	return 0;
}

/* -------------------------------------------------------------- */
int can0_write(int id, const void *buf, size_t size)
{
	struct canbus *bus = to_cbus0();
	return can_write(bus, id, 1<<id, buf, size);
}

inline int can0_writeall(const void *buf, size_t size)
{
	struct canbus *bus = to_cbus0();
	return can_write(bus, CANID_ALL, bus->mask, buf, size);
}

int can0_read_response(int id, const void *wrbuf, size_t size, void *resp)
{
	int ret;
	int retries = RETRIES;
	struct canbus *bus = to_cbus0();
	struct can_info *can = to_caninfo(bus, id);
	struct fifo *fifo = &can->fifo;
	__u32 mask = 1<<id;

	pthread_mutex_lock(&bus->lock);
	can_reset(bus, mask);
	ret = __write(bus, id, wrbuf, size);
	if (ret < 0) {
		pr_err("%s", __func__);
		goto err_out;
	}

	while (can->size == 0 && retries--)
		usleep(1000);
	if (can->size == 0) {
		pr_info("%s :timeout\n", __func__);
		goto err_out;
	}

	/* retries should be set more than 500 to wait for data */
	retries = 1000;
	while (fifo_cnt(fifo) < can->size && retries--) 
		usleep(1000);
	if (fifo_cnt(fifo) < can->size) {
		pr_info("%s :data lack(%x: %x,%x)\n",
				__func__, id, fifo_cnt(fifo), can->size);
		goto err_out;
	}

	ret = fifo_out(fifo, resp, can->size);
	if (can_is_acked(bus, mask, RETRIES)) {
		pthread_mutex_unlock(&bus->lock);
		return ret;
	}

err_out:
	pthread_mutex_unlock(&bus->lock);
	return -1;
}

int can0_init(void)
{
	struct canbus *bus = to_cbus0();

	if (__can_init(bus, 0, can0_thread) < 0) {
		pr_info("__can_init failed\n");
		return -1;
	}

	return 0;
}


