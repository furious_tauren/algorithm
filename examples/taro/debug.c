/* ~.~ *-c-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Sat Jul  2 11:46:52 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <string.h>
#include <debug.h>
#include <inet.h>

#define EVENT_INFO_REPORT	0x40000010
#define HBOARD_ERR_MASK		0x40000000
#define HFPGA_ERR_BASE		0x00000800
#define HARM_ERR_BASE		0x00000c00

struct event {
	unsigned size;
	unsigned type;
	unsigned id;
	unsigned seqnum;
	unsigned elevel;	/* 0:err, 1,info */
	unsigned eid;
	char	 desc[256];
};

int status(unsigned level, int hid, unsigned eid, char *desc)
{
	unsigned size;
	struct event event = {
		.id = EVENT_INFO_REPORT,
		.elevel = level,
	};


	/* if bits[24:31] equals to 0x40, the error comes from hboard,
	 * then bits[16:23] is the hid.
	 * bits[ 0:15]: errno
	 */
	event.eid = eid&0xFFFF;
	if (eid >= HFPGA_ERR_BASE) {
		event.eid |= hid<<16;
		event.eid |= HBOARD_ERR_MASK;
	}

	size = sizeof(event) - sizeof(event.desc);
	if (desc) {
		strcpy(event.desc, desc);
		size += strlen(desc);
	} else {
		event.desc[0] = 0;
	}
	size += 1;	/* the '\0' */
	event.size = size;

	pr_info("fatal error :errno 0x%08x\n", event.eid);
	return inet_write(0, &event, size);
}

