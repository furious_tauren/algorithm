/* ~.~ *-h-*
 *
 * Copyright (c) 2013, John Lee <furious_tauren@163.com>
 * Tue May 26 11:29:24 CST 2015
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __EP_H__
#define __EP_H__

#define BACKLOG 20

struct ep {
	int	fd;
	int	(*on_recv)(struct ep *ep);
	void	*arg;
};

typedef int (*on_recv)(struct ep *ep);

struct eplist {
	int		epoll_fd;
	int		num;
	struct ep	eps[1];
};

struct eplist *epl_alloc(size_t cnt);
void epl_free(struct eplist *list);
int epl_add_one(struct eplist *list, int fd, on_recv recv, void *arg);

static inline void epl_del_one(struct ep *ep)
{
	close(ep->fd);
	ep->fd = -1;
}

int epl_start(struct eplist *list);
#endif

