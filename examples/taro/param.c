/* ~.~ *-c-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Mon Nov 21 06:42:02 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/types.h>
#include <string.h>

#include <printer.h>
#include <debug.h>

#define E2PATH "/sys/bus/i2c/devices/3-0050/eeprom"

static struct printer_param __pr_param;

inline struct printer_param *to_param(void)
{
	return &__pr_param;
}

int read_param(off_t offset, void *buf, size_t size)
{
	struct printer_param *p = to_param();

	memcpy(buf, (void *)p + offset, size);
	return size;
}

int save_param(off_t offset, int bit, void *buf, size_t size)
{
	int fd;
	struct printer_param *p = to_param();

	fd = open(E2PATH, O_RDWR);
	if (fd < 0) {
		pr_info("%s :open eeprom failed\n", __func__);
		return -1;
	}

	if ((p->magic & PARAM_MASK) != PARAM_MAGIC)
		p->magic = PARAM_MAGIC;

	p->magic |= 1<<bit;
	memcpy((void *)p + offset, buf, size);

	if (write(fd, p, sizeof(*p)) < 0) {
		pr_info("%s :write failed\n", __func__);
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
}

int load_param(void)
{
	int fd;
	struct printer_param param;
	struct printer_param *p = to_param();

	fd = open(E2PATH, O_RDONLY);
	if (fd < 0) {
		pr_info("%s :open eeprom failed\n", __func__);
		return -1;
	}

	if (read(fd, &param, sizeof(param)) < 0) {
		pr_info("%s :failed to read eeprom\n", __func__);
		close(fd);
		return -1;
	}
	close(fd);

	if ((param.magic & PARAM_MASK) != PARAM_MAGIC)
		return 0;
	else
		p->magic = param.magic;

	if (param.magic & (1<<0)) {
		memcpy(&p->uv_led, &param.uv_led, sizeof(p->uv_led));
	}

	if (param.magic & (1<<1)) {
		memcpy(&p->env_temp, &param.env_temp, sizeof(p->env_temp));
	}

	return 0;
}

int write_eeprom(off_t offset, void *buf, size_t size)
{
	int fd = open(E2PATH, O_RDWR);
	if (fd < 0) {
		pr_info("%s :open eeprom failed\n", __func__);
		return -1;
	}

	lseek(fd, offset, SEEK_SET);
	if (write(fd, buf, size) < 0) {
		pr_info("%s :write failed\n", __func__);
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
}

int read_eeprom(off_t offset, void *buf, size_t size)
{
	int fd = open(E2PATH, O_RDWR);
	if (fd < 0) {
		pr_info("%s :open eeprom failed\n", __func__);
		return -1;
	}

	lseek(fd, offset, SEEK_SET);
	if (read(fd, buf, size) < 0) {
		pr_info("%s :write failed\n", __func__);
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
}
