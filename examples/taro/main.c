/* ~.~ *-c-*
 *
 * Copyright (c) 2015, John Lee <furious_tauren@163.com>
 * Sat Mar 26 11:35:38 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>

#include <debug.h>
#include <fpga.h>
#include <inet.h>
#include <can.h>
#include <command.h>
#include <io.h>
#include <printer.h>

typedef int (*init_func_t)(void);
static init_func_t __init_funcs[] = {
	load_param,
	io_init,
	can0_init,
	rice_init,
	inet_init,
	command_init,

	NULL
};

int main(int argc, char *argv[])
{

	struct command tmp;

	init_func_t *pfunc = __init_funcs;
	while (*pfunc) {
		if ((*pfunc)() < 0)
			panic("init failed\n");

		pfunc++;
	}

	format_cmd(&tmp, 0x110, NULL, 0);
	while (1) {
		if (can0_writeall(&tmp, tmp.size) < 0) {
			printf("wokao\n");
		}
		usleep(100 * 1000);
	}

	return 0;
}

