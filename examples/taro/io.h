/* ~.~ *-h-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Thu Jun 30 16:29:35 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __IO_H__
#define __IO_H__

#include <io_cmd.h>

int io_init(void);
void io_exit(void);

struct fifo *io_fifo(void);

int io_read(void *buf, size_t nbyte);
int io_write(const void *buf, size_t nbyte);

int io_poll(void);
int io_control(unsigned long request, long arg);

#endif

