/* ~.~ *-c-*
 *
 * Copyright (c) 2016, John Lee <furious_tauren@163.com>
 * Tue May 10 19:18:02 CST 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <linux/types.h>
#include <errno.h>

#include <fpga.h>
#include <ads1015.h>

/* -50 oC --- 200 oC */
static float v_table[] = {
	1.320, 1.360, 1.398, 1.430, 1.467, 
	1.500, 1.530, 1.560, 1.590, 1.620, 
	1.645, 1.670, 1.697, 1.720, 1.745, 
	1.770, 1.790, 1.810, 1.830, 1.850, 
	1.870, 1.890, 1.910, 1.930, 1.944, 
	1.960, 
};

static float read_temp(int chan)
{
	FILE *fp;
	char cmd[256];
	char str[32];
	float v, t = -50.0;
	int i;

	sprintf(cmd, "cat /sys/class/hwmon/hwmon0/device/in%d_input", chan);

	/* open the command for reading. */
	fp = popen(cmd, "r");
	if (fp == NULL) {
		perror("");
		return 0.0;
	}

	if (fgets(str, sizeof(str) - 1, fp) == NULL) {
		perror("");
		return 0.0;
	}
	pclose(fp);

	v = atof(str);
	v /= 1000.0;

	for (i = 0; i < sizeof(v_table) / sizeof(v_table[0]); i++) {
		if (v_table[i] > v) {
			i--;
			break;
		}
	}

	/* outrange */
	if (i == sizeof(v_table) / sizeof(v_table[0]))
		return 200.0;

	t += (i + (v - v_table[i]) / (v_table[i + 1] - v_table[i])) * 10.0;
	return t;
}


void heater_ctrl(struct env_temp *env_temp)
{
	int i;
	unsigned val = 0;

	if (env_temp->num > 4)
		env_temp->num = 4;

	for (i = 0; i < env_temp->num; i++) {
		float temp = read_temp(i + 4);

		temp *= 100.0;
		env_temp->temp[i].real_temp = temp;

		if (env_temp->temp[i].is_open == 0)
			continue;

		if (temp < env_temp->temp[i].setting_temp - 200)
			val |= 1 << i;
	}

	rice_iowrite32(val, 0x0250);
}

