#if 0
start: scl high, sda falling
stop: scl high, sda rising
ack: in the 9th clk, master release bus(sda high), client pull down sda.
data: level sampling(when scl is high), twice in a clock.
      __    __    __          __    __    __
SCL     |__| 1|__| 2|__ .... | 8|__| 9|__|
     _    _____       __..__      _____    __
SDA  S|__|  1b |__0b_|11111b|_0b_| NACK|__| P

#endif

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/types.h>

int i2c_init(const char *path, __u16 addr)
{
	int fd;

	fd = open(path, O_RDWR);
	if (fd < 0) {
		printf("failed to open %s\n", path);
		return fd;
	}

#ifdef I2C_MOD_1
    	return ioctl(fd, I2C_SLAVE, addr);
#else
	ioctl(fd, I2C_TIMEOUT, 2);
	ioctl(fd, I2C_RETRIES, 1);

	addr = addr;
	return 0;
#endif
}

int i2c_read(int fd, __u16 addr, __u16 reg, __u8 *buf, __u16 len)
{
#ifdef I2C_MOD_1
	addr = addr;
	write(fd, &reg ,1);
	return read(fd, buf, len);
#else
	struct i2c_rdwr_ioctl_data data;
	struct i2c_msg msgs[2];

	msgs[0].addr = addr;
	msgs[0].flags = 0;
	msgs[0].len = 1;
	msgs[0].buf = (__u8 *)&reg;

	msgs[1].addr = addr;
	msgs[1].flags = I2C_M_RD;
	msgs[1].len = len;
	msgs[1].buf = buf;

	data.nmsgs = 2;
	data.msgs = msgs;

	return ioctl(fd, I2C_RDWR, (unsigned long)&data);
#endif
}


int i2c_write(int fd, __u16 addr, __u16 offs, const __u8 *buf, __u16 len)
{
#ifdef I2C_MOD_1
	__u8 tmp[len + 1];
	tmp[0] = offs;
	memcpy(&tmp[1], buf, len);
	return write(fd, buf, len + 1);
#else
	struct i2c_rdwr_ioctl_data data;
	struct i2c_msg msg;
	__u8 tmp[len + 1];

	tmp[0] = offs;
	memcpy(&tmp[1], buf, len);

	msg.addr = addr;
	msg.flags = 0;
	msg.len = len;
	msg.buf = tmp;

	data.nmsgs = 1;
	data.msgs = &msg;

	return ioctl(fd, I2C_RDWR, (unsigned long)&data);
#endif
}


int main(void)
{
	return 0;
}
