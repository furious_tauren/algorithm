#! /bin/bash


export ARCH=aarch64
export CROSS_COMPILE=aarch64-linux-gnu-
topdir=$(dirname $(realpath $0))

apt list --installed dialog 2>&1 > /dev/null
if [ $? -ne 0 ]; then
	sudo apt update -y
	sudo apt install -y dialog
fi


setup_env()
{
	local str=$(dialog --stdout \
		--clear --backtitle "Vitis Enviroment Setup" \
		--title "Select \`setting64.sh\`" --fselect $topdir 20 60 \
		--and-widget --backtitle "Xilinx System Architecture" \
		--clear --title "Select \`XSA\`" --fselect $HOME 20 60 \
		--and-widget --backtitle "Platform information" \
		--clear --radiolist "Select platform:" 0 0 1\
		1 "mercury_xu6" on)

	local platforms_list=(mercury_xu6)
	local f0=$(echo $str | cut -d ' ' -f 1)
	local f1=$(echo $str | cut -d ' ' -f 2)
	local f2=$(echo $str | cut -d ' ' -f 3)
	local f3=${platforms_list[$((f2-1))]}

	dialog --stdout \
		--clear --backtitle "Enviroment Information" \
		--title "Is this OK?" \
		--yesno "Vitis Env: $f0\nXSA file: $f1\nPlatform: $f3" 0 0

	setting_script=$f0
	hardware_file=$f1
	platform=$f3
	echo "setting_script=${f0}" > env.txt
	echo "hardware_file=${f1}" >> env.txt
	echo "platform=${f3}" >> env.txt
}

set -e
if [ ! -f env.txt ]; then
	setup_env;
fi
source env.txt


echo "setting_script: $setting_script"
echo "hardware_file:  $hardware_file"
echo "platform:  $platform"

workspace=${topdir}/${platform}
platdir=${topdir}/platforms/${platform}
xil_ver="v"$(echo "$setting_script" | grep -o -E '[0-9]{4}\.[0-9]')
xsa=$(basename $hardware_file)
xsa_name=$(echo "${xsa%.*}")

ATF_TAR=arm-trusted-firmware-${xil_ver}.tar.gz
ATF_DIR=arm-trusted-firmware-xilinx-${xil_ver}

BOOT_TAR=u-boot-xlnx-${xil_ver}.tar.gz
BOOT_DIR=u-boot-xlnx-xilinx-${xil_ver}

DT_TAR=device-tree-xlnx_${xil_ver}.tar.gz
DT_DIR=device-tree-xlnx-xilinx_${xil_ver}

KERN_TAR=linux-xlnx-${xil_ver}.tar.gz
KERN_DIR=linux-xlnx-xilinx-${xil_ver}

source $setting_script
mkdir -p ${workspace}/hardware
cp -av ${hardware_file} ${workspace}/hardware

if [ ! -d $workspace ]; then
	mkdir -p $workspace
fi


## stage1: bl31
## ------------------------------------------------------------------
build_atf()
{
	cd $workspace
	if [ ! -e $ATF_TAR ]; then
		wget -O $ATF_TAR -c "https://codeload.github.com/Xilinx/arm-trusted-firmware/tar.gz/refs/tags/xilinx-${xil_ver}"
	fi
	if [ ! -e $ATF_DIR ]; then
		tar zxvf $ATF_TAR
	fi

	cd ${ATF_DIR}
	# gcc12 patch	
	set +e
	grep -e "--no-warn-rwx-segments" Makefile
	set -e
	if [ $? -ne 0 ]; then
		sed -i "s/DTC_FLAGS/TF_LDFLAGS\t\t+=\t--no-warn-rwx-segments\nDTC_FLAGS/" Makefile
	fi

	make PLAT=zynqmp ZYNQMP_CONSOLE=cadence0 bl31
}

## stage2: fsbl & pmu
## ------------------------------------------------------------------
gen_fsbl()
{
#export LC_ALL="C"
	cd $workspace

cat > gen_fsbl.tcl << EOF
hsi open_hw_design hardware/${xsa_name}.xsa
hsi generate_app -proc psu_cortexa53_0 -app zynqmp_fsbl -dir fsbl
hsi generate_app -proc psu_pmu_0 -app zynqmp_pmufw -dir pmufw
hsi close_hw_design ${xsa_name}
EOF

	xsct gen_fsbl.tcl

	make -C fsbl clean
	make -C fsbl

	make -C pmufw clean
	make -C pmufw
}

## stage3: u-boot
## ------------------------------------------------------------------
build_boot()
{
	cd $workspace

	if [ ! -e $BOOT_TAR ]; then
		wget -O $BOOT_TAR -c "https://codeload.github.com/Xilinx/u-boot-xlnx/tar.gz/refs/tags/xilinx-${xil_ver}"
	fi
	if [ ! -e $BOOT_DIR ]; then
		tar zxvf $BOOT_TAR
	fi
	exit
	cd $BOOT_DIR
	# make distclean
	make xilinx_zynqmp_virt_defconfig
	export DEVICE_TREE="zynqmp-zcu102-rev1.0"
	make -j20
}

## stage4: dtc
## ------------------------------------------------------------------
gen_dts()
{
	cd $workspace

	local dts=devicetree

	if [ ! -e $DT_TAR ]; then
		wget -O $DT_TAR -c "https://codeload.github.com/Xilinx/device-tree-xlnx/tar.gz/refs/tags/xilinx_${xil_ver}"
	fi
	if [ ! -e $DT_DIR ]; then
		tar zxvf $DT_TAR
	fi

cat > gen_dts.tcl << EOF
hsi open_hw_design hardware/${xsa_name}.xsa
hsi set_repo_path ${workspace}/$DT_DIR
hsi create_sw_design device-tree -os device_tree -proc psu_cortexa53_0
hsi generate_target -dir $dts
hsi close_hw_design ${xsa_name}
EOF
	xsct gen_dts.tcl

	cd $dts
	# open zynqmp.dtsi, add two lines to sdhci1 node:
	#   add:
	#     disable-wp;
	#     no-1-8-v;
	# open system-top.dts, modify the bootargs node:
	#   bootargs = "console=ttyPS0,115200 earlycon clk_ignore_unused cpuidle.off=1 root=/dev/mmcblk1p2 rw rootfstype=ext4 rootwait";

	# set +e
	# grep "disable-wp;" zynqmp.dtsi
	# if [ $? -ne 0 ]; then
	# 	sed -i '/clk_in_sd1/a \\t\t\tdisable-wp;\n\t\t\tno-1-8-v; ' zynqmp.dtsi
	# fi
	# set -e

	set +e
	grep "vpss_0:" pl.dtsi
	if [ $? -eq 0 ]; then
		sed -i '/vpss_0:/,/\};/d' pl.dtsi
	fi
	set -e

	# yuv444: 0x1
	sed -i 's/\(xlnx,video-format = <\).*/\10x1>;/g' pl.dtsi
	if [ -f ${platdir}/system-top.dts ]; then
		cp -f ${platdir}/system-top.dts .
	fi
	if [ -f ${platdir}/system-user.dtsi ]; then
		cp -f ${platdir}/system-user.dtsi .
	fi
	gcc -I dts -E -nostdinc -undef -D__DTS__ -x assembler-with-cpp \
	-o system-top.dts.tmp system-top.dts
	dtc -I dts -O dtb -o system.dtb system-top.dts.tmp
}

## stage5: kernel
## ------------------------------------------------------------------
build_kernel()
{
	cd ${workspace}
	if [ ! -e $KERN_TAR ]; then
		wget -O $KERN_TAR -c "https://codeload.github.com/Xilinx/linux-xlnx/tar.gz/refs/tags/xilinx-${xil_ver}"
	fi
	if [ ! -e $KERN_DIR ]; then
		tar zxvf $KERN_TAR
	fi
	cd $KERN_DIR

	if [ -f ${platdir}/aislab_defconfig ]; then
		echo "using custom defconfig..."
		cp -f ${platdir}/aislab_defconfig  arch/arm64/configs
		make ARCH=arm64 aislab_defconfig
	else
		make ARCH=arm64 xilinx_zynqmp_defconfig
	fi
	make ARCH=arm64 -j32

	sudo make ARCH=arm64 INSTALL_MOD_PATH=${workspace} modules_install
	find ${workspace}/lib/modules -name build -o -name source | sudo xargs -l1 rm -rf
	sudo chown -R root:root ${workspace}/lib/modules
}

## stage6: image
## ------------------------------------------------------------------
gen_image()
{
	cd $workspace
	mkdir -p images
	set +e
	cp -fv ${KERN_DIR}/arch/arm64/boot/Image images
	cp -fv devicetree/system.dtb images
	set -e

	cp -fv fsbl/executable.elf images/fsbl.elf
	cp -fv pmufw/executable.elf images/pmufw.elf
	cp -fv hardware/${xsa_name}.bit images/fpga.bit
	cp -fv ${ATF_DIR}/build/zynqmp/release/bl31/bl31.elf images

	read -r -p "Using prebuilt u-boot? [Y/n] " response
	case "$response" in
	[yY][eE][sS]|[yY]) 
		cp -fv ${platdir}/u-boot.elf images
		;;
	*)
		cp -fv ${BOOT_DIR}/u-boot.elf images
		;;
	esac


	cat > hsi_flow.bif << EOF
the_ROM_image:
{
    [bootloader, destination_cpu = a53-0] images/fsbl.elf
    [destination_cpu = pmu] images/pmufw.elf
    [destination_device = pl] images/fpga.bit
    [destination_cpu = a53-0, exception_level = el-3, trustzone = secure] images/bl31.elf
    [destination_cpu = a53-0, exception_level = el-2] images/u-boot.elf
}
EOF
	bootgen -image hsi_flow.bif -arch zynqmp -w -o images/BOOT.bin
	mkimage -A arm -O linux -Tscript -C none -d ${platdir}/boot.cmd images/uboot.scr
}


cmd=$1

case $cmd in
	(atf)
		build_atf
		;;
	(fsbl)
		gen_fsbl
		;;
	(boot)
		build_boot
		;;
	(linux)
		build_kernel
		;;
	(dts)
		gen_dts
		;;
	(image)
		gen_image
		;;
	(all)
		build_atf
		gen_fsbl
		build_boot
		build_kernel
		gen_dts
		gen_image
		;;
	(*)
		exit
		;;
esac


