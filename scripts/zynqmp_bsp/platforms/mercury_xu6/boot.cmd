setenv devicetree_image system.dtb
setenv def_args "console=ttyPS0,115200 earlycon clk_ignore_unused cpuidle.off=1 cma=128M earlyprintk rw"

run mmc_args && mmc rescan && load mmc 0 ${kernel_loadaddr} ${kernel_image} && load mmc 0 ${devicetree_loadaddr} ${devicetree_image} && booti ${kernel_loadaddr} - ${devicetree_loadaddr}
