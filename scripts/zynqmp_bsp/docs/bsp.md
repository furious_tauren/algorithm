# bsp

## build

### boot

```bash
git clone https://github.com/enclustra-bsp/bsp-xilinx.git
cd bsp-xilinx
./build.sh
```

### kernel
```
cd ${topdir}
./build.sh linux
```

## burn

### boot

```sh
tftpboot 0x200000 BOOT.bin

sf probe 0 0 0
sf erase 0x0 0x1000000
sf write 0x200000 0x0 0x1000000
```

### Programming others

* Step 1: netboot

```sh
setenv ipaddr 192.168.1.105; setenv netmask 255.255.255.0; setenv serverip 192.168.1.250;
tftpboot 800000 Image
tftpboot 8000000 system.dtb

# emmc boot
setenv bootargs "console=ttyPS0,115200 earlycon clk_ignore_unused cpuidle.off=1 cma=128M root=/dev/mmcblk0p2 rw rootwait earlyprintk"
setenv bootargs "console=ttyPS0,115200 earlycon clk_ignore_unused cpuidle.off=1 cma=128M rootwait root=/dev/mmcblk0p2

# ip=<client-ip>:<server-ip>:<gw-ip>:<netmask>:<hostname>:<device>:<autoconf>
setenv bootargs "console=ttyPS0,115200 earlycon clk_ignore_unused cpuidle.off=1 cma=128M root=/dev/nfs rw nfsroot=192.168.1.250:/srv/nfs/ubuntu-22.04.4-base,proto=tcp,vers=3 ip=dhcp earlyprintk"
setenv bootargs "console=ttyPS0,115200 earlycon clk_ignore_unused cpuidle.off=1 cma=128M root=/dev/nfs rw nfsroot=192.168.1.250:/srv/nfs/ubuntu-22.04.4-base,proto=tcp,vers=3 ip=${ipaddr}:${serverip}:${gatewayip}:${netmask} ::eth0:on earlyprintk"
setenv bootargs "console=ttyPS0,115200 earlycon clk_ignore_unused cpuidle.off=1 cma=128M root=/dev/nfs rw nfsroot=192.168.1.250:/srv/nfs/rootfs,proto=tcp,vers=3 ip=${ipaddr}:${serverip}:${gatewayip}:${netmask} ::eth0:on earlyprintk"
booti 800000 - 8000000

```

* Step 2: programming

Log on your board and run:

```sh
sudo mount -t nfs -o nolock 192.168.1.250:/srv/nfs /mnt

echo "# /bin/bash

set -e

sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | sudo fdisk /dev/mmcblk0
o # clear the in memory partition table
n # new partition
p # primary partition
1 # partition number 1
  # default - start at beginning of disk 
+64M
n # new partition
p # primary partition
2 # partition number 2
  # default - start at beginning of disk 
  # default - start at beginning of disk 
a # make a partition bootable
1 # bootable partition is partition 1 -- /dev/mmcblk0p1
p # print the in-memory partition table
w # write the partition table
EOF

mkfs.vfat -n BOOT /dev/mmcblk0p1
mkfs.ext4 -L rootfs /dev/mmcblk0p2

mount /dev/mmcblk0p1 /srv
cp -v /mnt/images/Image /srv
cp -v /mnt/images/system.dtb /srv
cp -v /mnt/images/uboot.scr /srv
cp -v /mnt/images/BOOT.bin /srv
umount /srv; sync
mount /dev/mmcblk0p2 /srv
tar xf /mnt/rootfs.tar -C /srv
umount /srv; sync
" > parted.sh 

chmod +x parted.sh
sudo ./parted.sh
```

## ip

### axi4-stream data fifo

