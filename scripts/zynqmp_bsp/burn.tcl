##connect
#targets -set -nocase -filter {name =~ "*PS TAP*"}
#fpga "system_wrapper.bit"
#targets -set -nocase -filter {name =~ "PSU"}
#mwr 0xFFCA0038 0x1FF
## Download PMUFW to PMU
#target -set -filter {name =~ "MicroBlaze PMU"}
#puts "Downloading pmufw"
#dow pmufw.elf
#con
#targets -set -nocase -filter {name =~ "PSU"}
## write bootloop and release A53-0 reset
#mwr 0xffff0000 0x14000000
#mwr 0xFD1A0104 0x380E
## Download FSBL to A53 #0
#targets -set -filter {name =~ "Cortex-A53 #0"}
#rst -processor
#puts "Downloading fsbl"
#dow fsbl.elf
#con
#after 30000
#stop
#
#puts "Downloading uboot"
#dow "out/u-boot.elf"
#puts "Downloading atf"
#dow "out/bl31.elf"
#con

# When using the Platform Cable, you can use 127.0.0.1 to connect
connect -url tcp:127.0.0.1:3121

# Optional: configuring the FPGA: --------------------
targets -set -nocase -filter {name =~ "*PS TAP*"}
fpga "system_wrapper.bit"

## PMU: -------------------------------
# the Platform Management Unit cannot be accessed
# without disabling some of the security gates.
# By default, JTAGsecurity gates are enabled
# This disables security gates for DAP, PLTAP and PMU.
targets -set -nocase -filter {name =~ "*PSU*"}
mwr 0xffca0038 0x1ff
targets -set -nocase -filter {name =~ "*MicroBlaze PMU*"}
dow "pmufw.elf"
con

# the resets must be removed. In addition,
# the Processing System Unit must be initialized
targets -set -nocase -filter {name =~ "*PSU*"}
# write bootloop and release A53-0 reset
mwr 0xffff0000 0x14000000
mwr 0xFD1A0104 0x380E

targets -set -nocase -filter {name =~ "*A53 #0*"}
rst -processor
dow "fsbl.elf"
con
after 30000; stop



puts "Downloading uboot"
dow "u-boot.elf"
puts "Downloading atf"
dow "bl31.elf"
con


