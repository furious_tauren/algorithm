#! /bin/bash

setup_env()
{
	local str=$(dialog --stdout \
		--backtitle "Vivado Enviroment Setup" \
		--title "Select \`setting64.sh\`" \
		--fselect $HOME 20 60 \
		--and-widget --clear \
		--backtitle "Backup Project" \
		--title "Project(xxx.xpr)" \
		--fselect $HOME 20 60)

	local f0=$(echo $str | cut -d ' ' -f 1)
	local f1=$(echo $str | cut -d ' ' -f 2)

	dialog --stdout \
		--clear --backtitle "Enviroment Information" \
		--title "Is this OK?" \
		--yesno "Vivado Env: $f0\nProject: $f1" 0 0

	setting_script=$f0
	project_file=$f1
	echo "setting_script=${f0}" > env.txt
	echo "project_file=${f1}" >> env.txt
}

set -e
if [ ! -f env.txt ]; then
	setup_env;
fi
source env.txt
source $setting_script

project=$(basename $project_file)
scripts=$(dirname $(realpath $0))
read -r -p "Enter backup directory(default: $scripts):" backup_dir
if [ -z $backup_dir ]; then
	backup_dir=$scripts
fi
mkdir -p $backup_dir
proj_tcl=${backup_dir}/create_project.tcl

pushd $(dirname $project_file) 2>&1 > /dev/null
echo "
open_project $project
# update ip
report_ip_status
upgrade_ip [get_ips]
# reset_run synth_1

# paths_relative_to: root of relative path(cd ..; -paths_relative_to aislab)
write_project_tcl -origin_dir_override imports -paths_relative_to . -force $proj_tcl
close_project
exit
" | tee /tmp/backup_${project}.tcl
vivado -mode batch -source /tmp/backup_${project}.tcl

s1=$(grep -e "^ \"\[file normalize \"\$origin_dir" $proj_tcl)
s2=$(echo $s1 | sed -e 's!"\[file normalize "\$origin_dir/!!g')
flist=$(echo $s2 | sed -e 's/"\]"\\/ /g')
for i in $flist; do
    rp=${backup_dir}/imports/$(dirname $i)
    mkdir -p $rp
    cp $i $rp
done
popd 2>&1 > /dev/null


# cd ${new_proj}
# vivado -mode batch -source proj.tcl
