#! /bin/bash

set -e

## opencv for vision
## doc: https://support.xilinx.com/s/article/Vitis-Libraries-Compiling-and-Installing-OpenCV?language=en_US

setup_env()
{
	local str=$(dialog --stdout \
		--clear --backtitle "Vitis Enviroment Setup" \
		--title "Select Vitis(xxx/Vitis/20xx.x)" \
		--fselect $HOME 20 60)
	VITIS_PATH=$str
	echo "export VITIS_PATH=$str" >> ~/.zshrc
}

select_prefix()
{
	local str=$(dialog --stdout \
		--title "Destination where to install operncv?" \
		--fselect $HOME 20 60)
	PREFIX=$str
}

git_clone()
{
	local url=$1
	local arg2=$2
	local branch=$3
	local dest=$4
	if [ ! -d $dest ]; then
		git clone $url $arg2 $branch $dest
	fi
}

if [ -z $VITIS_PATH ]; then
	setup_env;
fi
select_prefix
if [ -z $PREFIX ]; then
	echo "Has not specified destination, using ${pwd}"
	PREFIX=$(pwd)
fi
if [ ! -d $PREFIX ]; then
	echo "Directory is not invalid, exiting..."
	exit
fi

VERSION=$(echo "$VITIS_PATH" | grep -o -E '[0-9]{4}\.[0-9]')
git_clone https://github.com/opencv/opencv --branch 4.4.0 source
git_clone https://github.com/opencv/opencv_contrib --branch 4.4.0 source_contrib
git_clone https://github.com/Xilinx/Vitis_Libraries.git --branch v${VERSION}_rel $PREFIX/Vitis_Libraries

source ${VITIS_PATH}/settings64.sh
export LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/:$LIBRARY_PATH
# asm/errno.h: No such file
export CPATH="/usr/include/x86_64-linux-gnu"

TOPDIR=$(pwd)
mkdir -p source/build
cd source/build

export HTTP_PROXY=127.0.0.1:7897
export HTTPS_PROXY=127.0.0.1:7897

cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D OPENCV_EXTRA_MODULES_PATH=$TOPDIR/source_contrib/modules \
	-D CMAKE_INSTALL_PREFIX=$PREFIX/opencv \
	-D WITH_V4L=ON \
	-D BUILD_TESTS=OFF \
	-D BUILD_ZLIB=ON \
	-D BUILD_JPEG=ON \
	-D WITH_JPEG=ON \
	-D WITH_PNG=ON \
	-D BUILD_EXAMPLES=OFF \
	-D INSTALL_C_EXAMPLES=OFF \
	-D INSTALL_PYTHON_EXAMPLES=OFF \
	-D WITH_OPENEXR=OFF \
	-D BUILD_OPENEXR=OFF \
	-D CMAKE_CXX_COMPILER=$XILINX_HLS/tps/lnx64/gcc-6.2.0/bin/g++ \
	..

make -j20
#make install

