# Hardware Test

## i2c

** Warning: Can’t use SMBus Quick Write command, will skip some addresses
By default, i2cdetect command uses the “SMBUS quick write” sequence for
certain address range.

Some simpler i2c slave devices may not respond to SMBUS quick write command.
If you provide the “-r” option, it will use I2C READ sequence always.

```sh
i2cdetect -l
i2cdetect -y -r 0
```

## gpio

```sh
# ------> gpioset
# 
# Usage: gpioset [OPTIONS] <chip name/number> \<offset1\> \<value1\> \<offset2\>=\<value2\> ...
# 
# Set GPIO line values of a GPIO chip and maintain the state until the process exits
# 
# Options:
#   -h, --help:		display this message and exit
#   -v, --version:	display the version and exit
#   -l, --active-low:	set the line active state to low
#   -B, --bias=[as-is|disable|pull-down|pull-up] (defaults to 'as-is'):
#       set the line bias
#   -D, --drive=[push-pull|open-drain|open-source] (defaults to 'push-pull'):
#       set the line drive mode
#   -m, --mode=[exit|wait|time|signal] (defaults to 'exit'):
#       tell the program what to do after setting values
#   -s, --sec=SEC:	specify the number of seconds to wait (only valid for --mode=time)
#   -u, --usec=USEC:	specify the number of microseconds to wait (only valid for --mode=time)
#   -b, --background:	after setting values: detach from the controlling terminal
# 
# Biases:
#   as-is:	leave bias unchanged
#   disable:	disable bias
#   pull-up:	enable pull-up
#   pull-down:	enable pull-down
# 
# Drives:
#   push-pull:	drive the line both high and low
#   open-drain:	drive the line low or go high impedance
#   open-source:	drive the line high or go high impedance
#
# Modes:
#   exit:	set values and exit immediately
#   wait:	set values and wait for user to press ENTER
#   time:	set values and sleep for a specified amount of time
#   signal:	set values and wait for SIGINT or SIGTERM
gpioset --mode=time -s 1 0 24=1

# ------> gpiodetect
# List all GPIO chips, print their labels and number of GPIO lines.
gpiodetect

# ------> gpioinfo

# Usage: gpioinfo [OPTIONS] \<gpiochip1\> ...

# Print information about all lines of the specified GPIO chip(s) (or all gpiochips if none are specified).
gpioinfo
gpioinfo 0

# ------> gpioget

# Usage: gpioget [OPTIONS] <chip name/number> <offset 1> <offset 2> ...
# 
# Read line value(s) from a GPIO chip
# 
# Options:
#   -h, --help:		display this message and exit
#   -v, --version:	display the version and exit
#   -l, --active-low:	set the line active state to low
#   -B, --bias=[as-is|disable|pull-down|pull-up] (defaults to 'as-is'):
# 		set the line bias
#
# Biases:
#   as-is:	leave bias unchanged
#   disable:	disable bias
#   pull-up:	enable pull-up
#   pull-down:	enable pull-down

```

## uart

```sh
TTY=/dev/ttyUSB7
stty -F $TTY speed 115200 cs8 -parity -cstopb -ixon -ixoff
```

## lm75

```sh
sudo sensors-detect
sudo sensors
```

## rtc

```sh
timedatectl
timedatectl set-ntp false

# Set the system clock to the specified time.
# This will also update the RTC time accordingly.
timedatectl set-time "2015-01-31 11:13:54"
```
