#! /bin/sh

set -e

if [ $# -lt 4 ]; then
	echo "usage: $0 host user passwd gui[YES/NO]"
	exit
fi

# change the owner of '/'
chown root:root /

# change the setting here
HOST=$1
USER=$2
PASSWORD=$3
XWIN=$4

read -p "Enter Serial TTY(default ttyS0): " tty
if [ -z $tty ]; then
	tty=ttyS0
fi

export TERM=xterm-256color

# setup hostname
echo $HOST > /etc/hostname
echo "127.0.0.1\tlocalhost.localdomain localhost" > /etc/hosts
echo "127.0.0.1\t$HOST" >> /etc/hosts

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# fetch the latest package lists from server then upgrade
apt update
apt upgrade -y

# install these first to avoid warnings
apt install -y dialog perl apt-utils bash-completion

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# install minimal packages, `init` is included by systemd-sysv
# which is dependence for ssh.
apt install -y language-pack-en-base locales
apt install -y sudo ssh

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# -- eth & net
# net-tools: arp, ifconfig, netstat, rarp, nameif, route
# ifupdown:  provides the tools ifup and ifdown
apt install -y net-tools ifupdown iputils-ping ethtool iperf3

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# -- system maintenance
# kmod: lsmod & insmod
apt install -y cron udev kmod resolvconf rsyslog \
	fdisk bzip2 vim git dosfstools nfs-common

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# -- common peripheral utils
apt install -y gpiod i2c-tools spi-tools usbutils pciutils can-utils lm-sensors

## bsdmainutils: hexdump
apt install -y bsdmainutils devmem2



# # docker
# apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
# #curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
# #add-apt-repository "deb [arch="$(dpkg --print-architecture)"] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# curl -fsSL https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
# add-apt-repository "deb [arch="$(dpkg --print-architecture)"] https://mirrors.ustc.edu.cn/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
# 
# apt update
# apt install -y docker-ce docker-ce-cli containerd.io

# arm32 support
# dpkg --add-architecture armhf
# apt update -y
# apt-get install -y libc6:armhf libstdc++6:armhf

if [ "$XWIN" = "xfce" ]; then
	apt install -y lightdm lightdm-gtk-greeter

	# xfce
	# xfce4-goodies – The goodies package contains various plugins,
	# applications and more, making using XFCE on Ubuntu a nicer experience.
	apt install -y xfce4 xfce4-goodies x-window-system
elif [ "$XWIN" = "lxqt" ]; then

	# lxqt: low quanlity
	apt install -y lxqt-core xinit
	
	# apt install -y xorg xinit fluxbox
	# apt install -y synaptic dillo xfce4-terminal menu menu-xdg logrotate localepurge

elif [ "$XWIN" = "lxde" ]; then
	apt install -y lxde
	apt install -y xorg file-roller
fi

# cleanup
apt autoremove -y
apt-get clean
# rm -rf /tmp/{*,.*}
find /var/log -type f \( -name "*.gz" -o -name "*.xz" -o -name "*.log" \) -delete

# enable serial console
ln -s /lib/systemd/system/serial-getty\@.service /etc/systemd/system/getty.target.wants/serial-getty\@${tty}.service
sed -i 's/TimeoutStartSec=5min/TimeoutStartSec=30sec/g' /etc/systemd/system/network-online.target.wants/networking.service

# disable netdev alias
ln -s /dev/null /etc/systemd/network/99-default.link

# A start job is running for Raise ne …rk interfaces
sed -i 's/^timeout 300;/timeout 20;/g' /etc/dhcp/dhclient.conf

# create User
useradd -s '/bin/bash' -m -G adm,sudo $USER
echo $USER:$PASSWORD | chpasswd

# auto update DNS
# dpkg-reconfigure resolvconf

# timezone
# dpkg-reconfigure tzdata


