#! /bin/bash

scripts=$(dirname $(realpath $0))


if [ "$EUID" -ne 0 ]; then
	echo "Please run as root..."
	exit
fi

apt list --installed android-sdk-libsparse-utils 2>&1 > /dev/null
if [ $? -ne 0 ]; then
	sudo apt update -y
	apt install -y android-sdk-libsparse-utils
fi

ubuntu_dir=$(dialog --stdout --title "ubuntu directory" --fselect $scripts 20 60)
issue=$(basename $ubuntu_dir)

workspace=${scripts}/images

mkdir -p $workspace
pushd $workspace

mount | grep "${issue}.img" 2>&1 > /dev/null
if [ $? -eq 0 ]; then
	umount $ubuntu_dir
fi


rm -fr ${issue}.*

set -e

dd if=/dev/zero of=${issue}.img bs=1M count=4096
mkfs.ext4 ${issue}.img

pushd $ubuntu_dir
tar cf ${workspace}/${issue}.tar .
popd

mount -o loop ${issue}.img $ubuntu_dir
tar -xf ${issue}.tar -C $ubuntu_dir
umount $ubuntu_dir

e2fsck -p -f ${issue}.img
resize2fs -M ${issue}.img
img2simg -s ${issue}.img ${issue}.simg
popd

ls -lh $workspace

