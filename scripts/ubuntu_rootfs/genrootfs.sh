#! /bin/bash

# Building Ubuntu Root Filesystem

set -e

scripts=$(dirname $(realpath $0))
workspace=${scripts}/tmp
if [ ! -d $workspace ]; then
	mkdir -p $workspace
fi

main_version=22.04
sub_version=4
arch=arm64
arch_alias=aarch64

read -p "Enter ARCH(only supports arm or arm64, default arm64): " arch
if [ -z $arch ]; then
	echo "using \`arm64\` as default"
	arch=arm64
	arch_alias=aarch64
elif [ "arm" == $arch ]; then
	arch_alias=arm
elif [ "arm64" == $arch ]; then
	arch_alias=aarch64
else 
	echo "Unsupported ARCH \`${arch}\`, exiting..."
	exit 1
fi

qemu=qemu-${arch_alias}-static

HOST="aislab"
USER="devel"
PASSWORD="123456"
XWIN="base"
while [[ $# -gt 0 ]]; do
	case $1 in
		-h=*|--host=*)
			HOST="${1#*=}"
			shift # past argument
			;;
		-u=*|--user=*)
			USER="${1#*=}"
			shift
			;;
		-p=*|--password=*)
			PASSWORD="${1#*=}"
			shift
			;;
		-m=*|--version=*)
			main_version="${1#*=}"
			shift
			;;
		-s=*|--subversion=*)
			sub_version="${1#*=}"
			shift
			;;
		-x=*|--xwin=*)
			XWIN="${1#*=}"
			shift
			;;
		--default)
			DEFAULT=YES
			shift # past argument
			;;
		-*|--*)
			echo "Unknown option $1"
			exit 1
			;;
		*)
			;;
	esac
done


ubuntu=ubuntu-base-${main_version}.${sub_version}-base-${arch}.tar.gz
rootfs=${workspace}/ubuntu-${main_version}.${sub_version}-${XWIN}
setup_ubuntu() {
	if [ ! -f /usr/bin/${qemu} ]; then
		apt install -y qemu-user-static
	fi

	pushd $workspace
	if [ -e ${rootfs} ]; then
		set +e
		mount | grep ${rootfs}/proc 2>&1 > /dev/null
		if [ $? -eq 0 ]; then
			${scripts}/ch-mount -u ${rootfs}/
		fi
		set -e
		rm -fr ${rootfs}
	fi

	if [ ! -f ${ubuntu} ]; then
		wget -c http://cdimage.ubuntu.com/ubuntu-base/releases/${main_version}/release/${ubuntu}
	fi

	mkdir -p ${rootfs}
	# Extract with 'sudo' to allow 'mknod' to work
	tar -xpf ${ubuntu} -C ${rootfs}
	
	# chroot to the new filesystem and initialize:
	if [ ! -f ${rootfs}/usr/bin/${qemu} ]; then
		cp -av /usr/bin/${qemu} ${rootfs}/usr/bin/
	fi
	cp -bv /etc/resolv.conf ${rootfs}/etc/resolv.conf

	#cp -av ${scripts}/sources.list.${main_version} ${rootfs}/etc/apt/sources.list
	cp -av ${scripts}/setup.sh ${rootfs}/tmp

	# XXX
	set +e
	${scripts}/ch-mount -m ${rootfs}/ /tmp/setup.sh $HOST $USER $PASSWORD $XWIN
	if [ $? -ne 0 ]; then
		echo "Error: exiting..."
		${scripts}/ch-mount -u ${rootfs}/
		exit
	fi
	${scripts}/ch-mount -u ${rootfs}/
	
	echo "Clean ........................"
	
	# clean
	# rm -vfr ${rootfs}/usr/bin/${qemu}
	cp -av ${scripts}/interfaces ${rootfs}/etc/network/
	rm -vfr ${rootfs}/tmp/setup.sh
	#rm -rf /tmp/{*,.*} # /tmp
	rm -vfr ${rootfs}/var/lib/apt/lists/*
	popd
	
	echo "OK"
}


setup_ubuntu



