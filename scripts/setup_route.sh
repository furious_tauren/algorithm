#! /bin/sh


user=`whoami`

if [ $user != "root" ]; then
    echo "You should run this script as root, exit!" && exit 1;
fi

eth=enp0s31f6
net=wlp0s20f3
#net=enx56ebe9e0316d

sysctl -w net.ipv4.ip_forward=1

# Forward LAN packets to the WAN.
iptables -A FORWARD -i $eth -o $net -j ACCEPT

## Forward WAN packets to the LAN if the LAN initiated the connection.
iptables -A FORWARD -i $net -o $eth -m state --state ESTABLISHED,RELATED -j ACCEPT

# NAT traffic going out the WAN interface.
iptables -t nat -A POSTROUTING -o $net -j MASQUERADE

